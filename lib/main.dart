import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chat/chat_bloc.dart';
import 'package:bakamlachat/blocs/chatgrup/chatgrup_bloc.dart';
import 'package:bakamlachat/blocs/profile/profile_bloc.dart';
import 'package:bakamlachat/blocs/user/user_bloc.dart';
import 'package:bakamlachat/cubits/cubits.dart';
import 'package:bakamlachat/repositories/auth/auth_repository.dart';
import 'package:bakamlachat/repositories/chat/chat_repository.dart';
import 'package:bakamlachat/repositories/chat_message/chat_message_repository.dart';
import 'package:bakamlachat/repositories/user/user_repository.dart';
import 'package:bakamlachat/screens/call_log_screen/call_log_screen.dart';
import 'package:bakamlachat/screens/chat/chat_screen.dart';
import 'package:bakamlachat/screens/chat_group_list/chat_group_list_screen.dart';
import 'package:bakamlachat/screens/chat_group_list/chat_group_screen.dart';
import 'package:bakamlachat/screens/chat_list/chat_list_screen.dart';
import 'package:bakamlachat/screens/home/home_screen.dart';
import 'package:bakamlachat/screens/profile/profile_screeen.dart';
import 'package:bakamlachat/screens/profile/profile_screen_view_model.dart';
import 'package:bakamlachat/screens/redirect/no_connection_screen.dart';
import 'package:bakamlachat/screens/redirect/not_allowed_screen.dart';
import 'package:bakamlachat/screens/setelan/setelan_screen.dart';
import 'package:bakamlachat/screens/setelan/setelan_screen_view_model.dart';
import 'package:bakamlachat/screens/splash/splash_screen.dart';
import 'package:bakamlachat/utils/zego_config.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bakamlachat/screens/guest/guest_screen.dart';
import 'package:bakamlachat/screens/guest/signup_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:zego_express_engine/zego_express_engine.dart';
import 'package:zego_uikit_prebuilt_call/zego_uikit_prebuilt_call.dart';
import 'package:zego_uikit_signaling_plugin/zego_uikit_signaling_plugin.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: kIsWeb
        ? HydratedStorage.webStorageDirectory
        : await getTemporaryDirectory(),
  );
  // Get your AppID and AppSign from ZEGOCLOUD Console
  //[My Projects -> AppID] : https://console.zegocloud.com/project
  await ZegoExpressEngine.createEngineWithProfile(ZegoEngineProfile(
    ZegoKonfig.appId,
    ZegoScenario.Default,
    appSign: ZegoKonfig.appSign,
    // appSign: kIsWeb ? null : ZegoKonfig.appSign,
  ));

  /// 1.1.2: set navigator key to ZegoUIKitPrebuiltCallInvitationService
  ZegoUIKitPrebuiltCallInvitationService().setNavigatorKey(navigatorKey);

  // call the useSystemCallingUI
  ZegoUIKit().initLog().then((value) {
    ZegoUIKitPrebuiltCallInvitationService().useSystemCallingUI(
      [ZegoUIKitSignalingPlugin()],
    );
    runApp(MyApp(navigatorKey: navigatorKey));
  });
}

class MyApp extends StatefulWidget {
  final GlobalKey navigatorKey;

  const MyApp({
    required this.navigatorKey,
    Key? key,
  }) : super(key: key);

  @override
  State createState() => MyAppState();
}

class MyAppState extends State {
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider<AuthRepository>(
            create: (_) => AuthRepository(),
          ),
          RepositoryProvider<ChatRepository>(
            create: (_) => ChatRepository(),
          ),
          RepositoryProvider<ChatMessageRepository>(
            create: (_) => ChatMessageRepository(),
          ),
          RepositoryProvider<UserRepository>(
            create: (_) => UserRepository(),
          ),
        ],
        child: MultiBlocProvider(
          providers: [
            BlocProvider(create: (_) => AuthBloc()),
            BlocProvider(
              create: (context) => GuestCubit(
                  authRepository: context.read<AuthRepository>(),
                  authBloc: context.read<AuthBloc>()),
            ),
            BlocProvider(
              create: (context) => ChatBloc(
                chatRepository: context.read<ChatRepository>(),
                chatMessageRepository: context.read<ChatMessageRepository>(),
              ),
            ),
            BlocProvider(
              create: (context) => ChatgrupBloc(
                chatRepository: context.read<ChatRepository>(),
                chatMessageRepository: context.read<ChatMessageRepository>(),
              ),
            ),
            BlocProvider(
              create: (context) =>
                  UserBloc(userRepository: context.read<UserRepository>()),
            ),
            BlocProvider(
              create: (context) =>
                  ProfileBloc()..add(const ProfileEvent.getProfile()),
            ),
          ],
          child: MaterialApp(
            navigatorKey: navigatorKey,
            debugShowCheckedModeBanner: false,
            title: 'Bakamla Chat',
            theme: ThemeData(
              primarySwatch: Colors.red,
              useMaterial3: false,
            ),
            initialRoute: SplashScreen.routeName,
            builder: EasyLoading.init(),
            routes: {
              SplashScreen.routeName: (_) => const SplashScreen(),
              SignUpScreen.routeName: (_) => const SignUpScreen(),
              GuestScreen.routeName: (_) => const GuestScreen(),
              ChatListScreen.routeName: (_) => const ChatListScreen(),
              ChatScreen.routeName: (_) => const ChatScreen(),
              SetelanScreen.routeName: (_) =>
                  SetelanScreen(viewModel: SetelanScreenViewModel()),
              ProfileScreen.routeName: (_) =>
                  ProfileScreen(viewModel: ProfileScreenViewModel()),
              MainScreen.routeName: (_) => const MainScreen(),
              ChatGroupListScreen.routeName: (_) => const ChatGroupListScreen(),
              ChatGrupScreen.routeName: (_) => const ChatGrupScreen(),
              CallLogScreen.routeName: (_) => const CallLogScreen(),
              NoConnectionScreen.routeName: (_) => const NoConnectionScreen(),
              NotAllowedScreen.routeName: (_) => const NotAllowedScreen(),
              // PopUpScreen
            },
          ),
        ));
  }
}
