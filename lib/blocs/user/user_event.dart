part of 'user_bloc.dart';

@freezed
class UserEvent with _$UserEvent {
  const factory UserEvent.started() = UserStarted;
  // const factory UserEvent.updateUser(UserEntity user) = UpdateUser;
}
