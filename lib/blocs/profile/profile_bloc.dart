import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/models/profile.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:bakamlachat/utils/utils.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile_event.dart';
part 'profile_state.dart';
part 'profile_bloc.freezed.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final authToken = AuthBloc().state.token;

  ProfileBloc() : super(const ProfileInitial()) {
    on<ProfileEvent>((event, emit) async {
      try {
        emit(const ProfileLoading());
        final profile = await Dio().get(Endpoints.profileGet,
            options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer $authToken"
              },
            ));
        if (profile.statusCode == 200) {
          final profiles = ProfileEntity.fromJson(profile.data);
          eLog('success');
          emit(ProfileLoaded(profiles));
        } else {
          eLog("Failed to get profile. Status code: ${profile.statusCode}");
          emit(const ProfileError("Gagal Status Code"));
        }
      } catch (e) {
        emit(const ProfileError("Gagal mengambil profile"));
      }
    });
    on<changeProfile>((event, emit) async {
      try {
        emit(const ProfileLoading());
        late String? url;
        if (event.type == 'profile') {
          url = Endpoints.profileAvatar;
        } else if (event.type == 'username') {
          url = Endpoints.profileUsername;
        } else if (event.type == 'bio') {
          url = Endpoints.profileBio;
        } else {
          url = "";
        }
        final resp = await Dio().post(url,
            data: event.data,
            options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer $authToken"
              },
            ));
        if (resp.statusCode == 200) {
          add(const ProfileEvent.getProfile());
        } else {
          eLog("Error: ${resp.statusCode}");
        }
      } catch (e) {
        eLog("Failed to upload image");
      }
    });
  }
}
