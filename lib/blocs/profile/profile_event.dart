part of 'profile_bloc.dart';

@freezed
class ProfileEvent with _$ProfileEvent {
  const factory ProfileEvent.getProfile() = getProfile;
  const factory ProfileEvent.changeProfile({
    required FormData data,
    required String type,
  }) = changeProfile;
}
