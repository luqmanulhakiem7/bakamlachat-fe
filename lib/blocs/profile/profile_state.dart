part of 'profile_bloc.dart';

@freezed
class ProfileState with _$ProfileState {
  const factory ProfileState.initial() = ProfileInitial;
  const factory ProfileState.loading() = ProfileLoading;
  const factory ProfileState.loaded(ProfileEntity? profile) = ProfileLoaded;
  const factory ProfileState.error(String message) = ProfileError;
}
