import 'dart:ffi';

import 'package:bakamlachat/enums/enums.dart';
import 'package:bakamlachat/models/chat_message_model.dart';
import 'package:bakamlachat/models/chat_model.dart';
import 'package:bakamlachat/models/models.dart';
import 'package:bakamlachat/models/requests/create_chat_messages_request.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/repositories/chat/chat_repository.dart';
import 'package:bakamlachat/repositories/chat_message/chat_message_repository.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:bloc/bloc.dart';
import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'chatgrup_event.dart';
part 'chatgrup_state.dart';
part 'chatgrup_bloc.freezed.dart';

class ChatgrupBloc extends Bloc<ChatgrupEvent, ChatgrupState> {
  final ChatRepository _chatRepository;
  final ChatMessageRepository _chatMessageRepository;

  ChatgrupBloc(
      {required ChatRepository chatRepository,
      required ChatMessageRepository chatMessageRepository})
      : _chatRepository = chatRepository,
        _chatMessageRepository = chatMessageRepository,
        super(ChatgrupState.initial()) {
    on<ChatGrupStarted>((event, emit) async {
      if (state.status.isLoading) return;
      emit(state.copyWith(status: DataStatus.loading));

      final result = await _chatRepository.getGrupChats();

      emit(state.copyWith(
        status: DataStatus.loaded,
        chats: result.success ? result.data ?? [] : [],
      ));
    });
    on<ChatGrupReset>((event, emit) {
      emit(ChatgrupState.initial());
    });
    on<ChatGrupSelected>((event, emit) {
      emit(state.copyWith(selectedChat: event.chat));
    });
    on<GetGrupChatMessage>((event, emit) async {
      if (state.status.isFetching) return;

      emit(state.copyWith(status: DataStatus.fetching));

      ChatEntity? chat;
      chat = state.selectedChat;

      if (chat == null) {
        emit(state.copyWith(
          chatMessages: [],
          status: DataStatus.loaded,
        ));
        return;
      }

      final result = await _chatMessageRepository.getChatGrupMessages(
        chatId: chat.id,
        page: 1,
      );

      if (result.success) {
        emit(state.copyWith(
          chatMessages: result.data ?? [],
          status: DataStatus.loaded,
          selectedChat: chat,
        ));
      } else {
        emit(state.copyWith(
          chatMessages: [],
          status: DataStatus.error,
          message: result.message,
        ));
      }
    });
    on<SendGrupMessage>((event, emit) async {
      emit(state.copyWith(status: DataStatus.submitting));

      final result = await _chatMessageRepository.createChatGrupMessage(
        CreateChatMessagesRequest(
          chatId: event.chatId,
          message: event.message.text,
          ivkey: event.ivkey,
        ),
        event.socketId,
      );

      if (result.success) {
        final messages = [result.data!, ...state.chatMessages];

        emit(
          state.copyWith(
            chatMessages: messages,
            status: DataStatus.loaded,
          ),
        );
      } else {
        emit(state.copyWith(
          status: DataStatus.loaded,
        ));
      }
    });
    on<sendGrupMedia>((event, emit) async {
      try {
        final resp = await Dio().post(Endpoints.chatGrupMessage,
            data: event.data,
            options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer ${event.token}"
              },
            ));
        if (resp.statusCode == 200) {
          add(const LoadMoreGrupChatMessage());
          add(const GetGrupChatMessage());
        }
      } catch (e) {
        eLog("Gagal Upload");
      }
    });
    on<LoadMoreGrupChatMessage>((event, emit) async {
      if (state.status.isLoadingMore || state.isLastPage) return;

      emit(state.copyWith(status: DataStatus.loadingMore));

      final newPage = state.page + 1;
      final result = await _chatMessageRepository.getChatGrupMessages(
        chatId: state.selectedChat!.id,
        page: newPage,
      );

      if (result.success) {
        final newMessages = result.data ?? [];

        if (newMessages.isNotEmpty) {
          emit(state.copyWith(
            chatMessages: [...state.chatMessages, ...newMessages],
            status: DataStatus.loaded,
            page: newPage,
          ));
        } else {
          emit(state.copyWith(
            status: DataStatus.loaded,
            isLastPage: true,
          ));
        }
      } else {
        emit(state.copyWith(
          message: result.message,
          status: DataStatus.error,
        ));
      }
    });
    on<AddNewGrupMessage>((event, emit) {
      emit(state.copyWith(
        chatMessages: [event.message, ...state.chatMessages],
      ));
    });
    on<ChatGrupNotificationOpened>((event, emit) {
      emit(state.copyWith(
        notificationChatId: event.chatId,
      ));
    });
  }
}
