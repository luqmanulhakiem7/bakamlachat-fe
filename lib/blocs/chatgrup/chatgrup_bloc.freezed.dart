// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'chatgrup_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ChatgrupEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() reset,
    required TResult Function(ChatMessageEntity message) addNewGrupMesage,
    required TResult Function(ChatEntity chat) chatGrupSelected,
    required TResult Function() getGrupChatMessage,
    required TResult Function() loadMoreChatMessage,
    required TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)
        sendMessage,
    required TResult Function(FormData data, String token) sendMedia,
    required TResult Function(int chatId) chatGrupNotificationOpened,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? reset,
    TResult? Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult? Function(ChatEntity chat)? chatGrupSelected,
    TResult? Function()? getGrupChatMessage,
    TResult? Function()? loadMoreChatMessage,
    TResult? Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult? Function(FormData data, String token)? sendMedia,
    TResult? Function(int chatId)? chatGrupNotificationOpened,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? reset,
    TResult Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult Function(ChatEntity chat)? chatGrupSelected,
    TResult Function()? getGrupChatMessage,
    TResult Function()? loadMoreChatMessage,
    TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult Function(FormData data, String token)? sendMedia,
    TResult Function(int chatId)? chatGrupNotificationOpened,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatGrupStarted value) started,
    required TResult Function(ChatGrupReset value) reset,
    required TResult Function(AddNewGrupMessage value) addNewGrupMesage,
    required TResult Function(ChatGrupSelected value) chatGrupSelected,
    required TResult Function(GetGrupChatMessage value) getGrupChatMessage,
    required TResult Function(LoadMoreGrupChatMessage value)
        loadMoreChatMessage,
    required TResult Function(SendGrupMessage value) sendMessage,
    required TResult Function(sendGrupMedia value) sendMedia,
    required TResult Function(ChatGrupNotificationOpened value)
        chatGrupNotificationOpened,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatGrupStarted value)? started,
    TResult? Function(ChatGrupReset value)? reset,
    TResult? Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult? Function(ChatGrupSelected value)? chatGrupSelected,
    TResult? Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult? Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult? Function(SendGrupMessage value)? sendMessage,
    TResult? Function(sendGrupMedia value)? sendMedia,
    TResult? Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatGrupStarted value)? started,
    TResult Function(ChatGrupReset value)? reset,
    TResult Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult Function(ChatGrupSelected value)? chatGrupSelected,
    TResult Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult Function(SendGrupMessage value)? sendMessage,
    TResult Function(sendGrupMedia value)? sendMedia,
    TResult Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatgrupEventCopyWith<$Res> {
  factory $ChatgrupEventCopyWith(
          ChatgrupEvent value, $Res Function(ChatgrupEvent) then) =
      _$ChatgrupEventCopyWithImpl<$Res, ChatgrupEvent>;
}

/// @nodoc
class _$ChatgrupEventCopyWithImpl<$Res, $Val extends ChatgrupEvent>
    implements $ChatgrupEventCopyWith<$Res> {
  _$ChatgrupEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ChatGrupStartedImplCopyWith<$Res> {
  factory _$$ChatGrupStartedImplCopyWith(_$ChatGrupStartedImpl value,
          $Res Function(_$ChatGrupStartedImpl) then) =
      __$$ChatGrupStartedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ChatGrupStartedImplCopyWithImpl<$Res>
    extends _$ChatgrupEventCopyWithImpl<$Res, _$ChatGrupStartedImpl>
    implements _$$ChatGrupStartedImplCopyWith<$Res> {
  __$$ChatGrupStartedImplCopyWithImpl(
      _$ChatGrupStartedImpl _value, $Res Function(_$ChatGrupStartedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ChatGrupStartedImpl
    with DiagnosticableTreeMixin
    implements ChatGrupStarted {
  const _$ChatGrupStartedImpl();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatgrupEvent.started()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'ChatgrupEvent.started'));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ChatGrupStartedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() reset,
    required TResult Function(ChatMessageEntity message) addNewGrupMesage,
    required TResult Function(ChatEntity chat) chatGrupSelected,
    required TResult Function() getGrupChatMessage,
    required TResult Function() loadMoreChatMessage,
    required TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)
        sendMessage,
    required TResult Function(FormData data, String token) sendMedia,
    required TResult Function(int chatId) chatGrupNotificationOpened,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? reset,
    TResult? Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult? Function(ChatEntity chat)? chatGrupSelected,
    TResult? Function()? getGrupChatMessage,
    TResult? Function()? loadMoreChatMessage,
    TResult? Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult? Function(FormData data, String token)? sendMedia,
    TResult? Function(int chatId)? chatGrupNotificationOpened,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? reset,
    TResult Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult Function(ChatEntity chat)? chatGrupSelected,
    TResult Function()? getGrupChatMessage,
    TResult Function()? loadMoreChatMessage,
    TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult Function(FormData data, String token)? sendMedia,
    TResult Function(int chatId)? chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatGrupStarted value) started,
    required TResult Function(ChatGrupReset value) reset,
    required TResult Function(AddNewGrupMessage value) addNewGrupMesage,
    required TResult Function(ChatGrupSelected value) chatGrupSelected,
    required TResult Function(GetGrupChatMessage value) getGrupChatMessage,
    required TResult Function(LoadMoreGrupChatMessage value)
        loadMoreChatMessage,
    required TResult Function(SendGrupMessage value) sendMessage,
    required TResult Function(sendGrupMedia value) sendMedia,
    required TResult Function(ChatGrupNotificationOpened value)
        chatGrupNotificationOpened,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatGrupStarted value)? started,
    TResult? Function(ChatGrupReset value)? reset,
    TResult? Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult? Function(ChatGrupSelected value)? chatGrupSelected,
    TResult? Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult? Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult? Function(SendGrupMessage value)? sendMessage,
    TResult? Function(sendGrupMedia value)? sendMedia,
    TResult? Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatGrupStarted value)? started,
    TResult Function(ChatGrupReset value)? reset,
    TResult Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult Function(ChatGrupSelected value)? chatGrupSelected,
    TResult Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult Function(SendGrupMessage value)? sendMessage,
    TResult Function(sendGrupMedia value)? sendMedia,
    TResult Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class ChatGrupStarted implements ChatgrupEvent {
  const factory ChatGrupStarted() = _$ChatGrupStartedImpl;
}

/// @nodoc
abstract class _$$ChatGrupResetImplCopyWith<$Res> {
  factory _$$ChatGrupResetImplCopyWith(
          _$ChatGrupResetImpl value, $Res Function(_$ChatGrupResetImpl) then) =
      __$$ChatGrupResetImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ChatGrupResetImplCopyWithImpl<$Res>
    extends _$ChatgrupEventCopyWithImpl<$Res, _$ChatGrupResetImpl>
    implements _$$ChatGrupResetImplCopyWith<$Res> {
  __$$ChatGrupResetImplCopyWithImpl(
      _$ChatGrupResetImpl _value, $Res Function(_$ChatGrupResetImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ChatGrupResetImpl
    with DiagnosticableTreeMixin
    implements ChatGrupReset {
  const _$ChatGrupResetImpl();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatgrupEvent.reset()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'ChatgrupEvent.reset'));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ChatGrupResetImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() reset,
    required TResult Function(ChatMessageEntity message) addNewGrupMesage,
    required TResult Function(ChatEntity chat) chatGrupSelected,
    required TResult Function() getGrupChatMessage,
    required TResult Function() loadMoreChatMessage,
    required TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)
        sendMessage,
    required TResult Function(FormData data, String token) sendMedia,
    required TResult Function(int chatId) chatGrupNotificationOpened,
  }) {
    return reset();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? reset,
    TResult? Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult? Function(ChatEntity chat)? chatGrupSelected,
    TResult? Function()? getGrupChatMessage,
    TResult? Function()? loadMoreChatMessage,
    TResult? Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult? Function(FormData data, String token)? sendMedia,
    TResult? Function(int chatId)? chatGrupNotificationOpened,
  }) {
    return reset?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? reset,
    TResult Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult Function(ChatEntity chat)? chatGrupSelected,
    TResult Function()? getGrupChatMessage,
    TResult Function()? loadMoreChatMessage,
    TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult Function(FormData data, String token)? sendMedia,
    TResult Function(int chatId)? chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (reset != null) {
      return reset();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatGrupStarted value) started,
    required TResult Function(ChatGrupReset value) reset,
    required TResult Function(AddNewGrupMessage value) addNewGrupMesage,
    required TResult Function(ChatGrupSelected value) chatGrupSelected,
    required TResult Function(GetGrupChatMessage value) getGrupChatMessage,
    required TResult Function(LoadMoreGrupChatMessage value)
        loadMoreChatMessage,
    required TResult Function(SendGrupMessage value) sendMessage,
    required TResult Function(sendGrupMedia value) sendMedia,
    required TResult Function(ChatGrupNotificationOpened value)
        chatGrupNotificationOpened,
  }) {
    return reset(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatGrupStarted value)? started,
    TResult? Function(ChatGrupReset value)? reset,
    TResult? Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult? Function(ChatGrupSelected value)? chatGrupSelected,
    TResult? Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult? Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult? Function(SendGrupMessage value)? sendMessage,
    TResult? Function(sendGrupMedia value)? sendMedia,
    TResult? Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
  }) {
    return reset?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatGrupStarted value)? started,
    TResult Function(ChatGrupReset value)? reset,
    TResult Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult Function(ChatGrupSelected value)? chatGrupSelected,
    TResult Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult Function(SendGrupMessage value)? sendMessage,
    TResult Function(sendGrupMedia value)? sendMedia,
    TResult Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (reset != null) {
      return reset(this);
    }
    return orElse();
  }
}

abstract class ChatGrupReset implements ChatgrupEvent {
  const factory ChatGrupReset() = _$ChatGrupResetImpl;
}

/// @nodoc
abstract class _$$AddNewGrupMessageImplCopyWith<$Res> {
  factory _$$AddNewGrupMessageImplCopyWith(_$AddNewGrupMessageImpl value,
          $Res Function(_$AddNewGrupMessageImpl) then) =
      __$$AddNewGrupMessageImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ChatMessageEntity message});

  $ChatMessageEntityCopyWith<$Res> get message;
}

/// @nodoc
class __$$AddNewGrupMessageImplCopyWithImpl<$Res>
    extends _$ChatgrupEventCopyWithImpl<$Res, _$AddNewGrupMessageImpl>
    implements _$$AddNewGrupMessageImplCopyWith<$Res> {
  __$$AddNewGrupMessageImplCopyWithImpl(_$AddNewGrupMessageImpl _value,
      $Res Function(_$AddNewGrupMessageImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = null,
  }) {
    return _then(_$AddNewGrupMessageImpl(
      null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as ChatMessageEntity,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $ChatMessageEntityCopyWith<$Res> get message {
    return $ChatMessageEntityCopyWith<$Res>(_value.message, (value) {
      return _then(_value.copyWith(message: value));
    });
  }
}

/// @nodoc

class _$AddNewGrupMessageImpl
    with DiagnosticableTreeMixin
    implements AddNewGrupMessage {
  const _$AddNewGrupMessageImpl(this.message);

  @override
  final ChatMessageEntity message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatgrupEvent.addNewGrupMesage(message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ChatgrupEvent.addNewGrupMesage'))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddNewGrupMessageImpl &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AddNewGrupMessageImplCopyWith<_$AddNewGrupMessageImpl> get copyWith =>
      __$$AddNewGrupMessageImplCopyWithImpl<_$AddNewGrupMessageImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() reset,
    required TResult Function(ChatMessageEntity message) addNewGrupMesage,
    required TResult Function(ChatEntity chat) chatGrupSelected,
    required TResult Function() getGrupChatMessage,
    required TResult Function() loadMoreChatMessage,
    required TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)
        sendMessage,
    required TResult Function(FormData data, String token) sendMedia,
    required TResult Function(int chatId) chatGrupNotificationOpened,
  }) {
    return addNewGrupMesage(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? reset,
    TResult? Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult? Function(ChatEntity chat)? chatGrupSelected,
    TResult? Function()? getGrupChatMessage,
    TResult? Function()? loadMoreChatMessage,
    TResult? Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult? Function(FormData data, String token)? sendMedia,
    TResult? Function(int chatId)? chatGrupNotificationOpened,
  }) {
    return addNewGrupMesage?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? reset,
    TResult Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult Function(ChatEntity chat)? chatGrupSelected,
    TResult Function()? getGrupChatMessage,
    TResult Function()? loadMoreChatMessage,
    TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult Function(FormData data, String token)? sendMedia,
    TResult Function(int chatId)? chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (addNewGrupMesage != null) {
      return addNewGrupMesage(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatGrupStarted value) started,
    required TResult Function(ChatGrupReset value) reset,
    required TResult Function(AddNewGrupMessage value) addNewGrupMesage,
    required TResult Function(ChatGrupSelected value) chatGrupSelected,
    required TResult Function(GetGrupChatMessage value) getGrupChatMessage,
    required TResult Function(LoadMoreGrupChatMessage value)
        loadMoreChatMessage,
    required TResult Function(SendGrupMessage value) sendMessage,
    required TResult Function(sendGrupMedia value) sendMedia,
    required TResult Function(ChatGrupNotificationOpened value)
        chatGrupNotificationOpened,
  }) {
    return addNewGrupMesage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatGrupStarted value)? started,
    TResult? Function(ChatGrupReset value)? reset,
    TResult? Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult? Function(ChatGrupSelected value)? chatGrupSelected,
    TResult? Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult? Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult? Function(SendGrupMessage value)? sendMessage,
    TResult? Function(sendGrupMedia value)? sendMedia,
    TResult? Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
  }) {
    return addNewGrupMesage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatGrupStarted value)? started,
    TResult Function(ChatGrupReset value)? reset,
    TResult Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult Function(ChatGrupSelected value)? chatGrupSelected,
    TResult Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult Function(SendGrupMessage value)? sendMessage,
    TResult Function(sendGrupMedia value)? sendMedia,
    TResult Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (addNewGrupMesage != null) {
      return addNewGrupMesage(this);
    }
    return orElse();
  }
}

abstract class AddNewGrupMessage implements ChatgrupEvent {
  const factory AddNewGrupMessage(final ChatMessageEntity message) =
      _$AddNewGrupMessageImpl;

  ChatMessageEntity get message;
  @JsonKey(ignore: true)
  _$$AddNewGrupMessageImplCopyWith<_$AddNewGrupMessageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ChatGrupSelectedImplCopyWith<$Res> {
  factory _$$ChatGrupSelectedImplCopyWith(_$ChatGrupSelectedImpl value,
          $Res Function(_$ChatGrupSelectedImpl) then) =
      __$$ChatGrupSelectedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ChatEntity chat});

  $ChatEntityCopyWith<$Res> get chat;
}

/// @nodoc
class __$$ChatGrupSelectedImplCopyWithImpl<$Res>
    extends _$ChatgrupEventCopyWithImpl<$Res, _$ChatGrupSelectedImpl>
    implements _$$ChatGrupSelectedImplCopyWith<$Res> {
  __$$ChatGrupSelectedImplCopyWithImpl(_$ChatGrupSelectedImpl _value,
      $Res Function(_$ChatGrupSelectedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chat = null,
  }) {
    return _then(_$ChatGrupSelectedImpl(
      null == chat
          ? _value.chat
          : chat // ignore: cast_nullable_to_non_nullable
              as ChatEntity,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $ChatEntityCopyWith<$Res> get chat {
    return $ChatEntityCopyWith<$Res>(_value.chat, (value) {
      return _then(_value.copyWith(chat: value));
    });
  }
}

/// @nodoc

class _$ChatGrupSelectedImpl
    with DiagnosticableTreeMixin
    implements ChatGrupSelected {
  const _$ChatGrupSelectedImpl(this.chat);

  @override
  final ChatEntity chat;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatgrupEvent.chatGrupSelected(chat: $chat)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ChatgrupEvent.chatGrupSelected'))
      ..add(DiagnosticsProperty('chat', chat));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChatGrupSelectedImpl &&
            (identical(other.chat, chat) || other.chat == chat));
  }

  @override
  int get hashCode => Object.hash(runtimeType, chat);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChatGrupSelectedImplCopyWith<_$ChatGrupSelectedImpl> get copyWith =>
      __$$ChatGrupSelectedImplCopyWithImpl<_$ChatGrupSelectedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() reset,
    required TResult Function(ChatMessageEntity message) addNewGrupMesage,
    required TResult Function(ChatEntity chat) chatGrupSelected,
    required TResult Function() getGrupChatMessage,
    required TResult Function() loadMoreChatMessage,
    required TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)
        sendMessage,
    required TResult Function(FormData data, String token) sendMedia,
    required TResult Function(int chatId) chatGrupNotificationOpened,
  }) {
    return chatGrupSelected(chat);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? reset,
    TResult? Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult? Function(ChatEntity chat)? chatGrupSelected,
    TResult? Function()? getGrupChatMessage,
    TResult? Function()? loadMoreChatMessage,
    TResult? Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult? Function(FormData data, String token)? sendMedia,
    TResult? Function(int chatId)? chatGrupNotificationOpened,
  }) {
    return chatGrupSelected?.call(chat);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? reset,
    TResult Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult Function(ChatEntity chat)? chatGrupSelected,
    TResult Function()? getGrupChatMessage,
    TResult Function()? loadMoreChatMessage,
    TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult Function(FormData data, String token)? sendMedia,
    TResult Function(int chatId)? chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (chatGrupSelected != null) {
      return chatGrupSelected(chat);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatGrupStarted value) started,
    required TResult Function(ChatGrupReset value) reset,
    required TResult Function(AddNewGrupMessage value) addNewGrupMesage,
    required TResult Function(ChatGrupSelected value) chatGrupSelected,
    required TResult Function(GetGrupChatMessage value) getGrupChatMessage,
    required TResult Function(LoadMoreGrupChatMessage value)
        loadMoreChatMessage,
    required TResult Function(SendGrupMessage value) sendMessage,
    required TResult Function(sendGrupMedia value) sendMedia,
    required TResult Function(ChatGrupNotificationOpened value)
        chatGrupNotificationOpened,
  }) {
    return chatGrupSelected(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatGrupStarted value)? started,
    TResult? Function(ChatGrupReset value)? reset,
    TResult? Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult? Function(ChatGrupSelected value)? chatGrupSelected,
    TResult? Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult? Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult? Function(SendGrupMessage value)? sendMessage,
    TResult? Function(sendGrupMedia value)? sendMedia,
    TResult? Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
  }) {
    return chatGrupSelected?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatGrupStarted value)? started,
    TResult Function(ChatGrupReset value)? reset,
    TResult Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult Function(ChatGrupSelected value)? chatGrupSelected,
    TResult Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult Function(SendGrupMessage value)? sendMessage,
    TResult Function(sendGrupMedia value)? sendMedia,
    TResult Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (chatGrupSelected != null) {
      return chatGrupSelected(this);
    }
    return orElse();
  }
}

abstract class ChatGrupSelected implements ChatgrupEvent {
  const factory ChatGrupSelected(final ChatEntity chat) =
      _$ChatGrupSelectedImpl;

  ChatEntity get chat;
  @JsonKey(ignore: true)
  _$$ChatGrupSelectedImplCopyWith<_$ChatGrupSelectedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetGrupChatMessageImplCopyWith<$Res> {
  factory _$$GetGrupChatMessageImplCopyWith(_$GetGrupChatMessageImpl value,
          $Res Function(_$GetGrupChatMessageImpl) then) =
      __$$GetGrupChatMessageImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetGrupChatMessageImplCopyWithImpl<$Res>
    extends _$ChatgrupEventCopyWithImpl<$Res, _$GetGrupChatMessageImpl>
    implements _$$GetGrupChatMessageImplCopyWith<$Res> {
  __$$GetGrupChatMessageImplCopyWithImpl(_$GetGrupChatMessageImpl _value,
      $Res Function(_$GetGrupChatMessageImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetGrupChatMessageImpl
    with DiagnosticableTreeMixin
    implements GetGrupChatMessage {
  const _$GetGrupChatMessageImpl();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatgrupEvent.getGrupChatMessage()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'ChatgrupEvent.getGrupChatMessage'));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetGrupChatMessageImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() reset,
    required TResult Function(ChatMessageEntity message) addNewGrupMesage,
    required TResult Function(ChatEntity chat) chatGrupSelected,
    required TResult Function() getGrupChatMessage,
    required TResult Function() loadMoreChatMessage,
    required TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)
        sendMessage,
    required TResult Function(FormData data, String token) sendMedia,
    required TResult Function(int chatId) chatGrupNotificationOpened,
  }) {
    return getGrupChatMessage();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? reset,
    TResult? Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult? Function(ChatEntity chat)? chatGrupSelected,
    TResult? Function()? getGrupChatMessage,
    TResult? Function()? loadMoreChatMessage,
    TResult? Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult? Function(FormData data, String token)? sendMedia,
    TResult? Function(int chatId)? chatGrupNotificationOpened,
  }) {
    return getGrupChatMessage?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? reset,
    TResult Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult Function(ChatEntity chat)? chatGrupSelected,
    TResult Function()? getGrupChatMessage,
    TResult Function()? loadMoreChatMessage,
    TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult Function(FormData data, String token)? sendMedia,
    TResult Function(int chatId)? chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (getGrupChatMessage != null) {
      return getGrupChatMessage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatGrupStarted value) started,
    required TResult Function(ChatGrupReset value) reset,
    required TResult Function(AddNewGrupMessage value) addNewGrupMesage,
    required TResult Function(ChatGrupSelected value) chatGrupSelected,
    required TResult Function(GetGrupChatMessage value) getGrupChatMessage,
    required TResult Function(LoadMoreGrupChatMessage value)
        loadMoreChatMessage,
    required TResult Function(SendGrupMessage value) sendMessage,
    required TResult Function(sendGrupMedia value) sendMedia,
    required TResult Function(ChatGrupNotificationOpened value)
        chatGrupNotificationOpened,
  }) {
    return getGrupChatMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatGrupStarted value)? started,
    TResult? Function(ChatGrupReset value)? reset,
    TResult? Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult? Function(ChatGrupSelected value)? chatGrupSelected,
    TResult? Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult? Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult? Function(SendGrupMessage value)? sendMessage,
    TResult? Function(sendGrupMedia value)? sendMedia,
    TResult? Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
  }) {
    return getGrupChatMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatGrupStarted value)? started,
    TResult Function(ChatGrupReset value)? reset,
    TResult Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult Function(ChatGrupSelected value)? chatGrupSelected,
    TResult Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult Function(SendGrupMessage value)? sendMessage,
    TResult Function(sendGrupMedia value)? sendMedia,
    TResult Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (getGrupChatMessage != null) {
      return getGrupChatMessage(this);
    }
    return orElse();
  }
}

abstract class GetGrupChatMessage implements ChatgrupEvent {
  const factory GetGrupChatMessage() = _$GetGrupChatMessageImpl;
}

/// @nodoc
abstract class _$$LoadMoreGrupChatMessageImplCopyWith<$Res> {
  factory _$$LoadMoreGrupChatMessageImplCopyWith(
          _$LoadMoreGrupChatMessageImpl value,
          $Res Function(_$LoadMoreGrupChatMessageImpl) then) =
      __$$LoadMoreGrupChatMessageImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadMoreGrupChatMessageImplCopyWithImpl<$Res>
    extends _$ChatgrupEventCopyWithImpl<$Res, _$LoadMoreGrupChatMessageImpl>
    implements _$$LoadMoreGrupChatMessageImplCopyWith<$Res> {
  __$$LoadMoreGrupChatMessageImplCopyWithImpl(
      _$LoadMoreGrupChatMessageImpl _value,
      $Res Function(_$LoadMoreGrupChatMessageImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadMoreGrupChatMessageImpl
    with DiagnosticableTreeMixin
    implements LoadMoreGrupChatMessage {
  const _$LoadMoreGrupChatMessageImpl();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatgrupEvent.loadMoreChatMessage()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'ChatgrupEvent.loadMoreChatMessage'));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadMoreGrupChatMessageImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() reset,
    required TResult Function(ChatMessageEntity message) addNewGrupMesage,
    required TResult Function(ChatEntity chat) chatGrupSelected,
    required TResult Function() getGrupChatMessage,
    required TResult Function() loadMoreChatMessage,
    required TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)
        sendMessage,
    required TResult Function(FormData data, String token) sendMedia,
    required TResult Function(int chatId) chatGrupNotificationOpened,
  }) {
    return loadMoreChatMessage();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? reset,
    TResult? Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult? Function(ChatEntity chat)? chatGrupSelected,
    TResult? Function()? getGrupChatMessage,
    TResult? Function()? loadMoreChatMessage,
    TResult? Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult? Function(FormData data, String token)? sendMedia,
    TResult? Function(int chatId)? chatGrupNotificationOpened,
  }) {
    return loadMoreChatMessage?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? reset,
    TResult Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult Function(ChatEntity chat)? chatGrupSelected,
    TResult Function()? getGrupChatMessage,
    TResult Function()? loadMoreChatMessage,
    TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult Function(FormData data, String token)? sendMedia,
    TResult Function(int chatId)? chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (loadMoreChatMessage != null) {
      return loadMoreChatMessage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatGrupStarted value) started,
    required TResult Function(ChatGrupReset value) reset,
    required TResult Function(AddNewGrupMessage value) addNewGrupMesage,
    required TResult Function(ChatGrupSelected value) chatGrupSelected,
    required TResult Function(GetGrupChatMessage value) getGrupChatMessage,
    required TResult Function(LoadMoreGrupChatMessage value)
        loadMoreChatMessage,
    required TResult Function(SendGrupMessage value) sendMessage,
    required TResult Function(sendGrupMedia value) sendMedia,
    required TResult Function(ChatGrupNotificationOpened value)
        chatGrupNotificationOpened,
  }) {
    return loadMoreChatMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatGrupStarted value)? started,
    TResult? Function(ChatGrupReset value)? reset,
    TResult? Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult? Function(ChatGrupSelected value)? chatGrupSelected,
    TResult? Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult? Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult? Function(SendGrupMessage value)? sendMessage,
    TResult? Function(sendGrupMedia value)? sendMedia,
    TResult? Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
  }) {
    return loadMoreChatMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatGrupStarted value)? started,
    TResult Function(ChatGrupReset value)? reset,
    TResult Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult Function(ChatGrupSelected value)? chatGrupSelected,
    TResult Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult Function(SendGrupMessage value)? sendMessage,
    TResult Function(sendGrupMedia value)? sendMedia,
    TResult Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (loadMoreChatMessage != null) {
      return loadMoreChatMessage(this);
    }
    return orElse();
  }
}

abstract class LoadMoreGrupChatMessage implements ChatgrupEvent {
  const factory LoadMoreGrupChatMessage() = _$LoadMoreGrupChatMessageImpl;
}

/// @nodoc
abstract class _$$SendGrupMessageImplCopyWith<$Res> {
  factory _$$SendGrupMessageImplCopyWith(_$SendGrupMessageImpl value,
          $Res Function(_$SendGrupMessageImpl) then) =
      __$$SendGrupMessageImplCopyWithImpl<$Res>;
  @useResult
  $Res call({int chatId, ChatMessage message, String? ivkey, String socketId});
}

/// @nodoc
class __$$SendGrupMessageImplCopyWithImpl<$Res>
    extends _$ChatgrupEventCopyWithImpl<$Res, _$SendGrupMessageImpl>
    implements _$$SendGrupMessageImplCopyWith<$Res> {
  __$$SendGrupMessageImplCopyWithImpl(
      _$SendGrupMessageImpl _value, $Res Function(_$SendGrupMessageImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatId = null,
    Object? message = null,
    Object? ivkey = freezed,
    Object? socketId = null,
  }) {
    return _then(_$SendGrupMessageImpl(
      null == chatId
          ? _value.chatId
          : chatId // ignore: cast_nullable_to_non_nullable
              as int,
      null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as ChatMessage,
      freezed == ivkey
          ? _value.ivkey
          : ivkey // ignore: cast_nullable_to_non_nullable
              as String?,
      socketId: null == socketId
          ? _value.socketId
          : socketId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SendGrupMessageImpl
    with DiagnosticableTreeMixin
    implements SendGrupMessage {
  const _$SendGrupMessageImpl(this.chatId, this.message, this.ivkey,
      {required this.socketId});

  @override
  final int chatId;
  @override
  final ChatMessage message;
  @override
  final String? ivkey;
  @override
  final String socketId;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatgrupEvent.sendMessage(chatId: $chatId, message: $message, ivkey: $ivkey, socketId: $socketId)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ChatgrupEvent.sendMessage'))
      ..add(DiagnosticsProperty('chatId', chatId))
      ..add(DiagnosticsProperty('message', message))
      ..add(DiagnosticsProperty('ivkey', ivkey))
      ..add(DiagnosticsProperty('socketId', socketId));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SendGrupMessageImpl &&
            (identical(other.chatId, chatId) || other.chatId == chatId) &&
            (identical(other.message, message) || other.message == message) &&
            (identical(other.ivkey, ivkey) || other.ivkey == ivkey) &&
            (identical(other.socketId, socketId) ||
                other.socketId == socketId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, chatId, message, ivkey, socketId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SendGrupMessageImplCopyWith<_$SendGrupMessageImpl> get copyWith =>
      __$$SendGrupMessageImplCopyWithImpl<_$SendGrupMessageImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() reset,
    required TResult Function(ChatMessageEntity message) addNewGrupMesage,
    required TResult Function(ChatEntity chat) chatGrupSelected,
    required TResult Function() getGrupChatMessage,
    required TResult Function() loadMoreChatMessage,
    required TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)
        sendMessage,
    required TResult Function(FormData data, String token) sendMedia,
    required TResult Function(int chatId) chatGrupNotificationOpened,
  }) {
    return sendMessage(chatId, message, ivkey, socketId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? reset,
    TResult? Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult? Function(ChatEntity chat)? chatGrupSelected,
    TResult? Function()? getGrupChatMessage,
    TResult? Function()? loadMoreChatMessage,
    TResult? Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult? Function(FormData data, String token)? sendMedia,
    TResult? Function(int chatId)? chatGrupNotificationOpened,
  }) {
    return sendMessage?.call(chatId, message, ivkey, socketId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? reset,
    TResult Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult Function(ChatEntity chat)? chatGrupSelected,
    TResult Function()? getGrupChatMessage,
    TResult Function()? loadMoreChatMessage,
    TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult Function(FormData data, String token)? sendMedia,
    TResult Function(int chatId)? chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (sendMessage != null) {
      return sendMessage(chatId, message, ivkey, socketId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatGrupStarted value) started,
    required TResult Function(ChatGrupReset value) reset,
    required TResult Function(AddNewGrupMessage value) addNewGrupMesage,
    required TResult Function(ChatGrupSelected value) chatGrupSelected,
    required TResult Function(GetGrupChatMessage value) getGrupChatMessage,
    required TResult Function(LoadMoreGrupChatMessage value)
        loadMoreChatMessage,
    required TResult Function(SendGrupMessage value) sendMessage,
    required TResult Function(sendGrupMedia value) sendMedia,
    required TResult Function(ChatGrupNotificationOpened value)
        chatGrupNotificationOpened,
  }) {
    return sendMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatGrupStarted value)? started,
    TResult? Function(ChatGrupReset value)? reset,
    TResult? Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult? Function(ChatGrupSelected value)? chatGrupSelected,
    TResult? Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult? Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult? Function(SendGrupMessage value)? sendMessage,
    TResult? Function(sendGrupMedia value)? sendMedia,
    TResult? Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
  }) {
    return sendMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatGrupStarted value)? started,
    TResult Function(ChatGrupReset value)? reset,
    TResult Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult Function(ChatGrupSelected value)? chatGrupSelected,
    TResult Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult Function(SendGrupMessage value)? sendMessage,
    TResult Function(sendGrupMedia value)? sendMedia,
    TResult Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (sendMessage != null) {
      return sendMessage(this);
    }
    return orElse();
  }
}

abstract class SendGrupMessage implements ChatgrupEvent {
  const factory SendGrupMessage(
      final int chatId, final ChatMessage message, final String? ivkey,
      {required final String socketId}) = _$SendGrupMessageImpl;

  int get chatId;
  ChatMessage get message;
  String? get ivkey;
  String get socketId;
  @JsonKey(ignore: true)
  _$$SendGrupMessageImplCopyWith<_$SendGrupMessageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$sendGrupMediaImplCopyWith<$Res> {
  factory _$$sendGrupMediaImplCopyWith(
          _$sendGrupMediaImpl value, $Res Function(_$sendGrupMediaImpl) then) =
      __$$sendGrupMediaImplCopyWithImpl<$Res>;
  @useResult
  $Res call({FormData data, String token});
}

/// @nodoc
class __$$sendGrupMediaImplCopyWithImpl<$Res>
    extends _$ChatgrupEventCopyWithImpl<$Res, _$sendGrupMediaImpl>
    implements _$$sendGrupMediaImplCopyWith<$Res> {
  __$$sendGrupMediaImplCopyWithImpl(
      _$sendGrupMediaImpl _value, $Res Function(_$sendGrupMediaImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = null,
    Object? token = null,
  }) {
    return _then(_$sendGrupMediaImpl(
      null == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as FormData,
      null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$sendGrupMediaImpl
    with DiagnosticableTreeMixin
    implements sendGrupMedia {
  const _$sendGrupMediaImpl(this.data, this.token);

  @override
  final FormData data;
  @override
  final String token;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatgrupEvent.sendMedia(data: $data, token: $token)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ChatgrupEvent.sendMedia'))
      ..add(DiagnosticsProperty('data', data))
      ..add(DiagnosticsProperty('token', token));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$sendGrupMediaImpl &&
            (identical(other.data, data) || other.data == data) &&
            (identical(other.token, token) || other.token == token));
  }

  @override
  int get hashCode => Object.hash(runtimeType, data, token);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$sendGrupMediaImplCopyWith<_$sendGrupMediaImpl> get copyWith =>
      __$$sendGrupMediaImplCopyWithImpl<_$sendGrupMediaImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() reset,
    required TResult Function(ChatMessageEntity message) addNewGrupMesage,
    required TResult Function(ChatEntity chat) chatGrupSelected,
    required TResult Function() getGrupChatMessage,
    required TResult Function() loadMoreChatMessage,
    required TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)
        sendMessage,
    required TResult Function(FormData data, String token) sendMedia,
    required TResult Function(int chatId) chatGrupNotificationOpened,
  }) {
    return sendMedia(data, token);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? reset,
    TResult? Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult? Function(ChatEntity chat)? chatGrupSelected,
    TResult? Function()? getGrupChatMessage,
    TResult? Function()? loadMoreChatMessage,
    TResult? Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult? Function(FormData data, String token)? sendMedia,
    TResult? Function(int chatId)? chatGrupNotificationOpened,
  }) {
    return sendMedia?.call(data, token);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? reset,
    TResult Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult Function(ChatEntity chat)? chatGrupSelected,
    TResult Function()? getGrupChatMessage,
    TResult Function()? loadMoreChatMessage,
    TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult Function(FormData data, String token)? sendMedia,
    TResult Function(int chatId)? chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (sendMedia != null) {
      return sendMedia(data, token);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatGrupStarted value) started,
    required TResult Function(ChatGrupReset value) reset,
    required TResult Function(AddNewGrupMessage value) addNewGrupMesage,
    required TResult Function(ChatGrupSelected value) chatGrupSelected,
    required TResult Function(GetGrupChatMessage value) getGrupChatMessage,
    required TResult Function(LoadMoreGrupChatMessage value)
        loadMoreChatMessage,
    required TResult Function(SendGrupMessage value) sendMessage,
    required TResult Function(sendGrupMedia value) sendMedia,
    required TResult Function(ChatGrupNotificationOpened value)
        chatGrupNotificationOpened,
  }) {
    return sendMedia(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatGrupStarted value)? started,
    TResult? Function(ChatGrupReset value)? reset,
    TResult? Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult? Function(ChatGrupSelected value)? chatGrupSelected,
    TResult? Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult? Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult? Function(SendGrupMessage value)? sendMessage,
    TResult? Function(sendGrupMedia value)? sendMedia,
    TResult? Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
  }) {
    return sendMedia?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatGrupStarted value)? started,
    TResult Function(ChatGrupReset value)? reset,
    TResult Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult Function(ChatGrupSelected value)? chatGrupSelected,
    TResult Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult Function(SendGrupMessage value)? sendMessage,
    TResult Function(sendGrupMedia value)? sendMedia,
    TResult Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (sendMedia != null) {
      return sendMedia(this);
    }
    return orElse();
  }
}

abstract class sendGrupMedia implements ChatgrupEvent {
  const factory sendGrupMedia(final FormData data, final String token) =
      _$sendGrupMediaImpl;

  FormData get data;
  String get token;
  @JsonKey(ignore: true)
  _$$sendGrupMediaImplCopyWith<_$sendGrupMediaImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ChatGrupNotificationOpenedImplCopyWith<$Res> {
  factory _$$ChatGrupNotificationOpenedImplCopyWith(
          _$ChatGrupNotificationOpenedImpl value,
          $Res Function(_$ChatGrupNotificationOpenedImpl) then) =
      __$$ChatGrupNotificationOpenedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({int chatId});
}

/// @nodoc
class __$$ChatGrupNotificationOpenedImplCopyWithImpl<$Res>
    extends _$ChatgrupEventCopyWithImpl<$Res, _$ChatGrupNotificationOpenedImpl>
    implements _$$ChatGrupNotificationOpenedImplCopyWith<$Res> {
  __$$ChatGrupNotificationOpenedImplCopyWithImpl(
      _$ChatGrupNotificationOpenedImpl _value,
      $Res Function(_$ChatGrupNotificationOpenedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatId = null,
  }) {
    return _then(_$ChatGrupNotificationOpenedImpl(
      null == chatId
          ? _value.chatId
          : chatId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$ChatGrupNotificationOpenedImpl
    with DiagnosticableTreeMixin
    implements ChatGrupNotificationOpened {
  const _$ChatGrupNotificationOpenedImpl(this.chatId);

  @override
  final int chatId;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatgrupEvent.chatGrupNotificationOpened(chatId: $chatId)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty(
          'type', 'ChatgrupEvent.chatGrupNotificationOpened'))
      ..add(DiagnosticsProperty('chatId', chatId));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChatGrupNotificationOpenedImpl &&
            (identical(other.chatId, chatId) || other.chatId == chatId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, chatId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChatGrupNotificationOpenedImplCopyWith<_$ChatGrupNotificationOpenedImpl>
      get copyWith => __$$ChatGrupNotificationOpenedImplCopyWithImpl<
          _$ChatGrupNotificationOpenedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() reset,
    required TResult Function(ChatMessageEntity message) addNewGrupMesage,
    required TResult Function(ChatEntity chat) chatGrupSelected,
    required TResult Function() getGrupChatMessage,
    required TResult Function() loadMoreChatMessage,
    required TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)
        sendMessage,
    required TResult Function(FormData data, String token) sendMedia,
    required TResult Function(int chatId) chatGrupNotificationOpened,
  }) {
    return chatGrupNotificationOpened(chatId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? reset,
    TResult? Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult? Function(ChatEntity chat)? chatGrupSelected,
    TResult? Function()? getGrupChatMessage,
    TResult? Function()? loadMoreChatMessage,
    TResult? Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult? Function(FormData data, String token)? sendMedia,
    TResult? Function(int chatId)? chatGrupNotificationOpened,
  }) {
    return chatGrupNotificationOpened?.call(chatId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? reset,
    TResult Function(ChatMessageEntity message)? addNewGrupMesage,
    TResult Function(ChatEntity chat)? chatGrupSelected,
    TResult Function()? getGrupChatMessage,
    TResult Function()? loadMoreChatMessage,
    TResult Function(
            int chatId, ChatMessage message, String? ivkey, String socketId)?
        sendMessage,
    TResult Function(FormData data, String token)? sendMedia,
    TResult Function(int chatId)? chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (chatGrupNotificationOpened != null) {
      return chatGrupNotificationOpened(chatId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatGrupStarted value) started,
    required TResult Function(ChatGrupReset value) reset,
    required TResult Function(AddNewGrupMessage value) addNewGrupMesage,
    required TResult Function(ChatGrupSelected value) chatGrupSelected,
    required TResult Function(GetGrupChatMessage value) getGrupChatMessage,
    required TResult Function(LoadMoreGrupChatMessage value)
        loadMoreChatMessage,
    required TResult Function(SendGrupMessage value) sendMessage,
    required TResult Function(sendGrupMedia value) sendMedia,
    required TResult Function(ChatGrupNotificationOpened value)
        chatGrupNotificationOpened,
  }) {
    return chatGrupNotificationOpened(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatGrupStarted value)? started,
    TResult? Function(ChatGrupReset value)? reset,
    TResult? Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult? Function(ChatGrupSelected value)? chatGrupSelected,
    TResult? Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult? Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult? Function(SendGrupMessage value)? sendMessage,
    TResult? Function(sendGrupMedia value)? sendMedia,
    TResult? Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
  }) {
    return chatGrupNotificationOpened?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatGrupStarted value)? started,
    TResult Function(ChatGrupReset value)? reset,
    TResult Function(AddNewGrupMessage value)? addNewGrupMesage,
    TResult Function(ChatGrupSelected value)? chatGrupSelected,
    TResult Function(GetGrupChatMessage value)? getGrupChatMessage,
    TResult Function(LoadMoreGrupChatMessage value)? loadMoreChatMessage,
    TResult Function(SendGrupMessage value)? sendMessage,
    TResult Function(sendGrupMedia value)? sendMedia,
    TResult Function(ChatGrupNotificationOpened value)?
        chatGrupNotificationOpened,
    required TResult orElse(),
  }) {
    if (chatGrupNotificationOpened != null) {
      return chatGrupNotificationOpened(this);
    }
    return orElse();
  }
}

abstract class ChatGrupNotificationOpened implements ChatgrupEvent {
  const factory ChatGrupNotificationOpened(final int chatId) =
      _$ChatGrupNotificationOpenedImpl;

  int get chatId;
  @JsonKey(ignore: true)
  _$$ChatGrupNotificationOpenedImplCopyWith<_$ChatGrupNotificationOpenedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ChatgrupState {
  List<ChatEntity> get chats => throw _privateConstructorUsedError;
  List<ChatMessageEntity> get chatMessages =>
      throw _privateConstructorUsedError;
  ChatEntity? get selectedChat => throw _privateConstructorUsedError;
  DataStatus get status => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;
  Array<NativeType>? get otherUserId => throw _privateConstructorUsedError;
  bool get isLastPage => throw _privateConstructorUsedError;
  int get page => throw _privateConstructorUsedError;
  int? get notificationChatId => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ChatgrupStateCopyWith<ChatgrupState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatgrupStateCopyWith<$Res> {
  factory $ChatgrupStateCopyWith(
          ChatgrupState value, $Res Function(ChatgrupState) then) =
      _$ChatgrupStateCopyWithImpl<$Res, ChatgrupState>;
  @useResult
  $Res call(
      {List<ChatEntity> chats,
      List<ChatMessageEntity> chatMessages,
      ChatEntity? selectedChat,
      DataStatus status,
      String message,
      Array<NativeType>? otherUserId,
      bool isLastPage,
      int page,
      int? notificationChatId});

  $ChatEntityCopyWith<$Res>? get selectedChat;
}

/// @nodoc
class _$ChatgrupStateCopyWithImpl<$Res, $Val extends ChatgrupState>
    implements $ChatgrupStateCopyWith<$Res> {
  _$ChatgrupStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chats = null,
    Object? chatMessages = null,
    Object? selectedChat = freezed,
    Object? status = null,
    Object? message = null,
    Object? otherUserId = freezed,
    Object? isLastPage = null,
    Object? page = null,
    Object? notificationChatId = freezed,
  }) {
    return _then(_value.copyWith(
      chats: null == chats
          ? _value.chats
          : chats // ignore: cast_nullable_to_non_nullable
              as List<ChatEntity>,
      chatMessages: null == chatMessages
          ? _value.chatMessages
          : chatMessages // ignore: cast_nullable_to_non_nullable
              as List<ChatMessageEntity>,
      selectedChat: freezed == selectedChat
          ? _value.selectedChat
          : selectedChat // ignore: cast_nullable_to_non_nullable
              as ChatEntity?,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as DataStatus,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      otherUserId: freezed == otherUserId
          ? _value.otherUserId
          : otherUserId // ignore: cast_nullable_to_non_nullable
              as Array<NativeType>?,
      isLastPage: null == isLastPage
          ? _value.isLastPage
          : isLastPage // ignore: cast_nullable_to_non_nullable
              as bool,
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      notificationChatId: freezed == notificationChatId
          ? _value.notificationChatId
          : notificationChatId // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ChatEntityCopyWith<$Res>? get selectedChat {
    if (_value.selectedChat == null) {
      return null;
    }

    return $ChatEntityCopyWith<$Res>(_value.selectedChat!, (value) {
      return _then(_value.copyWith(selectedChat: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$ChatgrupStateImplCopyWith<$Res>
    implements $ChatgrupStateCopyWith<$Res> {
  factory _$$ChatgrupStateImplCopyWith(
          _$ChatgrupStateImpl value, $Res Function(_$ChatgrupStateImpl) then) =
      __$$ChatgrupStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<ChatEntity> chats,
      List<ChatMessageEntity> chatMessages,
      ChatEntity? selectedChat,
      DataStatus status,
      String message,
      Array<NativeType>? otherUserId,
      bool isLastPage,
      int page,
      int? notificationChatId});

  @override
  $ChatEntityCopyWith<$Res>? get selectedChat;
}

/// @nodoc
class __$$ChatgrupStateImplCopyWithImpl<$Res>
    extends _$ChatgrupStateCopyWithImpl<$Res, _$ChatgrupStateImpl>
    implements _$$ChatgrupStateImplCopyWith<$Res> {
  __$$ChatgrupStateImplCopyWithImpl(
      _$ChatgrupStateImpl _value, $Res Function(_$ChatgrupStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chats = null,
    Object? chatMessages = null,
    Object? selectedChat = freezed,
    Object? status = null,
    Object? message = null,
    Object? otherUserId = freezed,
    Object? isLastPage = null,
    Object? page = null,
    Object? notificationChatId = freezed,
  }) {
    return _then(_$ChatgrupStateImpl(
      chats: null == chats
          ? _value._chats
          : chats // ignore: cast_nullable_to_non_nullable
              as List<ChatEntity>,
      chatMessages: null == chatMessages
          ? _value._chatMessages
          : chatMessages // ignore: cast_nullable_to_non_nullable
              as List<ChatMessageEntity>,
      selectedChat: freezed == selectedChat
          ? _value.selectedChat
          : selectedChat // ignore: cast_nullable_to_non_nullable
              as ChatEntity?,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as DataStatus,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      otherUserId: freezed == otherUserId
          ? _value.otherUserId
          : otherUserId // ignore: cast_nullable_to_non_nullable
              as Array<NativeType>?,
      isLastPage: null == isLastPage
          ? _value.isLastPage
          : isLastPage // ignore: cast_nullable_to_non_nullable
              as bool,
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      notificationChatId: freezed == notificationChatId
          ? _value.notificationChatId
          : notificationChatId // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$ChatgrupStateImpl extends _ChatgrupState with DiagnosticableTreeMixin {
  const _$ChatgrupStateImpl(
      {required final List<ChatEntity> chats,
      required final List<ChatMessageEntity> chatMessages,
      this.selectedChat,
      required this.status,
      required this.message,
      this.otherUserId,
      required this.isLastPage,
      required this.page,
      this.notificationChatId})
      : _chats = chats,
        _chatMessages = chatMessages,
        super._();

  final List<ChatEntity> _chats;
  @override
  List<ChatEntity> get chats {
    if (_chats is EqualUnmodifiableListView) return _chats;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_chats);
  }

  final List<ChatMessageEntity> _chatMessages;
  @override
  List<ChatMessageEntity> get chatMessages {
    if (_chatMessages is EqualUnmodifiableListView) return _chatMessages;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_chatMessages);
  }

  @override
  final ChatEntity? selectedChat;
  @override
  final DataStatus status;
  @override
  final String message;
  @override
  final Array<NativeType>? otherUserId;
  @override
  final bool isLastPage;
  @override
  final int page;
  @override
  final int? notificationChatId;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatgrupState(chats: $chats, chatMessages: $chatMessages, selectedChat: $selectedChat, status: $status, message: $message, otherUserId: $otherUserId, isLastPage: $isLastPage, page: $page, notificationChatId: $notificationChatId)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ChatgrupState'))
      ..add(DiagnosticsProperty('chats', chats))
      ..add(DiagnosticsProperty('chatMessages', chatMessages))
      ..add(DiagnosticsProperty('selectedChat', selectedChat))
      ..add(DiagnosticsProperty('status', status))
      ..add(DiagnosticsProperty('message', message))
      ..add(DiagnosticsProperty('otherUserId', otherUserId))
      ..add(DiagnosticsProperty('isLastPage', isLastPage))
      ..add(DiagnosticsProperty('page', page))
      ..add(DiagnosticsProperty('notificationChatId', notificationChatId));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChatgrupStateImpl &&
            const DeepCollectionEquality().equals(other._chats, _chats) &&
            const DeepCollectionEquality()
                .equals(other._chatMessages, _chatMessages) &&
            (identical(other.selectedChat, selectedChat) ||
                other.selectedChat == selectedChat) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.message, message) || other.message == message) &&
            (identical(other.otherUserId, otherUserId) ||
                other.otherUserId == otherUserId) &&
            (identical(other.isLastPage, isLastPage) ||
                other.isLastPage == isLastPage) &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.notificationChatId, notificationChatId) ||
                other.notificationChatId == notificationChatId));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_chats),
      const DeepCollectionEquality().hash(_chatMessages),
      selectedChat,
      status,
      message,
      otherUserId,
      isLastPage,
      page,
      notificationChatId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChatgrupStateImplCopyWith<_$ChatgrupStateImpl> get copyWith =>
      __$$ChatgrupStateImplCopyWithImpl<_$ChatgrupStateImpl>(this, _$identity);
}

abstract class _ChatgrupState extends ChatgrupState {
  const factory _ChatgrupState(
      {required final List<ChatEntity> chats,
      required final List<ChatMessageEntity> chatMessages,
      final ChatEntity? selectedChat,
      required final DataStatus status,
      required final String message,
      final Array<NativeType>? otherUserId,
      required final bool isLastPage,
      required final int page,
      final int? notificationChatId}) = _$ChatgrupStateImpl;
  const _ChatgrupState._() : super._();

  @override
  List<ChatEntity> get chats;
  @override
  List<ChatMessageEntity> get chatMessages;
  @override
  ChatEntity? get selectedChat;
  @override
  DataStatus get status;
  @override
  String get message;
  @override
  Array<NativeType>? get otherUserId;
  @override
  bool get isLastPage;
  @override
  int get page;
  @override
  int? get notificationChatId;
  @override
  @JsonKey(ignore: true)
  _$$ChatgrupStateImplCopyWith<_$ChatgrupStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
