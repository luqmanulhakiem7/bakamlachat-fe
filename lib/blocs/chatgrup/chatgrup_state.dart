part of 'chatgrup_bloc.dart';

@freezed
class ChatgrupState with _$ChatgrupState {
  const ChatgrupState._();

  const factory ChatgrupState({
    required List<ChatEntity> chats,
    required List<ChatMessageEntity> chatMessages,
    ChatEntity? selectedChat,
    required DataStatus status,
    required String message,
    Array? otherUserId,
    required bool isLastPage,
    required int page,
    int? notificationChatId,
  }) = _ChatgrupState;

  factory ChatgrupState.initial() {
    return const ChatgrupState(
      chats: [],
      selectedChat: null,
      status: DataStatus.initial,
      message: "",
      otherUserId: null,
      chatMessages: [],
      isLastPage: false,
      page: 1,
      notificationChatId: null,
    );
  }

  List<ChatMessage> get uiChatMessages {
    return chatMessages.map((e) => e.toChatMessage).toList();
  }
}
