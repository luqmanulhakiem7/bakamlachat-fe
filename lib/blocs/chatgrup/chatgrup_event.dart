part of 'chatgrup_bloc.dart';

@freezed
class ChatgrupEvent with _$ChatgrupEvent {
  const factory ChatgrupEvent.started() = ChatGrupStarted;
  const factory ChatgrupEvent.reset() = ChatGrupReset;
  const factory ChatgrupEvent.addNewGrupMesage(ChatMessageEntity message) =
      AddNewGrupMessage;
  const factory ChatgrupEvent.chatGrupSelected(ChatEntity chat) =
      ChatGrupSelected;
  const factory ChatgrupEvent.getGrupChatMessage() = GetGrupChatMessage;
  const factory ChatgrupEvent.loadMoreChatMessage() = LoadMoreGrupChatMessage;
  const factory ChatgrupEvent.sendMessage(
      int chatId, ChatMessage message, String? ivkey,
      {required String socketId}) = SendGrupMessage;
  const factory ChatgrupEvent.sendMedia(FormData data, String token) =
      sendGrupMedia;
  const factory ChatgrupEvent.chatGrupNotificationOpened(int chatId) =
      ChatGrupNotificationOpened;
}
