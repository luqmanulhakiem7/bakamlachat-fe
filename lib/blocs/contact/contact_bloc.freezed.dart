// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'contact_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ContactEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getContact,
    required TResult Function(int id) addContact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getContact,
    TResult? Function(int id)? addContact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getContact,
    TResult Function(int id)? addContact,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(getContact value) getContact,
    required TResult Function(addContact value) addContact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(getContact value)? getContact,
    TResult? Function(addContact value)? addContact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(getContact value)? getContact,
    TResult Function(addContact value)? addContact,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ContactEventCopyWith<$Res> {
  factory $ContactEventCopyWith(
          ContactEvent value, $Res Function(ContactEvent) then) =
      _$ContactEventCopyWithImpl<$Res, ContactEvent>;
}

/// @nodoc
class _$ContactEventCopyWithImpl<$Res, $Val extends ContactEvent>
    implements $ContactEventCopyWith<$Res> {
  _$ContactEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$getContactImplCopyWith<$Res> {
  factory _$$getContactImplCopyWith(
          _$getContactImpl value, $Res Function(_$getContactImpl) then) =
      __$$getContactImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$getContactImplCopyWithImpl<$Res>
    extends _$ContactEventCopyWithImpl<$Res, _$getContactImpl>
    implements _$$getContactImplCopyWith<$Res> {
  __$$getContactImplCopyWithImpl(
      _$getContactImpl _value, $Res Function(_$getContactImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$getContactImpl implements getContact {
  const _$getContactImpl();

  @override
  String toString() {
    return 'ContactEvent.getContact()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$getContactImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getContact,
    required TResult Function(int id) addContact,
  }) {
    return getContact();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getContact,
    TResult? Function(int id)? addContact,
  }) {
    return getContact?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getContact,
    TResult Function(int id)? addContact,
    required TResult orElse(),
  }) {
    if (getContact != null) {
      return getContact();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(getContact value) getContact,
    required TResult Function(addContact value) addContact,
  }) {
    return getContact(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(getContact value)? getContact,
    TResult? Function(addContact value)? addContact,
  }) {
    return getContact?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(getContact value)? getContact,
    TResult Function(addContact value)? addContact,
    required TResult orElse(),
  }) {
    if (getContact != null) {
      return getContact(this);
    }
    return orElse();
  }
}

abstract class getContact implements ContactEvent {
  const factory getContact() = _$getContactImpl;
}

/// @nodoc
abstract class _$$addContactImplCopyWith<$Res> {
  factory _$$addContactImplCopyWith(
          _$addContactImpl value, $Res Function(_$addContactImpl) then) =
      __$$addContactImplCopyWithImpl<$Res>;
  @useResult
  $Res call({int id});
}

/// @nodoc
class __$$addContactImplCopyWithImpl<$Res>
    extends _$ContactEventCopyWithImpl<$Res, _$addContactImpl>
    implements _$$addContactImplCopyWith<$Res> {
  __$$addContactImplCopyWithImpl(
      _$addContactImpl _value, $Res Function(_$addContactImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$addContactImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$addContactImpl implements addContact {
  const _$addContactImpl({required this.id});

  @override
  final int id;

  @override
  String toString() {
    return 'ContactEvent.addContact(id: $id)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$addContactImpl &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$addContactImplCopyWith<_$addContactImpl> get copyWith =>
      __$$addContactImplCopyWithImpl<_$addContactImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getContact,
    required TResult Function(int id) addContact,
  }) {
    return addContact(id);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getContact,
    TResult? Function(int id)? addContact,
  }) {
    return addContact?.call(id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getContact,
    TResult Function(int id)? addContact,
    required TResult orElse(),
  }) {
    if (addContact != null) {
      return addContact(id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(getContact value) getContact,
    required TResult Function(addContact value) addContact,
  }) {
    return addContact(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(getContact value)? getContact,
    TResult? Function(addContact value)? addContact,
  }) {
    return addContact?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(getContact value)? getContact,
    TResult Function(addContact value)? addContact,
    required TResult orElse(),
  }) {
    if (addContact != null) {
      return addContact(this);
    }
    return orElse();
  }
}

abstract class addContact implements ContactEvent {
  const factory addContact({required final int id}) = _$addContactImpl;

  int get id;
  @JsonKey(ignore: true)
  _$$addContactImplCopyWith<_$addContactImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ContactState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(ContactEntity? contact) loaded,
    required TResult Function(String message) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(ContactEntity? contact)? loaded,
    TResult? Function(String message)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContactEntity? contact)? loaded,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ContactInitial value) initial,
    required TResult Function(ContactLoading value) loading,
    required TResult Function(ContactLoaded value) loaded,
    required TResult Function(ContactError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ContactInitial value)? initial,
    TResult? Function(ContactLoading value)? loading,
    TResult? Function(ContactLoaded value)? loaded,
    TResult? Function(ContactError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ContactInitial value)? initial,
    TResult Function(ContactLoading value)? loading,
    TResult Function(ContactLoaded value)? loaded,
    TResult Function(ContactError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ContactStateCopyWith<$Res> {
  factory $ContactStateCopyWith(
          ContactState value, $Res Function(ContactState) then) =
      _$ContactStateCopyWithImpl<$Res, ContactState>;
}

/// @nodoc
class _$ContactStateCopyWithImpl<$Res, $Val extends ContactState>
    implements $ContactStateCopyWith<$Res> {
  _$ContactStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ContactInitialImplCopyWith<$Res> {
  factory _$$ContactInitialImplCopyWith(_$ContactInitialImpl value,
          $Res Function(_$ContactInitialImpl) then) =
      __$$ContactInitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ContactInitialImplCopyWithImpl<$Res>
    extends _$ContactStateCopyWithImpl<$Res, _$ContactInitialImpl>
    implements _$$ContactInitialImplCopyWith<$Res> {
  __$$ContactInitialImplCopyWithImpl(
      _$ContactInitialImpl _value, $Res Function(_$ContactInitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ContactInitialImpl implements ContactInitial {
  const _$ContactInitialImpl();

  @override
  String toString() {
    return 'ContactState.initial()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ContactInitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(ContactEntity? contact) loaded,
    required TResult Function(String message) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(ContactEntity? contact)? loaded,
    TResult? Function(String message)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContactEntity? contact)? loaded,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ContactInitial value) initial,
    required TResult Function(ContactLoading value) loading,
    required TResult Function(ContactLoaded value) loaded,
    required TResult Function(ContactError value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ContactInitial value)? initial,
    TResult? Function(ContactLoading value)? loading,
    TResult? Function(ContactLoaded value)? loaded,
    TResult? Function(ContactError value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ContactInitial value)? initial,
    TResult Function(ContactLoading value)? loading,
    TResult Function(ContactLoaded value)? loaded,
    TResult Function(ContactError value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class ContactInitial implements ContactState {
  const factory ContactInitial() = _$ContactInitialImpl;
}

/// @nodoc
abstract class _$$ContactLoadingImplCopyWith<$Res> {
  factory _$$ContactLoadingImplCopyWith(_$ContactLoadingImpl value,
          $Res Function(_$ContactLoadingImpl) then) =
      __$$ContactLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ContactLoadingImplCopyWithImpl<$Res>
    extends _$ContactStateCopyWithImpl<$Res, _$ContactLoadingImpl>
    implements _$$ContactLoadingImplCopyWith<$Res> {
  __$$ContactLoadingImplCopyWithImpl(
      _$ContactLoadingImpl _value, $Res Function(_$ContactLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ContactLoadingImpl implements ContactLoading {
  const _$ContactLoadingImpl();

  @override
  String toString() {
    return 'ContactState.loading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ContactLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(ContactEntity? contact) loaded,
    required TResult Function(String message) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(ContactEntity? contact)? loaded,
    TResult? Function(String message)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContactEntity? contact)? loaded,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ContactInitial value) initial,
    required TResult Function(ContactLoading value) loading,
    required TResult Function(ContactLoaded value) loaded,
    required TResult Function(ContactError value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ContactInitial value)? initial,
    TResult? Function(ContactLoading value)? loading,
    TResult? Function(ContactLoaded value)? loaded,
    TResult? Function(ContactError value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ContactInitial value)? initial,
    TResult Function(ContactLoading value)? loading,
    TResult Function(ContactLoaded value)? loaded,
    TResult Function(ContactError value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class ContactLoading implements ContactState {
  const factory ContactLoading() = _$ContactLoadingImpl;
}

/// @nodoc
abstract class _$$ContactLoadedImplCopyWith<$Res> {
  factory _$$ContactLoadedImplCopyWith(
          _$ContactLoadedImpl value, $Res Function(_$ContactLoadedImpl) then) =
      __$$ContactLoadedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ContactEntity? contact});

  $ContactEntityCopyWith<$Res>? get contact;
}

/// @nodoc
class __$$ContactLoadedImplCopyWithImpl<$Res>
    extends _$ContactStateCopyWithImpl<$Res, _$ContactLoadedImpl>
    implements _$$ContactLoadedImplCopyWith<$Res> {
  __$$ContactLoadedImplCopyWithImpl(
      _$ContactLoadedImpl _value, $Res Function(_$ContactLoadedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? contact = freezed,
  }) {
    return _then(_$ContactLoadedImpl(
      freezed == contact
          ? _value.contact
          : contact // ignore: cast_nullable_to_non_nullable
              as ContactEntity?,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $ContactEntityCopyWith<$Res>? get contact {
    if (_value.contact == null) {
      return null;
    }

    return $ContactEntityCopyWith<$Res>(_value.contact!, (value) {
      return _then(_value.copyWith(contact: value));
    });
  }
}

/// @nodoc

class _$ContactLoadedImpl implements ContactLoaded {
  const _$ContactLoadedImpl(this.contact);

  @override
  final ContactEntity? contact;

  @override
  String toString() {
    return 'ContactState.loaded(contact: $contact)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ContactLoadedImpl &&
            (identical(other.contact, contact) || other.contact == contact));
  }

  @override
  int get hashCode => Object.hash(runtimeType, contact);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ContactLoadedImplCopyWith<_$ContactLoadedImpl> get copyWith =>
      __$$ContactLoadedImplCopyWithImpl<_$ContactLoadedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(ContactEntity? contact) loaded,
    required TResult Function(String message) error,
  }) {
    return loaded(contact);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(ContactEntity? contact)? loaded,
    TResult? Function(String message)? error,
  }) {
    return loaded?.call(contact);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContactEntity? contact)? loaded,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(contact);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ContactInitial value) initial,
    required TResult Function(ContactLoading value) loading,
    required TResult Function(ContactLoaded value) loaded,
    required TResult Function(ContactError value) error,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ContactInitial value)? initial,
    TResult? Function(ContactLoading value)? loading,
    TResult? Function(ContactLoaded value)? loaded,
    TResult? Function(ContactError value)? error,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ContactInitial value)? initial,
    TResult Function(ContactLoading value)? loading,
    TResult Function(ContactLoaded value)? loaded,
    TResult Function(ContactError value)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class ContactLoaded implements ContactState {
  const factory ContactLoaded(final ContactEntity? contact) =
      _$ContactLoadedImpl;

  ContactEntity? get contact;
  @JsonKey(ignore: true)
  _$$ContactLoadedImplCopyWith<_$ContactLoadedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ContactErrorImplCopyWith<$Res> {
  factory _$$ContactErrorImplCopyWith(
          _$ContactErrorImpl value, $Res Function(_$ContactErrorImpl) then) =
      __$$ContactErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String message});
}

/// @nodoc
class __$$ContactErrorImplCopyWithImpl<$Res>
    extends _$ContactStateCopyWithImpl<$Res, _$ContactErrorImpl>
    implements _$$ContactErrorImplCopyWith<$Res> {
  __$$ContactErrorImplCopyWithImpl(
      _$ContactErrorImpl _value, $Res Function(_$ContactErrorImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = null,
  }) {
    return _then(_$ContactErrorImpl(
      null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ContactErrorImpl implements ContactError {
  const _$ContactErrorImpl(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'ContactState.error(message: $message)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ContactErrorImpl &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ContactErrorImplCopyWith<_$ContactErrorImpl> get copyWith =>
      __$$ContactErrorImplCopyWithImpl<_$ContactErrorImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(ContactEntity? contact) loaded,
    required TResult Function(String message) error,
  }) {
    return error(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(ContactEntity? contact)? loaded,
    TResult? Function(String message)? error,
  }) {
    return error?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(ContactEntity? contact)? loaded,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ContactInitial value) initial,
    required TResult Function(ContactLoading value) loading,
    required TResult Function(ContactLoaded value) loaded,
    required TResult Function(ContactError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ContactInitial value)? initial,
    TResult? Function(ContactLoading value)? loading,
    TResult? Function(ContactLoaded value)? loaded,
    TResult? Function(ContactError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ContactInitial value)? initial,
    TResult Function(ContactLoading value)? loading,
    TResult Function(ContactLoaded value)? loaded,
    TResult Function(ContactError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ContactError implements ContactState {
  const factory ContactError(final String message) = _$ContactErrorImpl;

  String get message;
  @JsonKey(ignore: true)
  _$$ContactErrorImplCopyWith<_$ContactErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
