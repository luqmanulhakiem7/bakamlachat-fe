import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/models/contactentity.dart';
import 'package:bakamlachat/models/contactresponseentity.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/utils/utils.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'contact_event.dart';
part 'contact_state.dart';
part 'contact_bloc.freezed.dart';

class ContactBloc extends Bloc<ContactEvent, ContactState> {
  final authToken = AuthBloc().state.token;

  ContactBloc() : super(const ContactInitial()) {
    on<ContactEvent>((event, emit) async {
      try {
        emit(const ContactLoading());
        final contact = await Dio().get(Endpoints.contact,
            options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer $authToken"
              },
            ));
        if (contact.statusCode == 200) {
          final kontaks = ContactResponseEntity(contacs: contact.data);
          vLog(contact.data);
          eLog("=======================");
          vLog(kontaks);
          // List<Map<String, dynamic>> contactData = List<Map<String, dynamic>>.from(contact.data);
          // List<dynamic> data = contactData.map((item) => item['user'][0]).toList();
          // final contacts = ContactEntity.fromJson(data)
        }
      } catch (e) {
        emit(const ContactError("Gagal mengambil Kontak"));
      }
    });
  }
}
