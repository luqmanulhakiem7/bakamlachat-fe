part of 'contact_bloc.dart';

@freezed
class ContactEvent with _$ContactEvent {
  const factory ContactEvent.getContact() = getContact;
  const factory ContactEvent.addContact({required int id}) = addContact;
}
