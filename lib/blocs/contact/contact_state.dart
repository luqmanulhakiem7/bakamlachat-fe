part of 'contact_bloc.dart';

@freezed
class ContactState with _$ContactState {
  const factory ContactState.initial() = ContactInitial;
  const factory ContactState.loading() = ContactLoading;
  const factory ContactState.loaded(ContactEntity? contact) = ContactLoaded;
  const factory ContactState.error(String message) = ContactError;
}
