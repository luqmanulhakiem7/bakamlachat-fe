import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/utils/auth_token.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:dio/dio.dart';

class getAuthProfile {
  static Future<Map<String, dynamic>> getProfile() async {
    try {
      var resp = await Dio().get(Endpoints.profileGet,
          options: Options(
            headers: {
              "Accept": "application/json",
              "Authorization": "Bearer ${authToken()}"
            },
          ));
      if (resp.statusCode == 200) {
        return resp.data;
      }
    } catch (e) {
      eLog(e);
    }
    return {};
  }
}
