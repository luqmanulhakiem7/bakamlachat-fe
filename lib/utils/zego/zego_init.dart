import 'dart:convert';
import 'dart:io';

import 'package:bakamlachat/utils/logger.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:zego_uikit_prebuilt_call/zego_uikit_prebuilt_call.dart';
import 'package:zego_uikit_signaling_plugin/zego_uikit_signaling_plugin.dart';
import 'package:bakamlachat/utils/zego_config.dart';
import 'package:bakamlachat/models/use_variable.dart';

class ZegoInit {
  String formattedDate = DateFormat('MM-dd-yyyy').format(DateTime.now());
  String formattedTime = DateFormat('HH:mm').format(DateTime.now());

  Future<Map<String, dynamic>> readJsonFile() async {
    try {
      // Dapatkan direktori dokumen aplikasi
      Directory? appDocumentsDirectory = await getExternalStorageDirectory();

      // Buat path ke file JSON di dalam direktori dokumen aplikasi
      String filePath = '${appDocumentsDirectory!.path}/call_history.json';

      // Baca isi file JSON
      String jsonString = await File(filePath).readAsString();

      // Parse JSON menjadi objek Dart
      return jsonDecode(jsonString);
    } catch (e) {
      eLog('Error reading JSON file: $e');
      return {};
    }
  }

  int findLastId(List<dynamic> dataList) {
    int lastId = 0;
    for (var data in dataList) {
      int? currentId = int.tryParse(data["id"]);
      if (currentId != null && currentId > lastId) {
        lastId = currentId;
      }
    }
    return lastId;
  }

  Future<void> sendData(Map<String, dynamic> data) async {
    Directory? appDocumentsDirectory = await getExternalStorageDirectory();

    // Buat path ke file JSON di dalam direktori dokumen aplikasi
    String filePath = '${appDocumentsDirectory!.path}/call_history.json';

    String jsonString = await File(filePath).readAsString();
    var jsonDecode = await json.decode(jsonString);
    jsonDecode["data"].add(data);
    writeJsonFile(jsonDecode);
  }

  Future<void> writeJsonFile(Map<String, dynamic> data) async {
    try {
      // Dapatkan direktori dokumen aplikasi
      Directory? appDocumentsDirectory = await getExternalStorageDirectory();

      // Buat path ke file JSON di dalam direktori dokumen aplikasi
      String filePath = '${appDocumentsDirectory!.path}/call_history.json';

      // Konversi objek Dart ke JSON
      String jsonData = jsonEncode(data);

      // Tulis konten JSON ke dalam file
      await File(filePath).writeAsString(jsonData);
      eLog('berhasil');
    } catch (e) {
      eLog('Error writing JSON file: $e');
    }
  }

  void init(String userId, String userName) {
    ZegoUIKitPrebuiltCallInvitationService().init(
        appID: ZegoKonfig.appId /*input your AppID*/,
        appSign: ZegoKonfig.appSign /*input your AppSign*/,
        userID: userId,
        userName: userName,
        plugins: [ZegoUIKitSignalingPlugin()],
        invitationEvents: ZegoUIKitPrebuiltCallInvitationEvents(
          onOutgoingCallAccepted: (callID, callee) async {
            Map<String, dynamic> jsonData = await readJsonFile();

            // Mencari ID terakhir
            int lastId = findLastId(jsonData["data"]);

            // Menghasilkan ID baru
            int newId = lastId + 1;
            String userId = Value.getString()![0];
            String userName = Value.getString()![1];

            Map<String, dynamic> data = {
              "id": newId.toString(),
              "userId": userId,
              "username": userName,
              "time": formattedTime,
              "date": formattedDate,
              "desc": "outgoing",
              "status": "diterima",
              "type": ValueType.getString()
            };
            sendData(data);
          },
          onOutgoingCallTimeout: (callID, callees, isVideoCall) async {
            eLog("panggilan tak terjawab");
            // Membaca isi file JSON lokal
            Map<String, dynamic> jsonData = await readJsonFile();

            // Mencari ID terakhir
            int lastId = findLastId(jsonData["data"]);

            // Menghasilkan ID baru
            int newId = lastId + 1;
            String userId = Value.getString()![0];
            String userName = Value.getString()![1];

            Map<String, dynamic> data = {
              "id": newId.toString(),
              "userId": userId,
              "username": userName,
              "time": formattedTime,
              "date": formattedDate,
              "desc": "outgoing",
              "status": "ditolak",
              "type": ValueType.getString()
            };
            sendData(data);
          },
          onOutgoingCallDeclined: (callID, callee, customData) async {
            eLog("user tidak menerima panggilan");
            // Membaca isi file JSON lokal
            Map<String, dynamic> jsonData = await readJsonFile();

            // Mencari ID terakhir
            int lastId = findLastId(jsonData["data"]);
            String userId = Value.getString()![0];
            String userName = Value.getString()![1];

            int newId = lastId + 1;
            Map<String, dynamic> data = {
              "id": newId.toString(),
              "userId": userId,
              "username": userName,
              "time": formattedTime,
              "date": formattedDate,
              "desc": "outgoing",
              "status": "ditolak",
              "type": ValueType.getString()
            };
            sendData(data);
          },
          onOutgoingCallCancelButtonPressed: () async {
            // Membaca isi file JSON lokal
            Map<String, dynamic> jsonData = await readJsonFile();

            // Mencari ID terakhir
            int lastId = findLastId(jsonData["data"]);
            String userId = Value.getString()![0];
            String userName = Value.getString()![1];

            int newId = lastId + 1;
            Map<String, dynamic> data = {
              "id": newId.toString(),
              "userId": userId,
              "username": userName,
              "time": formattedTime,
              "date": formattedDate,
              "desc": "outgoing",
              "status": "cancel",
              "type": ValueType.getString()
            };
            sendData(data);
          },
          // Incoming Call
          onIncomingCallReceived:
              (callID, caller, callType, callees, customData) {
            var userId = caller.id;
            var userName = caller.name;
            var type = callType == ZegoCallType.voiceCall ? "voice" : "video";
            Value.setString(userId, userName);
            ValueType.setString(type);
          },
          onIncomingCallDeclineButtonPressed: () async {
            eLog("user tidak menerima panggilan");
            Map<String, dynamic> jsonData = await readJsonFile();

            // Mencari ID terakhir
            int lastId = findLastId(jsonData["data"]);

            // Menghasilkan ID baru
            int newId = lastId + 1;
            String userId = Value.getString()![0];
            String userName = Value.getString()![1];

            Map<String, dynamic> data = {
              "id": newId.toString(),
              "userId": userId,
              "username": userName,
              "time": formattedTime,
              "date": formattedDate,
              "desc": "incoming",
              "status": "ditolak",
              "type": ValueType.getString()
            };
            sendData(data);
          },
          onIncomingCallAcceptButtonPressed: () async {
            eLog("user menerima panggilan");
            Map<String, dynamic> jsonData = await readJsonFile();

            // Mencari ID terakhir
            int lastId = findLastId(jsonData["data"]);

            // Menghasilkan ID baru
            int newId = lastId + 1;
            String userId = Value.getString()![0];
            String userName = Value.getString()![1];

            Map<String, dynamic> data = {
              "id": newId.toString(),
              "userId": userId,
              "username": userName,
              "time": formattedTime,
              "date": formattedDate,
              "desc": "incoming",
              "status": "diterima",
              "type": ValueType.getString()
            };
            sendData(data);
          },
          onIncomingCallTimeout: (callID, caller) async {
            eLog("panggilan tak terjawab");
            Map<String, dynamic> jsonData = await readJsonFile();

            // Mencari ID terakhir
            int lastId = findLastId(jsonData["data"]);

            // Menghasilkan ID baru
            int newId = lastId + 1;
            String userId = Value.getString()![0];
            String userName = Value.getString()![1];

            Map<String, dynamic> data = {
              "id": newId.toString(),
              "userId": userId,
              "username": userName,
              "time": formattedTime,
              "date": formattedDate,
              "desc": "incoming",
              "status": "ditolak",
              "type": ValueType.getString()
            };
            sendData(data);
          },
          onIncomingCallCanceled: (callID, caller, customData) async {
            Map<String, dynamic> jsonData = await readJsonFile();

            // Mencari ID terakhir
            int lastId = findLastId(jsonData["data"]);

            // Menghasilkan ID baru
            int newId = lastId + 1;
            String userId = Value.getString()![0];
            String userName = Value.getString()![1];

            Map<String, dynamic> data = {
              "id": newId.toString(),
              "userId": userId,
              "username": userName,
              "time": formattedTime,
              "date": formattedDate,
              "desc": "incoming",
              "status": "cancel",
              "type": ValueType.getString()
            };
            sendData(data);
          },
        ));
  }

  void logout() {
    ZegoUIKitPrebuiltCallInvitationService().uninit();
  }
}
