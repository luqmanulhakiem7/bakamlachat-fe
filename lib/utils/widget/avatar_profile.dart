import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:flutter/material.dart';

class AvatarProfile extends StatelessWidget {
  const AvatarProfile(
      {super.key,
      required this.avatar,
      required this.height,
      required this.width});
  final String? avatar;
  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    var uri = '${Endpoints.avatar}${avatar}';

    return avatar != null
        ? Container(
            decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(50.0)),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50.0),
              child: Image.network(
                uri,
                height: height,
                width: width,
                fit: BoxFit.fill,
              ),
            ),
          )
        : Icon(Icons.account_circle, size: height);
  }
}
