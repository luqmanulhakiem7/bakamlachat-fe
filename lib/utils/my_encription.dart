import 'package:encrypt/encrypt.dart' as enkripsi;

class MyEncryptionDecryption {
  static final key = enkripsi.Key.fromUtf8('my 32 length key.is.bakamla.....');
  static final iv = enkripsi.IV.fromLength(16);
  static final encrypter = enkripsi.Encrypter(enkripsi.AES(key));

  static encryptAES(text) {
    final encrypted = encrypter.encrypt(text, iv: iv);
    final textEncrypted = encrypted.base64;
    // final key = iv.base64;
    return textEncrypted;
  }

  static decryptAES(String text, String key) {
    final fromDb = enkripsi.Encrypted.fromBase64(text);
    final ivFromDb = enkripsi.IV.fromBase64(key);
    // return ivFromDb;
    final hasil = encrypter.decrypt(fromDb, iv: ivFromDb);
    return hasil.toString();
  }
}
