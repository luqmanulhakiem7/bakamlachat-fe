import 'package:bakamlachat/blocs/blocs.dart';

String authToken() {
  final authToken = AuthBloc().state.token;
  return authToken as String;
}
