String formatPhoneNumber(String? phoneNumber) {
  // Hilangkan semua karakter selain digit
  String digitsOnly = phoneNumber!.replaceAll(RegExp(r'[^\d]'), '');

  // Periksa apakah nomor telepon memiliki prefix '+'
  bool hasPlusPrefix = digitsOnly.startsWith('+');

  // Jika nomor telepon memiliki prefix '+', hilangkan prefix tersebut
  if (hasPlusPrefix) {
    digitsOnly = digitsOnly.substring(1);
  }

  // Jumlah digit pada nomor telepon
  int length = digitsOnly.length;

  // Jika jumlah digit kurang dari 7, tidak mungkin memformat nomor telepon
  if (length < 7) {
    return phoneNumber;
  }

  // Format nomor telepon
  String formattedPhoneNumber = '+';

  // Jika nomor telepon memiliki prefix '+', tambahkan kembali prefix tersebut
  if (hasPlusPrefix) {
    formattedPhoneNumber += ' ';
  }

  // Tambahkan kode negara (misalnya, '+62')
  formattedPhoneNumber += '${digitsOnly.substring(0, 2)} ';

  // Tambahkan bagian kode wilayah (misalnya, '343')
  formattedPhoneNumber += '${digitsOnly.substring(2, 5)}-';

  // Tambahkan bagian nomor telepon
  // Bagian nomor telepon sekarang terbagi menjadi 2 bagian (bagian tengah dan akhir)
  formattedPhoneNumber +=
      '${digitsOnly.substring(5, length - 4)}-${digitsOnly.substring(length - 4)}';

  return formattedPhoneNumber;
}
