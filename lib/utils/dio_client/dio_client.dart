import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/utils/dio_client/app_interceptors.dart';
import 'package:dio/dio.dart';

class DioClient {
  static DioClient? _singleton;

  static late Dio _dio;

  DioClient._() {
    _dio = createDioClient();
  }

  factory DioClient() {
    return _singleton ??= DioClient._();
  }

  Dio get instance => _dio;

  Dio createDioClient() {
    final dio = Dio(
      BaseOptions(
        baseUrl: Endpoints.baseUrl,
        receiveTimeout: const Duration(seconds: 15), //15seconds
        connectTimeout: const Duration(seconds: 15),
        sendTimeout: const Duration(seconds: 15),
        headers: {
          Headers.acceptHeader: 'application/json',
          Headers.contentTypeHeader: 'application/json'
        },
      ),
    );

    dio.interceptors.addAll([
      AppInterceptors(),
      // PrettyDioLogger(
      //     requestHeader: true,
      //     requestBody: true,
      //     responseBody: true,
      //     responseHeader: true,
      //     error: true,
      //     compact: true,
      //     maxWidth: 90),
    ]);

    return dio;
  }
}
