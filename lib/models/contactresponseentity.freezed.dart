// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'contactresponseentity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ContactResponseEntity _$ContactResponseEntityFromJson(
    Map<String, dynamic> json) {
  return _ContactResponseEntity.fromJson(json);
}

/// @nodoc
mixin _$ContactResponseEntity {
  ContactEntity get contacs => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ContactResponseEntityCopyWith<ContactResponseEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ContactResponseEntityCopyWith<$Res> {
  factory $ContactResponseEntityCopyWith(ContactResponseEntity value,
          $Res Function(ContactResponseEntity) then) =
      _$ContactResponseEntityCopyWithImpl<$Res, ContactResponseEntity>;
  @useResult
  $Res call({ContactEntity contacs});

  $ContactEntityCopyWith<$Res> get contacs;
}

/// @nodoc
class _$ContactResponseEntityCopyWithImpl<$Res,
        $Val extends ContactResponseEntity>
    implements $ContactResponseEntityCopyWith<$Res> {
  _$ContactResponseEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? contacs = null,
  }) {
    return _then(_value.copyWith(
      contacs: null == contacs
          ? _value.contacs
          : contacs // ignore: cast_nullable_to_non_nullable
              as ContactEntity,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ContactEntityCopyWith<$Res> get contacs {
    return $ContactEntityCopyWith<$Res>(_value.contacs, (value) {
      return _then(_value.copyWith(contacs: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$ContactResponseEntityImplCopyWith<$Res>
    implements $ContactResponseEntityCopyWith<$Res> {
  factory _$$ContactResponseEntityImplCopyWith(
          _$ContactResponseEntityImpl value,
          $Res Function(_$ContactResponseEntityImpl) then) =
      __$$ContactResponseEntityImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({ContactEntity contacs});

  @override
  $ContactEntityCopyWith<$Res> get contacs;
}

/// @nodoc
class __$$ContactResponseEntityImplCopyWithImpl<$Res>
    extends _$ContactResponseEntityCopyWithImpl<$Res,
        _$ContactResponseEntityImpl>
    implements _$$ContactResponseEntityImplCopyWith<$Res> {
  __$$ContactResponseEntityImplCopyWithImpl(_$ContactResponseEntityImpl _value,
      $Res Function(_$ContactResponseEntityImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? contacs = null,
  }) {
    return _then(_$ContactResponseEntityImpl(
      contacs: null == contacs
          ? _value.contacs
          : contacs // ignore: cast_nullable_to_non_nullable
              as ContactEntity,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ContactResponseEntityImpl implements _ContactResponseEntity {
  _$ContactResponseEntityImpl({required this.contacs});

  factory _$ContactResponseEntityImpl.fromJson(Map<String, dynamic> json) =>
      _$$ContactResponseEntityImplFromJson(json);

  @override
  final ContactEntity contacs;

  @override
  String toString() {
    return 'ContactResponseEntity(contacs: $contacs)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ContactResponseEntityImpl &&
            (identical(other.contacs, contacs) || other.contacs == contacs));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, contacs);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ContactResponseEntityImplCopyWith<_$ContactResponseEntityImpl>
      get copyWith => __$$ContactResponseEntityImplCopyWithImpl<
          _$ContactResponseEntityImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ContactResponseEntityImplToJson(
      this,
    );
  }
}

abstract class _ContactResponseEntity implements ContactResponseEntity {
  factory _ContactResponseEntity({required final ContactEntity contacs}) =
      _$ContactResponseEntityImpl;

  factory _ContactResponseEntity.fromJson(Map<String, dynamic> json) =
      _$ContactResponseEntityImpl.fromJson;

  @override
  ContactEntity get contacs;
  @override
  @JsonKey(ignore: true)
  _$$ContactResponseEntityImplCopyWith<_$ContactResponseEntityImpl>
      get copyWith => throw _privateConstructorUsedError;
}
