import 'dart:convert';

import 'package:bakamlachat/utils/my_encription.dart';
import 'package:bakamlachat/utils/utils.dart';
import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:bakamlachat/models/user_models.dart';
import 'package:encrypt/encrypt.dart' as enkripsi;
// import 'package:intl/intl.dart';

part 'chat_message_model.freezed.dart';
part 'chat_message_model.g.dart';

@freezed
class ChatMessageEntity with _$ChatMessageEntity {
  const ChatMessageEntity._();

  factory ChatMessageEntity({
    required int id,
    @JsonKey(name: "chat_id") required int chatId,
    @JsonKey(name: "user_id") required int userId,
    required String message,
    String? ivkey,
    String? images,
    String? videos,
    String? documents,
    String? audios,
    String? locations,
    // ignore: non_constant_identifier_names
    String? contact_name,
    String? contact_number,
    String? longitude,
    String? latitude,
    @JsonKey(name: "created_at") required String createdAt,
    @JsonKey(name: "updated_at") required String updatedAt,
    required UserEntity user,
  }) = _ChatMessageEntity;

  bool get hasKey => ivkey != null && ivkey!.length > 0;

  bool get hasImage => images != null;
  bool get hasVideo => videos != null;
  bool get hasDocument => documents != null;
  bool get hasAudio => audios != null;
  bool get hasLocation => locations != null;
  bool get longExist => longitude != null;
  bool get latExist => latitude != null;
  bool get hasContact => contact_name != null;
  bool get noMessage =>
      message != images &&
      message != videos &&
      message != documents &&
      message != audios &&
      message != locations &&
      message != contact_name;

  factory ChatMessageEntity.fromJson(Map<String, dynamic> json) =>
      _$ChatMessageEntityFromJson(json);

  customProperti() {
    return IconButton(
        onPressed: () {
          eLog("test");
        },
        icon: const Icon(Icons.download));
  }

  // final encryptedFromDB =
  //     enkripsi.Encrypted.fromBase64(entoString); // apakah seperti ini?
  // final decrypted = encrypter.decrypt(encryptedFromDB, iv: iv);

  ChatMessage get toChatMessage {
    final keyA = enkripsi.Key.fromUtf8('my 32 length key.is.bakamla.....');
    final encrypter = enkripsi.Encrypter(enkripsi.AES(keyA));
    final iv = enkripsi.IV.fromLength(16);

    // pisah dulu
    var decryptedMessage = '';
    if (hasKey) {
      final fromDb = enkripsi.Encrypted.fromBase64(message);
      final ivv = enkripsi.IV.fromBase64(ivkey.toString());
      decryptedMessage = encrypter.decrypt(fromDb, iv: ivv);
    }
    // final parts = message.split('\$\$\$');
    // final messagetxt = parts[0];
    // final ivkey = parts[1];

    // final fromDb = enkripsi.Encrypted.fromBase64(messagetxt);
    // final iv2 = iv.base64.toString(); //this is from db convert into iv
    // final ivv = enkripsi.IV.fromBase64(ivkey);
    // final decryptedMessage = encrypter.decrypt(fromDb, iv: ivv);

    return ChatMessage(
      user: user.toChatUser,
      text: noMessage
          ? hasKey
              ? decryptedMessage
              : message
          : '',
      idMessage: id.toString(),
      ivkey: hasKey ? ivkey : null,
      image: hasImage ? images : null,
      video: hasVideo ? videos : null,
      document: hasDocument ? documents : null,
      audio: hasAudio ? audios : null,
      location: hasLocation ? locations : null,
      longitude: longExist ? longitude : null,
      latitude: latExist ? latitude : null,
      contactName: hasContact ? contact_name : null,
      contactNumber: hasContact ? contact_number : null,
      medias: hasImage
          ? [
              ChatMedia(
                  url:
                      "https://api.bakamla.barengsaya.com/storage/image/$images",
                  fileName: "$images",
                  type: MediaType.image)
            ]
          : hasVideo
              ? [
                  ChatMedia(
                      url:
                          "https://api.bakamla.barengsaya.com/storage/video/$videos",
                      fileName: "$videos",
                      type: MediaType.video)
                ]
              : hasDocument
                  ? [
                      ChatMedia(
                        url:
                            "https://api.bakamla.barengsaya.com/storage/document/$documents",
                        fileName: "$documents",
                        type: MediaType.file,
                        // customProperties: customProperti(),
                      )
                    ]
                  : hasAudio
                      ? [
                          ChatMedia(
                            url:
                                "https://api.bakamla.barengsaya.com/storage/audio/$audios",
                            fileName: "$documents",
                            type: MediaType.file,
                          )
                        ]
                      : hasLocation
                          ? [
                              ChatMedia(
                                url:
                                    "https://api.bakamla.barengsaya.com/storage/audio/",
                                fileName: "$locations",
                                type: MediaType.file,
                              )
                            ]
                          : hasContact
                              ? [
                                  ChatMedia(
                                    url:
                                        "https://api.bakamla.barengsaya.com/storage/audio/",
                                    fileName: "$contact_name",
                                    type: MediaType.file,
                                  )
                                ]
                              : [],
      createdAt: parseDateTime(createdAt),
    );
  }
}
