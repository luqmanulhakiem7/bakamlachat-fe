// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contactentity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ContactEntityImpl _$$ContactEntityImplFromJson(Map<String, dynamic> json) =>
    _$ContactEntityImpl(
      id: json['id'] as int,
      username: json['username'] as String,
      avatar: json['avatar'] as String?,
      kta: json['kta'] as String?,
    );

Map<String, dynamic> _$$ContactEntityImplToJson(_$ContactEntityImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'username': instance.username,
      'avatar': instance.avatar,
      'kta': instance.kta,
    };
