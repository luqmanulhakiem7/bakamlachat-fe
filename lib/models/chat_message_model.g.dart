// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_message_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ChatMessageEntityImpl _$$ChatMessageEntityImplFromJson(
        Map<String, dynamic> json) =>
    _$ChatMessageEntityImpl(
      id: json['id'] as int,
      chatId: json['chat_id'] as int,
      userId: json['user_id'] as int,
      message: json['message'] as String,
      ivkey: json['ivkey'] as String?,
      images: json['images'] as String?,
      videos: json['videos'] as String?,
      documents: json['documents'] as String?,
      audios: json['audios'] as String?,
      locations: json['locations'] as String?,
      contact_name: json['contact_name'] as String?,
      contact_number: json['contact_number'] as String?,
      longitude: json['longitude'] as String?,
      latitude: json['latitude'] as String?,
      createdAt: json['created_at'] as String,
      updatedAt: json['updated_at'] as String,
      user: UserEntity.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$ChatMessageEntityImplToJson(
        _$ChatMessageEntityImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'chat_id': instance.chatId,
      'user_id': instance.userId,
      'message': instance.message,
      'ivkey': instance.ivkey,
      'images': instance.images,
      'videos': instance.videos,
      'documents': instance.documents,
      'audios': instance.audios,
      'locations': instance.locations,
      'contact_name': instance.contact_name,
      'contact_number': instance.contact_number,
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'user': instance.user,
    };
