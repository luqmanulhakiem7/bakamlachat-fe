class Value {
  static String? otherId;
  static String? otherName;
  static void setString(String newId, String newName) {
    otherId = newId;
    otherName = newName;
  }

  static List<dynamic>? getString() {
    List maValue = [otherId, otherName];
    return maValue;
  }
}

class ValueType {
  static String? type;
  static void setString(String newType) {
    type = newType;
  }

  static String? getString() {
    return type;
  }
}
