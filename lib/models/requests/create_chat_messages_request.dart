import 'package:freezed_annotation/freezed_annotation.dart';

part 'create_chat_messages_request.freezed.dart';
part 'create_chat_messages_request.g.dart';

@freezed
class CreateChatMessagesRequest with _$CreateChatMessagesRequest {
  factory CreateChatMessagesRequest({
    @JsonKey(name: "chat_id") required int chatId,
    required String message,
    String? ivkey,
  }) = _CreateChatMessagesRequest;

  factory CreateChatMessagesRequest.fromJson(Map<String, dynamic> json) =>
      _$CreateChatMessagesRequestFromJson(json);
}
