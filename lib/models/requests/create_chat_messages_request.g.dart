// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_chat_messages_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$CreateChatMessagesRequestImpl _$$CreateChatMessagesRequestImplFromJson(
        Map<String, dynamic> json) =>
    _$CreateChatMessagesRequestImpl(
      chatId: json['chat_id'] as int,
      message: json['message'] as String,
      ivkey: json['ivkey'] as String?,
    );

Map<String, dynamic> _$$CreateChatMessagesRequestImplToJson(
        _$CreateChatMessagesRequestImpl instance) =>
    <String, dynamic>{
      'chat_id': instance.chatId,
      'message': instance.message,
      'ivkey': instance.ivkey,
    };
