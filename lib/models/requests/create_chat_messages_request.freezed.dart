// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'create_chat_messages_request.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

CreateChatMessagesRequest _$CreateChatMessagesRequestFromJson(
    Map<String, dynamic> json) {
  return _CreateChatMessagesRequest.fromJson(json);
}

/// @nodoc
mixin _$CreateChatMessagesRequest {
  @JsonKey(name: "chat_id")
  int get chatId => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;
  String? get ivkey => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CreateChatMessagesRequestCopyWith<CreateChatMessagesRequest> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateChatMessagesRequestCopyWith<$Res> {
  factory $CreateChatMessagesRequestCopyWith(CreateChatMessagesRequest value,
          $Res Function(CreateChatMessagesRequest) then) =
      _$CreateChatMessagesRequestCopyWithImpl<$Res, CreateChatMessagesRequest>;
  @useResult
  $Res call(
      {@JsonKey(name: "chat_id") int chatId, String message, String? ivkey});
}

/// @nodoc
class _$CreateChatMessagesRequestCopyWithImpl<$Res,
        $Val extends CreateChatMessagesRequest>
    implements $CreateChatMessagesRequestCopyWith<$Res> {
  _$CreateChatMessagesRequestCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatId = null,
    Object? message = null,
    Object? ivkey = freezed,
  }) {
    return _then(_value.copyWith(
      chatId: null == chatId
          ? _value.chatId
          : chatId // ignore: cast_nullable_to_non_nullable
              as int,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      ivkey: freezed == ivkey
          ? _value.ivkey
          : ivkey // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CreateChatMessagesRequestImplCopyWith<$Res>
    implements $CreateChatMessagesRequestCopyWith<$Res> {
  factory _$$CreateChatMessagesRequestImplCopyWith(
          _$CreateChatMessagesRequestImpl value,
          $Res Function(_$CreateChatMessagesRequestImpl) then) =
      __$$CreateChatMessagesRequestImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: "chat_id") int chatId, String message, String? ivkey});
}

/// @nodoc
class __$$CreateChatMessagesRequestImplCopyWithImpl<$Res>
    extends _$CreateChatMessagesRequestCopyWithImpl<$Res,
        _$CreateChatMessagesRequestImpl>
    implements _$$CreateChatMessagesRequestImplCopyWith<$Res> {
  __$$CreateChatMessagesRequestImplCopyWithImpl(
      _$CreateChatMessagesRequestImpl _value,
      $Res Function(_$CreateChatMessagesRequestImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? chatId = null,
    Object? message = null,
    Object? ivkey = freezed,
  }) {
    return _then(_$CreateChatMessagesRequestImpl(
      chatId: null == chatId
          ? _value.chatId
          : chatId // ignore: cast_nullable_to_non_nullable
              as int,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      ivkey: freezed == ivkey
          ? _value.ivkey
          : ivkey // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$CreateChatMessagesRequestImpl implements _CreateChatMessagesRequest {
  _$CreateChatMessagesRequestImpl(
      {@JsonKey(name: "chat_id") required this.chatId,
      required this.message,
      this.ivkey});

  factory _$CreateChatMessagesRequestImpl.fromJson(Map<String, dynamic> json) =>
      _$$CreateChatMessagesRequestImplFromJson(json);

  @override
  @JsonKey(name: "chat_id")
  final int chatId;
  @override
  final String message;
  @override
  final String? ivkey;

  @override
  String toString() {
    return 'CreateChatMessagesRequest(chatId: $chatId, message: $message, ivkey: $ivkey)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateChatMessagesRequestImpl &&
            (identical(other.chatId, chatId) || other.chatId == chatId) &&
            (identical(other.message, message) || other.message == message) &&
            (identical(other.ivkey, ivkey) || other.ivkey == ivkey));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, chatId, message, ivkey);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateChatMessagesRequestImplCopyWith<_$CreateChatMessagesRequestImpl>
      get copyWith => __$$CreateChatMessagesRequestImplCopyWithImpl<
          _$CreateChatMessagesRequestImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$CreateChatMessagesRequestImplToJson(
      this,
    );
  }
}

abstract class _CreateChatMessagesRequest implements CreateChatMessagesRequest {
  factory _CreateChatMessagesRequest(
      {@JsonKey(name: "chat_id") required final int chatId,
      required final String message,
      final String? ivkey}) = _$CreateChatMessagesRequestImpl;

  factory _CreateChatMessagesRequest.fromJson(Map<String, dynamic> json) =
      _$CreateChatMessagesRequestImpl.fromJson;

  @override
  @JsonKey(name: "chat_id")
  int get chatId;
  @override
  String get message;
  @override
  String? get ivkey;
  @override
  @JsonKey(ignore: true)
  _$$CreateChatMessagesRequestImplCopyWith<_$CreateChatMessagesRequestImpl>
      get copyWith => throw _privateConstructorUsedError;
}
