// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contactresponseentity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ContactResponseEntityImpl _$$ContactResponseEntityImplFromJson(
        Map<String, dynamic> json) =>
    _$ContactResponseEntityImpl(
      contacs: ContactEntity.fromJson(json['contacs'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$ContactResponseEntityImplToJson(
        _$ContactResponseEntityImpl instance) =>
    <String, dynamic>{
      'contacs': instance.contacs,
    };
