import 'package:bakamlachat/models/contactentity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'contactresponseentity.freezed.dart';
part 'contactresponseentity.g.dart';

@freezed
class ContactResponseEntity with _$ContactResponseEntity {
  factory ContactResponseEntity({
    required ContactEntity contacs,
  }) = _ContactResponseEntity;

  factory ContactResponseEntity.fromJson(Map<String, dynamic> json) =>
      _$ContactResponseEntityFromJson(json);
}
