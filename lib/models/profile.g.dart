// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ProfileEntityImpl _$$ProfileEntityImplFromJson(Map<String, dynamic> json) =>
    _$ProfileEntityImpl(
      id: json['id'] as int,
      username: json['username'] as String,
      bio: json['bio'] as String?,
      avatar: json['avatar'] as String?,
      phones: json['phones'] as String?,
      kta: json['kta'] as String?,
    );

Map<String, dynamic> _$$ProfileEntityImplToJson(_$ProfileEntityImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'username': instance.username,
      'bio': instance.bio,
      'avatar': instance.avatar,
      'phones': instance.phones,
      'kta': instance.kta,
    };
