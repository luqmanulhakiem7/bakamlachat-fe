import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile.freezed.dart';
part 'profile.g.dart';

@freezed
class ProfileEntity with _$ProfileEntity {
  factory ProfileEntity({
    required int id,
    required String username,
    String? bio,
    String? avatar,
    String? phones,
    String? kta,
  }) = _ProfileEntity;

  factory ProfileEntity.fromJson(Map<String, dynamic> json) =>
      _$ProfileEntityFromJson(json);
}
