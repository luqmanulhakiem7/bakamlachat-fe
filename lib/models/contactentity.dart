import 'package:freezed_annotation/freezed_annotation.dart';

part 'contactentity.freezed.dart';
part 'contactentity.g.dart';

@freezed
class ContactEntity with _$ContactEntity {
  const ContactEntity._();

  factory ContactEntity({
    required int id,
    required String username,
    String? avatar,
    String? kta,
  }) = _ContactEntity;

  factory ContactEntity.fromJson(Map<String, dynamic> json) =>
      _$ContactEntityFromJson(json);
}
