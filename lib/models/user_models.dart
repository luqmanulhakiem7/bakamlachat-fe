// import 'dart:convert';

import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'user_models.freezed.dart';
part 'user_models.g.dart';

@freezed
class UserEntity with _$UserEntity {
  const UserEntity._();

  factory UserEntity({
    required int id,
    required String email,
    required String username,
    String? avatar,
    String? phones,
    String? kta,
  }) = _UserEntity;

  factory UserEntity.fromJson(Map<String, dynamic> json) =>
      _$UserEntityFromJson(json);

  ChatUser get toChatUser {
    return ChatUser(
      id: id.toString(),
      firstName: username,
      profileImage: avatar != null
          ? "${Endpoints.avatar}${avatar}"
          : "${Endpoints.avatar}CDN-IMG-BM-AVATAR-GUEST.webp",
    );
  }
}

@freezed
class AuthUser with _$AuthUser {
  factory AuthUser({
    required UserEntity user,
    required String token,
  }) = _AuthUser;

  factory AuthUser.fromJson(Map<String, dynamic> json) =>
      _$AuthUserFromJson(json);
}
