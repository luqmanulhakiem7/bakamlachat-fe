import 'dart:io';

import 'package:bakamlachat/utils/logger.dart';
import 'package:path_provider/path_provider.dart';

class SetelanScreenViewModel {
  Future<void> deleteFile() async {
    // Dapatkan direktori dokumen aplikasi
    Directory appDocumentsDirectory = await getApplicationDocumentsDirectory();

    // Buat path ke file JSON di dalam direktori dokumen aplikasi
    String filePath = '${appDocumentsDirectory.path}/call_history.json';

    // Cek apakah file ada sebelum menghapus
    if (await File(filePath).exists()) {
      try {
        // Hapus file JSON dari direktori lokal aplikasi
        await File(filePath).delete();

        eLog('File JSON berhasil dihapus dari direktori lokal aplikasi!');
      } catch (e) {
        eLog('Error deleting JSON file: $e');
      }
    } else {
      eLog('File JSON tidak ditemukan di direktori lokal aplikasi.');
    }
  }
}
