import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/profile/profile_bloc.dart';
import 'package:bakamlachat/cubits/cubits.dart';
import 'package:bakamlachat/screens/profile/profile_screeen.dart';
import 'package:bakamlachat/screens/setelan/fragment_profile.dart';
import 'package:bakamlachat/screens/setelan/setelan_screen_view_model.dart';
import 'package:bakamlachat/screens/splash/splash_screen.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:bakamlachat/utils/onesignal/onesignal.dart';
import 'package:bakamlachat/utils/zego/zego_init.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SetelanScreen extends StatefulWidget {
  final SetelanScreenViewModel viewModel;
  const SetelanScreen({Key? key, required this.viewModel}) : super(key: key);

  static const routeName = "setelan";

  @override
  State<SetelanScreen> createState() => _SetelanScreen();
}

class _SetelanScreen extends State<SetelanScreen> {
  @override
  void initState() {
    super.initState();
    context.read<ProfileBloc>().add(const ProfileEvent.getProfile());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Row(
            children: [Text("Setelan")],
          ),
        ),
        body: ListView(
          children: [
            Column(
              children: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(ProfileScreen.routeName);
                  },
                  child: BlocConsumer<ProfileBloc, ProfileState>(
                    listener: (context, state) {
                      if (state is ProfileError) {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: const Text('Error'),
                            content: Text(state.message),
                            actions: [
                              TextButton(
                                onPressed: () => Navigator.of(context).pop(),
                                child: const Text('OK'),
                              ),
                            ],
                          ),
                        );
                      }
                    },
                    builder: (context, state) {
                      if (state is ProfileInitial) {
                        return const Center(child: Text('initial'));
                      } else if (state is ProfileLoaded) {
                        final data = state.profile;
                        return FragmentProfile(
                          data: data,
                        );
                      } else if (state is ProfileLoading) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        return const Center(child: Text('Unknown state'));
                      }
                    },
                  ),
                ),
              ],
            ),
            const Divider(),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                // Center(
                //   child: OutlinedButton.icon(
                //     onPressed: () {
                //       eLog("Akun");
                //     },
                //     style: OutlinedButton.styleFrom(
                //       fixedSize: const Size(300, 70),
                //       padding: const EdgeInsets.all(20.0),
                //       backgroundColor: Colors.white70,
                //     ),
                //     label: const Text(
                //       'Akun',
                //       style: TextStyle(
                //         color: Colors.blue,
                //       ),
                //     ),
                //     icon: const Icon(Icons.person),
                //   ),
                // ),
                const SizedBox(
                  height: 20,
                ),
                BlocConsumer<AuthBloc, AuthState>(
                  listener: (context, state) {
                    if (!state.isAuthenticated) {
                      deleteUserTag();
                      ZegoInit().logout();
                      Navigator.of(context).pushNamed(SplashScreen.routeName);
                    }
                  },
                  builder: (context, state) {
                    return OutlinedButton.icon(
                      onPressed: () {
                        widget.viewModel.deleteFile();
                        context.read<GuestCubit>().signOut();
                      },
                      style: OutlinedButton.styleFrom(
                        fixedSize: const Size(300, 70),
                        padding: const EdgeInsets.all(20.0),
                        backgroundColor: Colors.white70,
                      ),
                      label: const Text(
                        'Logout',
                        style: TextStyle(
                          color: Colors.blue,
                        ),
                      ),
                      icon: const Icon(Icons.logout),
                    );
                  },
                ),
              ],
            ),
          ],
        ));
  }
}
