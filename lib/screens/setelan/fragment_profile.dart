import 'package:bakamlachat/models/profile.dart';
import 'package:bakamlachat/utils/widget/avatar_profile.dart';
import 'package:flutter/material.dart';

class FragmentProfile extends StatelessWidget {
  final ProfileEntity? data;

  const FragmentProfile({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Center(
          child: AvatarProfile(avatar: data!.avatar, height: 60, width: 60),
        ),
        const SizedBox(width: 5),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              data!.username,
              style: const TextStyle(
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              data!.bio.toString(),
              style: const TextStyle(
                color: Colors.black54,
                fontSize: 15.0,
                fontWeight: FontWeight.w400,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
