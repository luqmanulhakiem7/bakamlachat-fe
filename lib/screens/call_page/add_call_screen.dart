import 'dart:math';

import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/user/user_bloc.dart';
import 'package:bakamlachat/models/user_models.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zego_express_engine/zego_express_engine.dart';
import 'package:zego_uikit_prebuilt_call/zego_uikit_prebuilt_call.dart';

class AddCallScreen extends StatefulWidget {
  const AddCallScreen({super.key});

  @override
  State<AddCallScreen> createState() => _AddCallScreen();
}

class _AddCallScreen extends State<AddCallScreen> {
  // List<Map<String, dynamic>> userApi = [];
  List<dynamic> userApi = [];
  final token = AuthBloc().state.token;

  // List<ZegoUIKitUser> invitees = [];

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  // void call(String id, String username) {
  //   setState(() {
  //     invitees.clear();
  //     invitees.add(ZegoUIKitUser(
  //       id: id,
  //       name: username,
  //     ));
  //   });
  // }

  void _getUser() async {
    final contact = await Dio().get(Endpoints.contact,
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          },
        ));

    setState(() {
      userApi = contact.data.map((item) {
        Map<String, dynamic> user = Map<String, dynamic>.from(item['user'][0]);
        user['isSelected'] = false; // Assuming default value is false
        return user;
      }).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Buat Panggilan"),
        // actions: [
        //   IconButton(
        //       onPressed: () {
        //         // eLog(UserApi);
        //       },
        //       icon: const Icon(Icons.search)),
        // ],
      ),
      body: Container(
        child: userApi.length <= 0
            ?
            // const Center(
            //     child: CircularProgressIndicator(),
            //   )
            const Center(
                child: Text(
                  'Belum Memiliki Teman :(',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey,
                  ),
                ),
              )
            : ListView.builder(
                itemCount: userApi.length,
                itemBuilder: (context, i) {
                  return Card(
                    child: ListTile(
                      leading: userApi[i]['avatar'] != null
                          ? CircleAvatar(
                              radius: 20.0,
                              backgroundImage: NetworkImage(
                                  Endpoints.avatar + userApi[i]['avatar']),
                            )
                          : const CircleAvatar(
                              backgroundColor:
                                  Color.fromARGB(255, 228, 228, 228),
                              child: Icon(Icons.person),
                            ),
                      title: Text("${userApi[i]['username']}"),
                      // subtitle: Text("${userApi[i]['phones']}"),
                      subtitle: const Text(""),
                      trailing: Wrap(
                        children: [
                          ZegoSendCallInvitationButton(
                            invitees: [
                              ZegoUIKitUser(
                                id: userApi[i]['id'].toString(),
                                name: userApi[i]['username'],
                              ) //Another exception was thrown: type 'int' is not a subtype of type 'String'
                            ],
                            callID: Random().nextInt(100).toString(),
                            resourceID: "zego_call",
                            isVideoCall: false,
                            icon: ButtonIcon(
                                icon: const Icon(
                                  Icons.call,
                                  color: Colors.white,
                                ),
                                backgroundColor:
                                    const Color.fromARGB(255, 253, 0, 0)),
                            buttonSize: const Size(35, 35),
                            iconSize: const Size(35, 35),
                            iconTextSpacing: double.nan,
                            margin: const EdgeInsets.only(left: 1, right: 5),
                            padding: const EdgeInsets.all(0),
                          ),
                          ZegoSendCallInvitationButton(
                            callID: Random().nextInt(100).toString(),
                            resourceID: "zego_call",
                            isVideoCall: true,
                            icon: ButtonIcon(
                                icon: const Icon(
                                  Icons.video_call,
                                  color: Colors.white,
                                ),
                                backgroundColor:
                                    const Color.fromARGB(255, 253, 0, 0)),
                            buttonSize: const Size(35, 35),
                            iconSize: const Size(35, 35),
                            iconTextSpacing: double.nan,
                            margin: const EdgeInsets.only(left: 1, right: 5),
                            padding: const EdgeInsets.all(0),
                            invitees: [
                              ZegoUIKitUser(
                                id: userApi[i]['id'].toString(),
                                name: userApi[i]['username'],
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
      ),
    );
  }
}
