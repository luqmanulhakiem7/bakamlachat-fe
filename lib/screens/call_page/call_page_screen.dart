// ignore_for_file: unused_field, unnecessary_null_comparison

import 'dart:math';

import 'package:bakamlachat/blocs/user/user_bloc.dart';
import 'package:bakamlachat/models/models.dart';
import 'package:bakamlachat/models/use_variable.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zego_uikit_prebuilt_call/zego_uikit_prebuilt_call.dart';

class CallPage extends StatefulWidget {
  const CallPage({
    super.key,
  });

  @override
  State<CallPage> createState() => _CallPageState();
}

class _CallPageState extends State<CallPage> {
  List<ZegoUIKitUser> invitees = [];
  List<Contact>? _contacts;
  bool _permissionDenied = false;
  List<Map<String, dynamic>> arrKontak = [];
  List<Map<String, dynamic>> arrUser = [];
  List<Map<String, dynamic>> resultExists = [];
  List<Map<String, dynamic>> resultNotExists = [];
  List<Map<String, dynamic>> index = [];

  @override
  void initState() {
    super.initState();
    _fetchContacts();
  }

  Future _fetchContacts() async {
    if (!await FlutterContacts.requestPermission(readonly: true)) {
      setState(() => _permissionDenied = true);
    } else {
      final contacts = await FlutterContacts.getContacts();
      setState(() {
        _contacts = contacts;
        fixKontak();
      });
    }
  }

  void fixKontak() async {
    // var i = 0;
    for (var i = 0; i < _contacts!.length; i++) {
      final fullContact2 = await FlutterContacts.getContact(_contacts![i].id);
      // if (fullContact2!.phones.first.number != null) {
      // var count = i++;
      if (fullContact2!.phones.isNotEmpty &&
          fullContact2.phones.first.number != null) {
        var numberPhone;
        var phoneNumber = fullContact2.phones.first.number
            .replaceAll("-", "")
            .replaceAll(" ", "");
        if (phoneNumber.startsWith("0")) {
          numberPhone = "+62" + phoneNumber.substring(1);
        } else {
          numberPhone = phoneNumber;
        }
        Map<String, dynamic> newObj = {
          'displayName': fullContact2.displayName,
          'phones': numberPhone,
        };
        eLog("===========ContacAsli Count: ${arrKontak}===========");
        setState(() {
          arrKontak.add(newObj);
        });
      }
    }
    setState(() {
      userApi();
    });
  }

  void userApi() {
    final userB = context.read<UserBloc>();
    UserState us = userB.state;
    List<UserEntity> a = us.map(
      initial: (_) => [],
      loaded: (state) => state.users,
    );
    setState(() {
      arrUser = a.map((userEntity) {
        return {
          'id': userEntity.id,
          'username': userEntity.username,
          'phones': userEntity.phones,
        };
      }).toList();
    });
    setState(() {
      getResult();
      getResult2();
    });
  }

  void getResult() {
    // eLog("array kotan");
    // eLog(arrKontak);
    // eLog("array user");
    // eLog(arrUser);
    for (var obj2 in arrKontak) {
      for (var obj1 in arrUser) {
        if (obj1['phones'].toString() == obj2['phones']) {
          bool isAlreadyExists =
              resultExists.any((e) => e['id'] == obj1['id'].toString());

          if (!isAlreadyExists) {
            setState(() {
              Map<String, dynamic> newObj = {
                'id': obj1['id'],
                'username': obj1['username'],
                'displayName': obj2['displayName'],
                'phones': obj1['phones'],
                'isSelected': false,
              };
              // invitees.add(invitee);
              resultExists.add(newObj);
              eLog("added");
            });
          }
          // break;
        }
      }
    }
    setState(() {});
  }

  void getResult2() {
    for (var obj3 in arrKontak) {
      for (var obj4 in resultExists) {
        if (obj4['phones'].toString() != obj3['phones']) {
          bool isNotExists = resultNotExists
              .any((e) => e['phones'].toString() == obj4['phones'].toString());

          if (!isNotExists) {
            Map<String, dynamic> newObj2 = {
              'displayName': obj3['displayName'],
              'phones': obj3['phones']
            };
            setState(() {
              resultNotExists.add(newObj2);
            });
            break;
          }
        }
      }
    }
    setState(() {
      // eLog(resultNotExists.length);
    });
  }

  void addInvitee(ZegoUIKitUser invitee) {
    bool isAlreadyInvited =
        invitees.any((existingInvitee) => existingInvitee.id == invitee.id);

    if (!isAlreadyInvited) {
      setState(() {
        invitees.add(invitee);
      });
    }
  }

  void addIndexing(Map<String, dynamic> indexing) {
    bool isAlreadyInvited = index.any((e) => e['id'] == indexing['id']);

    if (!isAlreadyInvited) {
      setState(() {
        index.add(indexing);
      });
    }
  }

  void removeInvitee(ZegoUIKitUser invitee) {
    // eLog(invitee.name);
    Iterable<Map<String, dynamic>> searchResult =
        resultExists.where((element) => element['id'].toString() == invitee.id);
    if (searchResult.isNotEmpty) {
      for (var abc in searchResult) {
        var check =
            index.where((element) => element['username'] == abc['username']);
        if (check.isNotEmpty) {
          for (var def in check) {
            resultExists[int.parse(def['id'])]['isSelected'] = false;
          }
        }
      }
    }
    setState(() {
      index.removeWhere((element) => element['username'] == invitee.name);
      invitees
          .removeWhere((existingInvitee) => existingInvitee.id == invitee.id);
    });
  }

  void _redirectToSMSApp(phone) async {
    // Nomor telepon yang dituju
    String phoneNumber = phone;

    // Pesan awal yang akan ditampilkan
    String initialMessage =
        "Ayo chat di Bakamla Messenger, Dapatkan di https://www.google.com";

    // Format URL untuk membuka aplikasi SMS dengan nomor tujuan dan pesan awal
    String uri = "sms:$phoneNumber?body=${Uri.encodeFull(initialMessage)}";

    // Buka aplikasi SMS dengan URI yang dibuat
    if (await canLaunchUrl(Uri.parse(uri))) {
      await launchUrl(Uri.parse(uri));
    } else {
      throw 'Could not launch $uri';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Text("Pilih Kontak"),
              Text('${invitees.length} Kontak')
            ],
          ),
          actions: [
            IconButton(
              onPressed: () async {
                //
              },
              icon: const Icon(Icons.search),
            ),
          ],
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            //
          },
          child: resultExists.isNotEmpty
              ? Container(
                  child: Column(
                    children: [
                      invitees.isNotEmpty
                          ? Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      height: 100,
                                      width: MediaQuery.of(context).size.width *
                                          0.65,
                                      child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: invitees.length,
                                        itemBuilder: (context, index) {
                                          var namaInvitees = invitees[index];

                                          return InkWell(
                                            onTap: () {
                                              var lama = ZegoUIKitUser(
                                                  id: "${namaInvitees.id}",
                                                  name: namaInvitees.name);
                                              removeInvitee(lama);
                                            },
                                            child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  const Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 10, right: 10)),
                                                  Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        const CircleAvatar(
                                                          backgroundColor:
                                                              Color.fromARGB(
                                                                  255,
                                                                  228,
                                                                  228,
                                                                  228),
                                                          child: Icon(
                                                              Icons.person),
                                                        ),
                                                        Text(
                                                            "${namaInvitees.name}"),
                                                      ]),
                                                ]),
                                          );
                                        },
                                      ),
                                    ),
                                    ZegoSendCallInvitationButton(
                                        onPressed: (code, message, p2) {
                                          ValueType.setString("audio");
                                          List<String> names = invitees
                                              .map((invitee) => invitee.name)
                                              .toList();
                                          List<String> id = invitees
                                              .map((invitee) => invitee.id)
                                              .toList();
                                          var allName = names.join(', ');
                                          var allId = id.join(', ');
                                          Value.setString(
                                            allId,
                                            allName,
                                          );
                                        },
                                        icon: ButtonIcon(
                                            icon: const Icon(
                                              Icons.call,
                                              color: Colors.white,
                                            ),
                                            backgroundColor:
                                                const Color.fromARGB(
                                                    255, 59, 184, 21)),
                                        buttonSize: const Size(50, 50),
                                        iconSize: const Size(50, 50),
                                        iconTextSpacing: double.nan,
                                        margin: const EdgeInsets.only(
                                            left: 1, right: 5),
                                        padding: const EdgeInsets.all(0),
                                        callID: "${Random().nextInt(100)}",
                                        resourceID: "zego_call",
                                        invitees: invitees,
                                        isVideoCall: false),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    ZegoSendCallInvitationButton(
                                        onPressed: (code, message, p2) {
                                          ValueType.setString("video");
                                          List<String> names = invitees
                                              .map((invitee) => invitee.name)
                                              .toList();
                                          List<String> id = invitees
                                              .map((invitee) => invitee.id)
                                              .toList();
                                          var allName = names.join(', ');
                                          var allId = id.join(', ');
                                          Value.setString(
                                            allId,
                                            allName,
                                          );
                                        },
                                        icon: ButtonIcon(
                                            icon: const Icon(
                                              Icons.video_call,
                                              color: Colors.white,
                                            ),
                                            backgroundColor:
                                                const Color.fromARGB(
                                                    255, 59, 184, 21)),
                                        buttonSize: const Size(50, 50),
                                        iconSize: const Size(50, 50),
                                        iconTextSpacing: double.nan,
                                        margin: const EdgeInsets.only(
                                            left: 1, right: 5),
                                        padding: const EdgeInsets.all(0),
                                        callID: "${Random().nextInt(100)}",
                                        resourceID: "zego_call",
                                        invitees: invitees,
                                        isVideoCall: true),
                                  ],
                                ),
                                const Divider(),
                              ],
                            )
                          : const SizedBox(
                              height: 2,
                            ),
                      Expanded(
                        child: Container(
                            child: BlocSelector<UserBloc, UserState,
                                List<UserEntity>>(
                          selector: (state) {
                            return state.map(
                              initial: (_) => [],
                              loaded: (state) => state.users,
                            );
                          },
                          builder: (context, state) {
                            return ListView.builder(
                              itemCount: resultExists.length +
                                  resultNotExists.length -
                                  2,
                              itemBuilder: (context, i) {
                                if (i == 0) {
                                  // return the header
                                  return const Padding(
                                      padding: EdgeInsets.all(15),
                                      child: Text(
                                        "Daftar Kontak Anda di Bakamla Messenger",
                                        style: TextStyle(color: Colors.grey),
                                      ));
                                }
                                i -= 1;

                                if (i == resultExists.length) {
                                  return const Padding(
                                      padding: EdgeInsets.only(
                                          left: 15,
                                          right: 15,
                                          top: 30,
                                          bottom: 15),
                                      child: Text(
                                        "Undang Kontak ke Bakamla Messenger",
                                        style: TextStyle(color: Colors.grey),
                                      ));
                                }
                                if (i >= resultExists.length &&
                                    i <=
                                        resultExists.length +
                                            resultNotExists.length) {
                                  return Card(
                                    child: InkWell(
                                      onTap: () {
                                        _redirectToSMSApp(
                                            resultNotExists[i]['phones']);
                                      },
                                      child: ListTile(
                                        leading: const CircleAvatar(
                                          backgroundColor: Color.fromARGB(
                                              255, 228, 228, 228),
                                          child: Icon(Icons.person),
                                        ),
                                        title: Text(
                                            resultNotExists[i]["displayName"]),
                                        subtitle:
                                            Text(resultNotExists[i]["phones"]),
                                        trailing: const Text(
                                          "Undang",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.red),
                                        ),
                                      ),
                                    ),
                                  );
                                }
                                // final userku = state[i];

                                return Card(
                                  child: InkWell(
                                    onTap: () {
                                      var baru = ZegoUIKitUser(
                                          id: resultExists[i]["id"].toString(),
                                          name: resultExists[i]["username"]);
                                      if (resultExists[i]['isSelected'] ==
                                          false) {
                                        resultExists[i]['isSelected'] = true;
                                        addInvitee(baru);
                                        Map<String, dynamic> indexing = {
                                          'id': "${i}",
                                          'username': resultExists[i]
                                              ['username'],
                                        };
                                        // eLog(obj2);
                                        // invitees.add(invitee);

                                        addIndexing(indexing);
                                        setState(() {
                                          eLog(invitees);
                                        });
                                      } else {
                                        removeInvitee(baru);
                                      }
                                    },
                                    child: ListTile(
                                      // tileColor: resultExists[i]["isSelected"]
                                      //     ? Colors.red
                                      //     : Colors.white,
                                      leading: const CircleAvatar(
                                        backgroundColor:
                                            Color.fromARGB(255, 228, 228, 228),
                                        child: Icon(Icons.person),
                                      ),
                                      title: Text(
                                          "${resultExists[i]["username"]} (${resultExists[i]["phones"]})"),
                                      subtitle:
                                          Text(resultExists[i]["displayName"]),
                                      trailing: CircleAvatar(
                                        backgroundColor: resultExists[i]
                                                ['isSelected']
                                            ? Colors.red
                                            : const Color.fromARGB(
                                                255, 228, 228, 228),
                                        child: Icon(
                                          resultExists[i]["isSelected"]
                                              ? Icons.check
                                              : Icons.add,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                          },
                        )),
                      ),
                    ],
                  ),
                )
              : resultNotExists.isNotEmpty
                  ? ListView.builder(
                      itemCount: resultNotExists.length,
                      itemBuilder: (context, i) {
                        return Card(
                          child: InkWell(
                            onTap: () {
                              _redirectToSMSApp(resultNotExists[i]['phones']);
                            },
                            child: ListTile(
                              leading: const CircleAvatar(
                                backgroundColor:
                                    Color.fromARGB(255, 228, 228, 228),
                                child: Icon(Icons.person),
                              ),
                              title: Text(resultNotExists[i]["displayName"]),
                              subtitle: Text(resultNotExists[i]["phones"]),
                              trailing: const Text(
                                "Undang",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.red),
                              ),
                            ),
                          ),
                        );
                      })
                  : const Center(
                      child: CircularProgressIndicator(),
                    ),
        ));
  }
}
