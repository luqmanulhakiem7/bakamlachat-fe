import 'dart:convert';

import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chat/chat_bloc.dart';
import 'package:bakamlachat/blocs/chatgrup/chatgrup_bloc.dart';
import 'package:bakamlachat/blocs/user/user_bloc.dart';
import 'package:bakamlachat/models/user_models.dart';
import 'package:bakamlachat/screens/chat_group_list/chat_group_add_screen.dart';
import 'package:bakamlachat/screens/chat_group_list/chat_group_screen.dart';
import 'package:bakamlachat/utils/my_encription.dart';
import 'package:bakamlachat/utils/utils.dart';
import 'package:bakamlachat/utils/widget/avatar_profile.dart';
import 'package:bakamlachat/widgets/startup_container.dart';
import 'package:bakamlachat/widgets/widgets.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:search_page/search_page.dart';

class ChatGroupListScreen extends StatefulWidget {
  const ChatGroupListScreen({super.key});
  static const routeName = "group-list";

  @override
  State<ChatGroupListScreen> createState() => _ChatGroupListScreenState();
}

class _ChatGroupListScreenState extends State<ChatGroupListScreen> {
  List<dynamic> data = [];
  bool? sameMessage;

  Future<void> setupOneSignal(int userId) async {
    await initOneSignal();
    registerOneSignalEventListener(
      onOpened: onOpened,
      onReceivedInForeground: onReceivedInForeground,
    );
    promptPolicyPrivacy(userId);
  }

  void onReceivedInForeground(OSNotificationReceivedEvent event) {
    vLog(
        "notification received in foreground notification: \n${event.notification.jsonRepresentation().replaceAll("\\n", "\n")}");

    try {
      final chatBloc = context.read<ChatgrupBloc>();
      final data = event.notification.additionalData;
      final selectedChat = chatBloc.state.selectedChat;

      if (selectedChat != null && data != null) {
        vLog(data);
        final chatId = (data['data']['chatId'] as int);

        if (selectedChat.id == chatId) {
          event.complete(null);
          return;
        }
      }
      chatBloc.add(const ChatGrupStarted());
      event.complete(event.notification);
    } catch (e) {
      event.complete(null);
    }
  }

  Future<void> promptPolicyPrivacy(int userId) async {
    final oneSignalShared = OneSignal.shared;

    bool userProvidedPrivacyConsent =
        await oneSignalShared.userProvidedPrivacyConsent();

    if (userProvidedPrivacyConsent) {
      sendUserTag(userId);
    } else {
      bool requiresConsent = await oneSignalShared.requiresUserPrivacyConsent();
      if (requiresConsent) {
        final accepted =
            await OneSignal.shared.promptUserForPushNotificationPermission();
        if (accepted) {
          await oneSignalShared.consentGranted(true);
          sendUserTag(userId);
        }
      } else {
        sendUserTag(userId);
      }
    }
  }

  // void initState() {
  //   super.initState();
  //   getChats();
  // }

  // Future<void> getChats() async {
  //   const String path = "https://api.bakamla.barengsaya.com/api/chat-group";
  //   final String token = AuthBloc().state.token.toString();
  //   try {
  //     final resp = await Dio().get(path,
  //         options: Options(
  //           headers: {
  //             "Accpet": "application/json",
  //             "Authorization": 'Bearer $token',
  //           },
  //         ));
  //     if (resp.statusCode == 200) {
  //       eLog("success");
  //       setState(() {
  //         data = resp.data['data'];
  //       });

  //       eLog(data[0]['last_message']['message']);
  //     } else {
  //       eLog("error status code");
  //     }
  //   } catch (e) {
  //     eLog("Error $e");
  //   }
  // }

  void onOpened(OSNotificationOpenedResult result) {
    vLog('notification opened handler called with: ${result}');

    vLog(
        "opened notification: \n${result.notification.jsonRepresentation().replaceAll("\\n", "\n")}");

    try {
      final data = result.notification.additionalData;
      if (data != null) {
        final chatId = (data['data']['chatId'] as int);
        final chatBloc = context.read<ChatgrupBloc>();
        final selectedChat = chatBloc.state.selectedChat;

        if (chatId != selectedChat?.id) {
          chatBloc.add(ChatGrupNotificationOpened(chatId));
          Navigator.of(context).pushNamed(ChatGrupScreen.routeName);
        }
      }
    } catch (_) {}
  }
  // void cekSameMessage(chats, index){
  //   sameMessage => chats[index].lastmessage?.message == chats[index].lastmessage?.images;
  // }

  @override
  Widget build(BuildContext context) {
    final authBloc = context.read<AuthBloc>();
    final currentUser = authBloc.state.user!;
    final chatBloc = context.read<ChatgrupBloc>();

    return StartUpContainer(
      onInit: () async {
        chatBloc.add(const ChatGrupStarted());

        // LaravelEcho.init(token: authBloc.state.token!);
        // setupOneSignal(authBloc.state.user!.id);
      },
      onDisposed: () {
        // chatBloc.add(const ChatGrupReset());
        // LaravelEcho.instance.disconnect();
      },
      child: Scaffold(
        body: RefreshIndicator(
          onRefresh: () async {
            chatBloc.add(const ChatGrupStarted());
          },
          child: BlocConsumer<ChatgrupBloc, ChatgrupState>(
            listener: (_, __) {},
            builder: (context, state) {
              // eLog(authBloc.state.user!.id);

              if (state.chats.isEmpty) {
                return const BlankContent(
                  content: "Tidak ada chat",
                  icon: Icons.chat_rounded,
                );
              }
              return ListView.separated(
                itemBuilder: (context, index) {
                  return ListTile(
                      key: ValueKey(state.chats[index]),
                      leading: state.chats[index].avatar != null
                          ? AvatarProfile(
                              avatar: state.chats[index].avatar,
                              height: 50,
                              width: 50,
                            )
                          : const CircleAvatar(
                              backgroundColor:
                                  Color.fromARGB(255, 228, 228, 228),
                              child: Icon(Icons.person),
                            ),
                      title: Text(state.chats[index].name.toString()),
                      subtitle: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            flex: 3,
                            child: Text(
                              state.chats[index].lastmessage != null
                                  ? state.chats[index].lastmessage!.hasKey
                                      ? MyEncryptionDecryption.decryptAES(
                                          state.chats[index].lastmessage!
                                              .message,
                                          state.chats[index].lastmessage!.ivkey
                                              .toString())
                                      : state.chats[index].lastmessage!.message
                                  : "Belum Ada Pesan",
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                            ),
                          ),
                          SizedBox(width: 10),
                          Text(utcToLocal(state.chats[index].updatedAt)),
                        ],
                      ),
                      onLongPress: () async {
                        return _showDeleteConfirmationDialog(
                            context, state.chats[index].id.toString());
                      },
                      onTap: () {
                        chatBloc.add(ChatGrupSelected(state.chats[index]));
                        Navigator.of(context)
                            .pushNamed(ChatGrupScreen.routeName);
                      });
                },
                separatorBuilder: (_, __) => const Divider(
                  height: 1.5,
                ),
                itemCount: state.chats.length,
              );
            },
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => const ChatGroupAddScreen()),
            );
          },
          child: const Icon(Icons.group_add),
        ),
      ),
    );
  }

  void _showDeleteConfirmationDialog(BuildContext context, String idMsg) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Bersihkan Pesan'),
          content: const Text(
              'Apakah anda yakin ingin membersihkan pesan pada chat ini?'),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Bersihkan'),
              onPressed: () async {
                final token = AuthBloc().state.token.toString();
                Dio dio = Dio();
                final urlId =
                    'https://api.bakamla.barengsaya.com/api/chat_reset/$idMsg';
                eLog(urlId);
                try {
                  final response = await dio.get(
                    urlId,
                    options: Options(
                      headers: {'Authorization': 'Bearer $token'},
                    ),
                  );
                  if (response.statusCode == 200) {
                    // ignore: use_build_context_synchronously
                    final chatBloc = context.read<ChatBloc>();
                    chatBloc.add(const ChatStarted());
                  } else {
                    eLog('code bukan 200');
                  }
                } catch (e) {
                  eLog(e);
                }
                // _deleteMessage(message);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _showSearch(BuildContext context, List<UserEntity> users) {
    showSearch(
      context: context,
      delegate: SearchPage<UserEntity>(
        items: users,
        searchLabel: 'Cari Kontak',
        suggestion: const Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.search,
                size: 25.0,
                color: Colors.grey,
              ),
              Text(
                'Cari Kontak berdasarkan nama',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
        failure: const Center(
          child: Text(
            'Kontak tidak ditemukan :(',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.w400,
              color: Colors.grey,
            ),
          ),
        ),
        filter: (user) => [
          user.email,
        ],
        builder: (user) => Row(
          children: [
            ListTile(
              leading: const Icon(Icons.account_circle, size: 50.0),
              title: Text(user.username),
              subtitle: Text(user.email),
              onTap: () {
                // Selected User
                vLog("usernya : ${user.id}");

                // Navigator.of(context).pushNamed(ChatScreen.routeName);
              },
            ),
          ],
        ),
      ),
    );
  }
}
