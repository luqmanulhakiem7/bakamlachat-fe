import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class ChatGroupEditNamaScreen extends StatefulWidget {
  final String namaGrup;
  final String idGrup;
  const ChatGroupEditNamaScreen(
      {super.key, required this.namaGrup, required this.idGrup});

  @override
  State<ChatGroupEditNamaScreen> createState() => _ChatGroupEditNamaScreen();
}

class _ChatGroupEditNamaScreen extends State<ChatGroupEditNamaScreen> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    // Set default value here
    _controller.text = widget.namaGrup;
  }

  void save(text) async {
    eLog(text);
    final token = AuthBloc().state.token.toString();
    Dio dio = Dio();
    var formData = {
      'nama': text,
    };
    final urlId =
        'https://api.bakamla.barengsaya.com/api/chat-group-nama/${widget.idGrup}';
    eLog(urlId);
    try {
      final response = await dio.post(
        urlId,
        data: formData,
        options: Options(
          headers: {'Authorization': 'Bearer $token'},
        ),
      );
      if (response.statusCode == 200) {
        // ignore: use_build_context_synchronously
        eLog('berhasil');
      } else {
        eLog('code bukan 200');
      }
    } catch (e) {
      eLog(e);
    }
    // _deleteMessage(message);
    Navigator.of(context)
      ..pop()
      ..pop()
      ..pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Nama Grup"),
        actions: [
          ElevatedButton(
              onPressed: () {
                save(_controller.text);
              },
              child: Text("Simpan"))
        ],
      ),
      body: Container(
        padding:
            const EdgeInsets.all(16.0), // Add padding for better appearance
        child: TextField(
          controller: _controller,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Masukkan Nama Grup',
          ),
        ),
      ),
    );
  }
}
