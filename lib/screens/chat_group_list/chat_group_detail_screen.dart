import 'dart:io';

import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chatgrup/chatgrup_bloc.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/screens/chat_group_list/chat_group_add_anggota_screen.dart';
import 'package:bakamlachat/screens/chat_group_list/chat_group_edit_nama_screen.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:bakamlachat/utils/widget/avatar_profile.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ChatGroupDetailScreen extends StatefulWidget {
  static const routeName = "chat-detail-grup-screen";

  final String chatId;
  final Map<String, dynamic> data;
  final List<dynamic> participants;

  const ChatGroupDetailScreen(
      {super.key,
      required this.chatId,
      required this.data,
      required this.participants});

  @override
  State<ChatGroupDetailScreen> createState() => _ChatGroupDetailScreen();
}

class _ChatGroupDetailScreen extends State<ChatGroupDetailScreen> {
  // final profileBloc = ProfileBloc();
  final _picker = ImagePicker();

  Future<void> pickImage(context) async {
    final XFile? pickedFile =
        await _picker.pickImage(source: ImageSource.gallery, imageQuality: 20);
    if (pickedFile != null) {
      File imageFile = File(pickedFile.path);
      _cropImage(imageFile, context);
    }
  }

  Future<void> _cropImage(image, context) async {
    if (image != null) {
      final croppedFile = await ImageCropper().cropImage(
        sourcePath: image!.path,
        compressFormat: ImageCompressFormat.jpg,
        compressQuality: 100,
        aspectRatioPresets: [
          CropAspectRatioPreset.ratio4x3,
        ],
        uiSettings: [
          AndroidUiSettings(
              toolbarTitle: 'Potong Gambar',
              toolbarColor: Colors.deepOrange,
              toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.original,
              lockAspectRatio: false),
          IOSUiSettings(
            title: 'Potong Gambar',
          ),
          WebUiSettings(
            context: context,
            presentStyle: CropperPresentStyle.dialog,
            boundary: const CroppieBoundary(
              width: 520,
              height: 520,
            ),
            viewPort:
                const CroppieViewPort(width: 480, height: 480, type: 'circle'),
            enableExif: true,
            enableZoom: true,
            showZoomer: true,
          ),
        ],
      );
      if (croppedFile != null) {
        updateAvatar(croppedFile.path);
      }
    }
  }

  Future<void> updateAvatar(String path) async {
    FormData formData = FormData.fromMap({
      'avatar': await MultipartFile.fromFile(path, filename: 'aba'),
    });
    eLog(formData);
    final token = AuthBloc().state.token.toString();
    Dio dio = Dio();
    final urlId =
        'https://api.bakamla.barengsaya.com/api/chat-group-avatar/${widget.data['id']}';
    eLog(urlId);
    try {
      final response = await dio.post(
        urlId,
        data: formData,
        options: Options(
          headers: {'Authorization': 'Bearer $token'},
        ),
      );
      if (response.statusCode == 200) {
        // ignore: use_build_context_synchronously
        eLog('berhasil');
      } else {
        eLog('code bukan 200');
      }
    } catch (e) {
      eLog(e);
    }
    // _deleteMessage(message);
    final chatBloc = context.read<ChatgrupBloc>();
    chatBloc.add(const ChatgrupEvent.reset());
    chatBloc.add(const ChatgrupEvent.started());

    Navigator.of(context)
      ..pop()
      ..pop();
    // profileBloc.add(ProfileEvent.changeProfile(
    //   data: formData,
    //   type: 'profile',
    // ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.data['name']),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChatGroupEditNamaScreen(
                        namaGrup: widget.data['name'],
                        idGrup: widget.data["id"].toString(),
                      ),
                    ),
                  );
                },
                icon: const Icon(Icons.edit)),
            IconButton(
              onPressed: () async {
                final chatBloc = context.read<ChatgrupBloc>();

                final chatid = chatBloc.state.selectedChat!.id;
                final token = AuthBloc().state.token.toString();
                Dio dio = Dio();
                final urlId =
                    'https://api.bakamla.barengsaya.com/api/chat-group-detail/$chatid';
                try {
                  final response = await dio.get(
                    urlId,
                    options: Options(
                      headers: {'Authorization': 'Bearer $token'},
                    ),
                  );
                  if (response.statusCode == 200) {
                    // ignore: use_build_context_synchronously
                    eLog(response.data['participants'][0]);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChatGroupAddAnggotaScreen(
                          data: response.data['participants'],
                        ),
                      ),
                    );
                  } else {
                    eLog('code bukan 200');
                  }
                } catch (e) {
                  eLog(e);
                }
              },
              icon: const Icon(Icons.person_add),
            )
          ],
        ),
        body: Column(
          children: [
            Center(
              child: GestureDetector(
                onTap: () {
                  // Handle tap event on image
                  eLog('Image was tapped!');
                },
                child: Stack(
                  children: [
                    AvatarProfile(
                      avatar: widget.data['avatar'],
                      height: 100,
                      width: 100,
                    ),
                    Positioned(
                      bottom: 0,
                      right: 1,
                      child: CircleAvatar(
                        child: IconButton(
                          icon: const Icon(Icons.camera_alt),
                          onPressed: () {
                            pickImage(context);
                            // Handle tap event on button icon
                            // widget.viewModel.pickImage(context);
                            eLog('Button icon was tapped!');
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            const Text("Anggota Grup"),
            Expanded(
              child: Container(
                child: ListView.builder(
                    itemCount: widget.participants.length,
                    itemBuilder: (context, index) {
                      eLog(
                          "ID PARTICIPANT: ${widget.participants[index]['id']}");
                      eLog("ID : ${widget.data["idKreator"]}");
                      eLog("IDAktif : ${AuthBloc().state.user!.id}");
                      eLog(widget.data["idKreator"] ==
                          AuthBloc().state.user!.id.toString());

                      return Card(
                        child: InkWell(
                          onLongPress: () {
                            if (widget.data["idKreator"] !=
                                widget.participants[index]['id']) {
                              _showDeleteConfirmationDialog(context,
                                  widget.participants[index]['id'].toString());
                            }
                          },
                          child: ListTile(
                            leading:
                                widget.participants[index]['avatar'] != null
                                    ? CircleAvatar(
                                        radius: 20.0,
                                        backgroundImage: NetworkImage(
                                            Endpoints.avatar +
                                                widget.participants[index]
                                                    ['avatar']),
                                      )
                                    : const CircleAvatar(
                                        backgroundColor:
                                            Color.fromARGB(255, 228, 228, 228),
                                        child: Icon(Icons.person),
                                      ),
                            title: Text(
                                "${widget.participants[index]['username']}"),
                            trailing: widget.data["idKreator"] ==
                                    widget.participants[index]['id']
                                ? TextButton(
                                    onPressed: () {
                                      //
                                    },
                                    child: Text("Kreator"))
                                : Text(""),
                          ),
                        ),
                      );
                    }),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            widget.data["idKreator"] == AuthBloc().state.user!.id.toString()
                ? OutlinedButton.icon(
                    onPressed: () async {
                      _showBubarkanGrup(context);
                    },
                    style: OutlinedButton.styleFrom(
                      fixedSize: const Size(300, 70),
                      padding: const EdgeInsets.all(20.0),
                      backgroundColor: Colors.white70,
                    ),
                    label: const Text(
                      'Bubarkan Grup',
                      style: TextStyle(
                        color: Colors.blue,
                      ),
                    ),
                    icon: const Icon(Icons.door_sliding),
                  )
                : OutlinedButton.icon(
                    onPressed: () async {
                      final token = AuthBloc().state.token.toString();
                      Dio dio = Dio();
                      final urlId =
                          'https://api.bakamla.barengsaya.com/api/chat-group-hapusanggota/${AuthBloc().state.user!.id}/${widget.data['id']}';
                      try {
                        final response = await dio.get(
                          urlId,
                          options: Options(
                            headers: {'Authorization': 'Bearer $token'},
                          ),
                        );
                        if (response.statusCode == 200) {
                          // ignore: use_build_context_synchronously
                          eLog('berhasil');
                        } else {
                          eLog('code bukan 200');
                        }
                      } catch (e) {
                        eLog(e);
                      }
                      // _deleteMessage(message);
                      Navigator.of(context)
                        ..pop()
                        ..pop();
                    },
                    style: OutlinedButton.styleFrom(
                      fixedSize: const Size(300, 70),
                      padding: const EdgeInsets.all(20.0),
                      backgroundColor: Colors.white70,
                    ),
                    label: const Text(
                      'Keluar dari Grup',
                      style: TextStyle(
                        color: Colors.blue,
                      ),
                    ),
                    icon: const Icon(Icons.door_sliding),
                  ),
            const SizedBox(
              height: 50,
            )
          ],
        ));
  }

  void _showDeleteConfirmationDialog(BuildContext context, String idMember) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Keluarkan'),
          content:
              const Text('Apakah anda yakin ingin mengeluarkan anggota ini?'),
          actions: <Widget>[
            TextButton(
              child: const Text('Batal'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Keluarkan'),
              onPressed: () async {
                final token = AuthBloc().state.token.toString();
                Dio dio = Dio();
                final urlId =
                    'https://api.bakamla.barengsaya.com/api/chat-group-hapusanggota/$idMember/${widget.data['id']}';
                eLog(urlId);
                try {
                  final response = await dio.get(
                    urlId,
                    options: Options(
                      headers: {'Authorization': 'Bearer $token'},
                    ),
                  );
                  if (response.statusCode == 200) {
                    // ignore: use_build_context_synchronously
                    eLog('berhasil');
                  } else {
                    eLog('code bukan 200');
                  }
                } catch (e) {
                  eLog(e);
                }
                // _deleteMessage(message);
                Navigator.of(context)
                  ..pop()
                  ..pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _showBubarkanGrup(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Bubarkan'),
          content: const Text('Apakah anda yakin ingin Membubarkan grup ini?'),
          actions: <Widget>[
            TextButton(
              child: const Text('Batal'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Bubarkan'),
              onPressed: () async {
                final token = AuthBloc().state.token.toString();
                Dio dio = Dio();
                final urlId =
                    'https://api.bakamla.barengsaya.com/api/chat-group-delete/${widget.data['id']}';
                eLog(urlId);
                try {
                  final response = await dio.get(
                    urlId,
                    options: Options(
                      headers: {'Authorization': 'Bearer $token'},
                    ),
                  );
                  if (response.statusCode == 200) {
                    // ignore: use_build_context_synchronously
                    eLog('berhasil');
                  } else {
                    eLog('code bukan 200');
                  }
                } catch (e) {
                  eLog(e);
                }
                // _deleteMessage(message);
                Navigator.of(context)
                  ..pop()
                  ..pop()
                  ..pop();
              },
            ),
          ],
        );
      },
    );
  }
}
