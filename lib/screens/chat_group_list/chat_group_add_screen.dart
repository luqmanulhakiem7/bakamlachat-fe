import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chatgrup/chatgrup_bloc.dart';
import 'package:bakamlachat/blocs/user/user_bloc.dart';
import 'package:bakamlachat/models/user_models.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/utils/laravel_echo/laravel_echo.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toast/toast.dart';

class ChatGroupAddScreen extends StatefulWidget {
  const ChatGroupAddScreen({super.key});

  @override
  State<ChatGroupAddScreen> createState() => _ChatGroupAddScreen();
}

class _ChatGroupAddScreen extends State<ChatGroupAddScreen> {
  List<dynamic> userApi = [];
  List<Map<String, dynamic>> invitees = [];
  final TextEditingController _controller = TextEditingController();
  final idAuth = AuthBloc().state.user!.id;
  final token = AuthBloc().state.token;

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  void addGrup(String name, List<Map<String, dynamic>> undangan) async {
    List<dynamic> array = undangan.map((map) => map["id"]).toList();
    array.add(idAuth);
    List<int> intList = array.map((element) => element as int).toList();
    var formData = {
      'name': name,
      'user_id': intList,
    };
    eLog(intList);
    final grupBloc = context.read<ChatgrupBloc>();

    try {
      final resp = await Dio().post(Endpoints.createGrupChat,
          data: formData,
          options: Options(
            headers: {
              "Content-Type": "application/json",
              "Accept": "application/json",
              "Authorization": "Bearer ${token}",
              'X-Socket-ID': LaravelEcho.instance.socketId(),
            },
          ));
      if (resp.statusCode == 200) {
        eLog("Berhasil");
        grupBloc.add(
            const ChatGrupStarted()); //cara navigasi pop dan menjalankan perintah ini
        Navigator.of(context).pop();
      } else {
        Toast.show("Gagal", gravity: Toast.bottom);
      }
      // ignore: deprecated_member_use
    } on DioError catch (e) {
      // eLog("HTTP Error: ${e.message}");
      // Toast.show("HTTP Error", gravity: Toast.bottom);
      grupBloc.add(
          const ChatGrupStarted()); //cara navigasi pop dan menjalankan perintah ini
      Navigator.of(context).pop();
    } catch (e) {
      eLog("Error: $e");
      Toast.show("Request Error", gravity: Toast.bottom);
    }
  }

  void _getUser() async {
    final contact = await Dio().get(Endpoints.contact,
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          },
        ));

    setState(() {
      userApi = contact.data.map((item) {
        Map<String, dynamic> user = Map<String, dynamic>.from(item['user'][0]);
        user['isSelected'] = false; // Assuming default value is false
        return user;
      }).toList();
    });
  }

  void addOrRemoveInvites(Map<String, dynamic> user) {
    bool isAlreadyInvited =
        invitees.any((existingInvitee) => existingInvitee['id'] == user['id']);

    if (!isAlreadyInvited) {
      setState(() {
        userApi[user['index']]['isSelected'] = true;
        invitees.add(user);
      });
    } else {
      setState(() {
        userApi[user['index']]['isSelected'] = false;
        invitees.removeWhere(
            (existingInvitee) => existingInvitee['id'] == user['id']);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    eLog(userApi);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Buat Grup"),
        // actions: [
        //   IconButton(
        //       onPressed: () {
        //         eLog(userApi);
        //       },
        //       icon: const Icon(Icons.search)),
        // ],
      ),
      body: Container(
        child: userApi.length <= 0
            ?
            // const Center(
            //     child: CircularProgressIndicator(),
            //   )
            const Center(
                child: Text(
                  'Belum Memiliki Teman :(',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey,
                  ),
                ),
              )
            : Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(
                        16.0), // Add padding for better appearance
                    child: TextField(
                      controller: _controller,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Masukkan Nama Grup',
                      ),
                    ),
                  ),
                  const Divider(),
                  invitees.isNotEmpty
                      ? Column(
                          children: [
                            Row(
                              children: [
                                SizedBox(
                                  height: 100,
                                  width: MediaQuery.of(context).size.width * 1,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: invitees.length,
                                    itemBuilder: (context, index) {
                                      return InkWell(
                                        onTap: () {
                                          eLog(invitees[index]);
                                          addOrRemoveInvites(invitees[index]);
                                        },
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              const Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 10, right: 10)),
                                              Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    invitees[index]['avatar'] !=
                                                            null
                                                        ? CircleAvatar(
                                                            radius: 20.0,
                                                            backgroundImage:
                                                                NetworkImage(Endpoints
                                                                        .avatar +
                                                                    invitees[
                                                                            index]
                                                                        [
                                                                        'avatar']))
                                                        : const CircleAvatar(
                                                            backgroundColor:
                                                                Color.fromARGB(
                                                                    255,
                                                                    228,
                                                                    228,
                                                                    228),
                                                            child: Icon(
                                                                Icons.person),
                                                          ),
                                                    Text(
                                                        "${invitees[index]['username']}"),
                                                  ]),
                                            ]),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                            const Divider(),
                          ],
                        )
                      : const SizedBox(
                          height: 2,
                        ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: userApi.length,
                      itemBuilder: (context, i) {
                        return Card(
                          child: InkWell(
                            onTap: () {
                              var user = {
                                'id': userApi[i]['id'],
                                'username': userApi[i]['username'],
                                'avatar': userApi[i]['avatar'],
                                'index': i,
                              };
                              addOrRemoveInvites(user);
                            },
                            child: ListTile(
                              leading: userApi[i]['avatar'] != null
                                  ? CircleAvatar(
                                      radius: 20.0,
                                      backgroundImage: NetworkImage(
                                          Endpoints.avatar +
                                              userApi[i]['avatar']))
                                  : const CircleAvatar(
                                      backgroundColor:
                                          Color.fromARGB(255, 228, 228, 228),
                                      child: Icon(Icons.person),
                                    ),
                              title: Text("${userApi[i]['username']}"),
                              // subtitle: Text("${userApi[i]['kta']}"),
                              subtitle: const Text(""),
                              trailing: CircleAvatar(
                                backgroundColor: userApi[i]['isSelected']
                                    ? Colors.red
                                    : const Color.fromARGB(255, 228, 228, 228),
                                child: Icon(
                                  userApi[i]["isSelected"]
                                      ? Icons.check
                                      : Icons.add,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (invitees.isNotEmpty) {
            addGrup(_controller.text, invitees);
          } else {
            Toast.show("User Belum Dipilih", gravity: Toast.bottom);
          }
        },
        child: const Icon(Icons.navigate_next),
      ),
    );
  }
}
