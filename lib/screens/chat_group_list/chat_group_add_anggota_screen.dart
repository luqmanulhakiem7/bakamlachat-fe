import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chatgrup/chatgrup_bloc.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/utils/laravel_echo/laravel_echo.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toast/toast.dart';

class ChatGroupAddAnggotaScreen extends StatefulWidget {
  static const routeName = "chat-grup-add-anggota-screen";

  final List<dynamic> data;

  const ChatGroupAddAnggotaScreen({super.key, required this.data});

  @override
  State<ChatGroupAddAnggotaScreen> createState() =>
      _ChatGroupAddAnggotaScreen();
}

class _ChatGroupAddAnggotaScreen extends State<ChatGroupAddAnggotaScreen> {
  List<dynamic> userApi = [];
  List<Map<String, dynamic>> invitees = [];
  final TextEditingController _controller = TextEditingController();
  final idAuth = AuthBloc().state.user!.id;
  final token = AuthBloc().state.token;

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  void addGrup(List<Map<String, dynamic>> id) async {
    List<dynamic> array = id.map((map) => map["id"]).toList();
    List<int> intList = array.map((element) => element as int).toList();
    var formData = {
      'id': intList,
    };
    eLog(intList);
    final grupBloc = context.read<ChatgrupBloc>();

    try {
      final chatBloc = context.read<ChatgrupBloc>();

      final chatid = chatBloc.state.selectedChat!.id;
      final token = AuthBloc().state.token.toString();
      Dio dio = Dio();
      final urlId =
          'https://api.bakamla.barengsaya.com/api/chat-group-tambahanggota/$chatid';
      try {
        final response = await dio.post(
          urlId,
          data: formData,
          options: Options(
            headers: {'Authorization': 'Bearer $token'},
          ),
        );
        if (response.statusCode == 200) {
          // ignore: use_build_context_synchronously
          eLog(response.data['participants'][0]);
          Navigator.of(context)
            ..pop()
            ..pop()
            ..pop();
        } else {
          eLog('code bukan 200');
        }
      } catch (e) {
        // eLog(e);
        Navigator.of(context)
          ..pop()
          ..pop()
          ..pop();
      }
      // ignore: deprecated_member_use
    } on DioError catch (e) {
      // eLog("HTTP Error: ${e.message}");
      // Toast.show("HTTP Error", gravity: Toast.bottom);
      Navigator.of(context)
        ..pop()
        ..pop();
    } catch (e) {
      eLog("Error: $e");
      Toast.show("Request Error", gravity: Toast.bottom);
    }
  }

  void _getUser() async {
    final contact = await Dio().get(Endpoints.contact,
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          },
        ));
    List<dynamic> excludedIds = widget.data.map((item) => item['id']).toList();
    eLog(
        "DSINSI : ${excludedIds}"); //saya ingin mengambil contact tapi dengan id yang tidak ad ada pada widget.data["index"]["id"]

    setState(() {
      eLog(excludedIds);
      userApi = contact.data.where((item) {
        int uId = item['user'][0]["id"];
        eLog("bukan bagian dari : ${!excludedIds.contains(uId)}");
        return !excludedIds.contains(uId);
      }).map((item) {
        Map<String, dynamic> user = Map<String, dynamic>.from(item['user'][0]);
        user['isSelected'] = false; // Assuming default value is false
        return user;
      }).toList();
    });
  }

  void addOrRemoveInvites(Map<String, dynamic> user) {
    bool isAlreadyInvited =
        invitees.any((existingInvitee) => existingInvitee['id'] == user['id']);

    if (!isAlreadyInvited) {
      setState(() {
        userApi[user['index']]['isSelected'] = true;
        invitees.add(user);
      });
    } else {
      setState(() {
        userApi[user['index']]['isSelected'] = false;
        invitees.removeWhere(
            (existingInvitee) => existingInvitee['id'] == user['id']);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tambah Anggota"),
      ),
      body: Container(
        child: userApi.length <= 0
            ?
            // const Center(
            //     child: CircularProgressIndicator(),
            //   )
            const Center(
                child: Text(
                  'Belum Memiliki Teman :(',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey,
                  ),
                ),
              )
            : Column(
                children: [
                  invitees.isNotEmpty
                      ? Column(
                          children: [
                            Row(
                              children: [
                                SizedBox(
                                  height: 100,
                                  width: MediaQuery.of(context).size.width * 1,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: invitees.length,
                                    itemBuilder: (context, index) {
                                      return InkWell(
                                        onTap: () {
                                          eLog(invitees[index]);
                                          addOrRemoveInvites(invitees[index]);
                                        },
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              const Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 10, right: 10)),
                                              Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    invitees[index]['avatar'] !=
                                                            null
                                                        ? CircleAvatar(
                                                            radius: 20.0,
                                                            backgroundImage:
                                                                NetworkImage(Endpoints
                                                                        .avatar +
                                                                    invitees[
                                                                            index]
                                                                        [
                                                                        'avatar']))
                                                        : const CircleAvatar(
                                                            backgroundColor:
                                                                Color.fromARGB(
                                                                    255,
                                                                    228,
                                                                    228,
                                                                    228),
                                                            child: Icon(
                                                                Icons.person),
                                                          ),
                                                    Text(
                                                        "${invitees[index]['username']}"),
                                                  ]),
                                            ]),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                            const Divider(),
                          ],
                        )
                      : const SizedBox(
                          height: 2,
                        ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: userApi.length,
                      itemBuilder: (context, i) {
                        return Card(
                          child: InkWell(
                            onTap: () {
                              var user = {
                                'id': userApi[i]['id'],
                                'username': userApi[i]['username'],
                                'avatar': userApi[i]['avatar'],
                                'index': i,
                              };
                              addOrRemoveInvites(user);
                            },
                            child: ListTile(
                              leading: userApi[i]['avatar'] != null
                                  ? CircleAvatar(
                                      radius: 20.0,
                                      backgroundImage: NetworkImage(
                                          Endpoints.avatar +
                                              userApi[i]['avatar']))
                                  : const CircleAvatar(
                                      backgroundColor:
                                          Color.fromARGB(255, 228, 228, 228),
                                      child: Icon(Icons.person),
                                    ),
                              title: Text("${userApi[i]['username']}"),
                              // subtitle: Text("${userApi[i]['kta']}"),
                              subtitle: const Text(""),
                              trailing: CircleAvatar(
                                backgroundColor: userApi[i]['isSelected']
                                    ? Colors.red
                                    : const Color.fromARGB(255, 228, 228, 228),
                                child: Icon(
                                  userApi[i]["isSelected"]
                                      ? Icons.check
                                      : Icons.add,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (invitees.isNotEmpty) {
            addGrup(invitees);
          } else {
            Toast.show("User Belum Dipilih", gravity: Toast.bottom);
          }
        },
        child: const Icon(Icons.navigate_next),
      ),
    );
  }
}
