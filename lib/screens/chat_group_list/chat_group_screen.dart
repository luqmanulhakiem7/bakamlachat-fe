import 'dart:convert';
import 'dart:io';

import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chatgrup/chatgrup_bloc.dart';
import 'package:bakamlachat/models/chat_model.dart';
import 'package:bakamlachat/models/models.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/screens/chat/contact_detail_screen.dart';
import 'package:bakamlachat/screens/chat/data.dart';
import 'package:bakamlachat/screens/chat/image_screen.dart';
import 'package:bakamlachat/screens/chat/list_menu_item.dart';
import 'package:bakamlachat/screens/chat/location_screen.dart';
import 'package:bakamlachat/screens/chat/play_screen.dart';
import 'package:bakamlachat/screens/chat_group_list/chat_group_detail_screen.dart';
import 'package:bakamlachat/utils/laravel_echo/laravel_echo.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:bakamlachat/widgets/startup_container.dart';
import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:just_audio/just_audio.dart';
import 'package:lecle_downloads_path_provider/lecle_downloads_path_provider.dart';
import 'package:pusher_client_fixed/pusher_client_fixed.dart';
import 'package:record/record.dart';
import 'package:toast/toast.dart';
import 'package:encrypt/encrypt.dart' as enkripsi;

class ChatGrupScreen extends StatefulWidget {
  static const routeName = "chat-grup-screen";

  const ChatGrupScreen({super.key});

  @override
  State<ChatGrupScreen> createState() => _ChatGrupScreenState();
}

class _ChatGrupScreenState extends State<ChatGrupScreen> {
  List<ChatMessage>? chatMsgs;

  List<ChatMessage> messages = allUsersSample;
  late TextEditingController _textController;
  late AudioPlayer player;
  final record = Record();
  final _picker = ImagePicker();

  void playAudio(name) async {
    String soundUrl = "${Endpoints.sound}$name";
    await player.setUrl(soundUrl);
    player.play();
  }

  void startRecord() async {
    if (await record.hasPermission()) {
      await record.start();
    }
  }

  void stopRecord() async {
    String? path = await record.stop();
    uploadVoiceMessage("$path");
  }

  Future<void> uploadVoiceMessage(String path) async {
    final token = AuthBloc().state.token.toString();
    // ignore: use_build_context_synchronously
    final chatBloc = context.read<ChatgrupBloc>();
    final state = chatBloc.state.selectedChat!.id;

    // ignore: unused_local_variable
    File voiceFile = File(path);
    String basename = path.split('/').last;

    var formData = FormData.fromMap({
      'chat_id': state,
      'audios': await MultipartFile.fromFile(path, filename: basename),
      'message': ' ',
    });

    chatBloc.add(sendGrupMedia(formData, token));
  }

  void listenChatChannel(ChatEntity chat) {
    LaravelEcho.instance.private('chat.${chat.id}').listen('.message.sent',
        (e) {
      if (e is PusherEvent) {
        if (e.data != null) {
          _handleNewMessage(jsonDecode(e.data!));
          vLog(jsonDecode(e.data!));
        }
      }
    }).error((err) {
      eLog(err);
    });
  }

  void leaveChatChannel(ChatEntity chat) {
    try {
      LaravelEcho.instance.leave('chat.${chat.id}');
    } catch (err) {
      eLog(err);
    }
  }

  void _handleNewMessage(Map<String, dynamic> data) {
    final chatBloc = context.read<ChatgrupBloc>();
    final selectedChat = chatBloc.state.selectedChat!;
    if (selectedChat.id == data['chat_id']) {
      final chatMessage = ChatMessageEntity.fromJson(data['message']);
      eLog(chatMessage);
      chatBloc.add(AddNewGrupMessage(chatMessage));
      playAudio('waterdrop.wav');
    }
  }

  Future<void> playRecordedAudio(String path) async {
    try {
      String soundUrl = "${Endpoints.audio}$path";
      await player.setUrl(soundUrl);
      player.play();
    } catch (e) {
      eLog("gagal play audio: $e");
    }
  }

  @override
  void initState() {
    super.initState();
    runEncrypt();
    _textController = TextEditingController();
    player = AudioPlayer();
    // getChats();
  }

  // Future<void> getChats() async {
  //   const String path = "https://api.bakamla.barengsaya.com/api/chat-group";
  //   final String token = AuthBloc().state.token.toString();
  //   try {
  //     final resp = await Dio().get(path,
  //         queryParameters: {
  //           'chat_id': '16',
  //           'page': '1',
  //         },
  //         options: Options(
  //           headers: {
  //             "Accept": "application/json",
  //             "Authorization": 'Bearer $token',
  //           },
  //         ));
  //     if (resp.statusCode == 200) {
  //       eLog(resp.data);
  //       setState(() {
  //         // chatMsgs = resp.data as List<dynamic>;
  //       });
  //     } else {
  //       eLog("error status code");
  //     }
  //   } catch (e) {
  //     eLog("Error $e");
  //   }
  // }

  bool isSuffixHidden = true;

  late final keyA;
  late final iv;
  late final encrypter;

  void runEncrypt() {
    setState(() {
      keyA = enkripsi.Key.fromUtf8('my 32 length key.is.bakamla.....');
      iv = enkripsi.IV.fromLength(16);
      encrypter = enkripsi.Encrypter(enkripsi.AES(keyA));
    });
  }

  @override
  Widget build(BuildContext context) {
    final chatBloc = context.read<ChatgrupBloc>();
    final authBloc = context.read<AuthBloc>();
    String namaGrup = "";

    return StartUpContainer(
      onInit: () {
        chatBloc.add(const GetGrupChatMessage());
        if (chatBloc.state.selectedChat != null) {
          listenChatChannel(chatBloc.state.selectedChat!);
        }
      },
      onDisposed: () {
        leaveChatChannel(chatBloc.state.selectedChat!);
        chatBloc.add(const ChatGrupReset());
        chatBloc.add(const ChatGrupStarted());
      },
      child: Scaffold(
        appBar: AppBar(
          title: BlocConsumer<ChatgrupBloc, ChatgrupState>(
            listener: (context, state) {
              if (state.selectedChat != null) {
                listenChatChannel(state.selectedChat!);
              }
            },
            listenWhen: (previous, current) =>
                previous.selectedChat != current.selectedChat,
            builder: (context, state) {
              namaGrup = state.selectedChat!.name.toString();
              return GestureDetector(
                  onTap: () {
                    eLog('a');
                  },
                  child: Text(namaGrup));
            },
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.info),
              onPressed: () async {
                final chatid = chatBloc.state.selectedChat!.id;
                final token = AuthBloc().state.token.toString();
                Dio dio = Dio();
                final urlId =
                    'https://api.bakamla.barengsaya.com/api/chat-group-detail/$chatid';
                try {
                  final response = await dio.get(
                    urlId,
                    options: Options(
                      headers: {'Authorization': 'Bearer $token'},
                    ),
                  );
                  if (response.statusCode == 200) {
                    // ignore: use_build_context_synchronously
                    eLog(response.data['participants'][0]);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChatGroupDetailScreen(
                          chatId: chatid.toString(),
                          data: response.data['data'],
                          participants: response.data['participants'],
                        ),
                      ),
                    );
                  } else {
                    eLog('code bukan 200');
                  }
                } catch (e) {
                  eLog(e);
                }
              },
            ),
          ],
        ),
        body: BlocBuilder<ChatgrupBloc, ChatgrupState>(
          builder: (context, state) {
            eLog(LaravelEcho.socketId);

            // vLog(state.chatMessages);
            return DashChat(
              inputOptions: InputOptions(
                onTextChange: (text) {
                  setState(() {
                    isSuffixHidden = text.isEmpty;
                  });
                },
                textController: _textController,
                sendButtonBuilder: (send) {
                  return isSuffixHidden
                      ? GestureDetector(
                          child: ElevatedButton(
                            onPressed: () async {},
                            style: ElevatedButton.styleFrom(
                              shape: const CircleBorder(),
                              padding: const EdgeInsets.all(15),
                              backgroundColor: Colors.red, // <-- Button color
                              foregroundColor: Colors.white, // <-- Splash color
                            ),
                            child: const Icon(Icons.mic, color: Colors.white),
                          ),
                          onLongPress: () async {
                            playAudio("start_record.wav");
                            startRecord();
                          },
                          onLongPressEnd: (_) async {
                            playAudio("stop_record.mp3");
                            stopRecord();
                          },
                        )
                      : ElevatedButton(
                          onPressed: () {
                            final currentUser = authBloc.state.user!.toChatUser;
                            final createdAt = DateTime.now();

                            final encrypted =
                                encrypter.encrypt(_textController.text, iv: iv);
                            final saveEncrypted = encrypted.base64;

                            final chatMessage = ChatMessage(
                              text: saveEncrypted,
                              user: currentUser,
                              createdAt: createdAt,
                            );
                            try {
                              chatBloc.add(SendGrupMessage(
                                state.selectedChat!.id,
                                chatMessage,
                                iv.base64,
                                socketId: LaravelEcho.socketId,
                              ));
                              _textController.clear();
                              isSuffixHidden = true;
                            } catch (e) {
                              eLog("erorsend: $e");
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            shape: const CircleBorder(),
                            padding: const EdgeInsets.all(15),
                            backgroundColor: Colors.red, // <-- Button color
                            foregroundColor: Colors.white, // <-- Splash color
                          ),
                          child: const Icon(Icons.send, color: Colors.white),
                        );
                },
                inputDecoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey[200],
                  hintText: "Tulis pesan....",
                  hintStyle: const TextStyle(
                    color: Colors.grey,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50.0),
                    borderSide: const BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  suffixIcon: Padding(
                      padding: const EdgeInsetsDirectional.only(start: 12.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const ListMenuItem(
                            chatInfo: 'grup',
                          ),
                          isSuffixHidden
                              ? IconButton(
                                  onPressed: () => showCamera(context),
                                  icon: const Icon(Icons.camera_alt),
                                )
                              : const SizedBox(
                                  height: 0.0,
                                ),
                        ],
                      )),
                ),
                alwaysShowSend: true,
              ),
              currentUser: authBloc.state.user!.toChatUser,
              onSend: (ChatMessage chatMessage) {
                // chatBloc.add(SendGrupMessage(
                //   state.selectedChat!.id,
                //   chatMessage,
                //   sockedId: LaravelEcho.socketId,
                // ));
              },
              messages: state.uiChatMessages,
              messageOptions: MessageOptions(
                onLongPressMessage: (message) {
                  if (message.user.id.toString() ==
                      AuthBloc().state.user!.id.toString()) {
                    //hapus pesan disini
                    return _showDeleteConfirmationDialog(
                        context, message.idMessage.toString());
                  }
                },
                messageMediaBuilder: (message, previousMessage, nextMessage) {
                  if (message.medias != null && message.medias!.isNotEmpty) {
                    if (message.location != '0') {
                      double lati = double.parse("${message.latitude}");
                      double longi = double.parse("${message.longitude}");
                      return Card(
                        color: Colors.red,
                        child: Container(
                          width: 170.0,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Row(
                              children: [
                                const Row(
                                  children: [
                                    Icon(
                                      Icons.location_pin,
                                      color: Colors.white,
                                    ),
                                    Text(
                                      " Lokasi",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ],
                                ),
                                const Spacer(),
                                const Spacer(),
                                IconButton(
                                    onPressed: () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                LocationScreen(
                                                  lat: lati,
                                                  long: longi,
                                                )),
                                      );
                                    },
                                    icon: const Icon(
                                      Icons.remove_red_eye,
                                      color: Colors.white,
                                    ))
                              ],
                            ),
                          ),
                        ),
                      );
                    }
                    if (message.image != null) {
                      return ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            padding: const EdgeInsets.all(0)),
                        onPressed: () {
                          eLog("${message.image}");
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Image(
                              image: NetworkImage(
                                  "${Endpoints.image}${message.image}")),
                        ),
                      );
                    }
                    if (message.video != null) {
                      return Card(
                        color: Colors.red,
                        child: Container(
                          width: 170.0,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Row(
                              children: [
                                const Row(
                                  children: [
                                    Icon(
                                      Icons.video_chat,
                                      color: Colors.white,
                                    ),
                                    Text(
                                      " Video",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ],
                                ),
                                const Spacer(),
                                const Spacer(),
                                IconButton(
                                    onPressed: () {
                                      eLog("${message.video}");
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => PlayScreen(
                                                  video: message.video,
                                                )),
                                      );
                                    },
                                    icon: const Icon(
                                      Icons.play_arrow,
                                      color: Colors.white,
                                    ))
                              ],
                            ),
                          ),
                        ),
                      );
                    }
                    if (message.contactName != null) {
                      return ElevatedButton(
                        onPressed: () {
                          openDetailContact(
                              message.contactName, message.contactNumber);
                        },
                        child: Container(
                          width: 170.0,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(children: [
                              Row(
                                children: [
                                  const CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Icon(
                                      Icons.person,
                                      color: Colors.red,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 5.0,
                                  ),
                                  Expanded(
                                    child: Text(
                                      "${message.contactName}",
                                      style:
                                          const TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ],
                              ),
                              const Divider(),
                              const Center(
                                child: Text("Lihat Kontak"),
                              )
                            ]),
                          ),
                        ),
                      );
                    }
                    if (message.audio != null) {
                      return Card(
                        color: Colors.red,
                        child: Container(
                          width: 170.0,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Row(
                              children: [
                                const Row(
                                  children: [
                                    Icon(
                                      Icons.voice_chat,
                                      color: Colors.white,
                                    ),
                                    Text(
                                      " Voice",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ],
                                ),
                                const Spacer(),
                                const Spacer(),
                                IconButton(
                                    onPressed: () {
                                      playRecordedAudio("${message.audio}");
                                    },
                                    icon: const Icon(
                                      Icons.play_arrow,
                                      color: Colors.white,
                                    ))
                              ],
                            ),
                          ),
                        ),
                      );
                    }
                    if (message.document != null) {
                      return Card(
                        color: Colors.red,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            children: [
                              const Icon(
                                Icons.file_copy,
                                color: Colors.white,
                              ),
                              Expanded(
                                child: Text(
                                  "${message.document}",
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ),
                              IconButton(
                                  onPressed: () {
                                    eLog("${message.document}");
                                    ToastContext().init(context);
                                    downloadFile("${message.document}");
                                  },
                                  icon: const Icon(
                                    Icons.download,
                                    color: Colors.white,
                                  ))
                            ],
                          ),
                        ),
                      );
                    } else {
                      return const Text(
                        "What it is",
                        style: TextStyle(color: Colors.white),
                      );
                    }
                  } else {
                    return const Text(" ");
                  }
                },
              ),
              messageListOptions: MessageListOptions(
                onLoadEarlier: () async {
                  chatBloc.add(const LoadMoreGrupChatMessage());
                },
              ),
            );
          },
        ),
      ),
    );
  }

  void openDetailContact(name, number) async {
    await Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => ContactDetailScreen(
              fullName: name,
              number: number,
              send: 'no',
              chatInfo: 'grup',
            )));
  }

  // Download File
  void downloadFile(String filename) async {
    Dio dio = Dio();
    String url = "${Endpoints.document}$filename";
    try {
      // Check if the file exists on the server
      Response response = await dio.head(url);
      if (response.statusCode == 200) {
        // Get the downloads directory
        Directory? downloadsDirectory =
            await DownloadsPath.downloadsDirectory();

        // Build the full path to save the file
        String savePath = '${downloadsDirectory!.path}/$filename';

        // Download the file
        await dio.download(url, savePath);
        // ignore: use_build_context_synchronously
        eLog('File downloaded successfully');
        Toast.show("Berhasil Mendownload File",
            duration: Toast.lengthLong, gravity: Toast.bottom);
      } else {
        Toast.show("Gagal File Tidak Ditemukan",
            duration: Toast.lengthLong, gravity: Toast.bottom);
        eLog('File not found on the server');
      }
    } catch (e) {
      Toast.show("Gagal Mendownload",
          duration: Toast.lengthLong, gravity: Toast.bottom);
      eLog('Error downloading file: $e');
    }
  }

  void _showDeleteConfirmationDialog(BuildContext context, String idMsg) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Hapus pesan'),
          content: const Text('Apakah anda yakin ingin menghapus pesan ini?'),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Delete'),
              onPressed: () async {
                final token = AuthBloc().state.token.toString();
                Dio dio = Dio();
                final urlId =
                    'https://api.bakamla.barengsaya.com/api/chat_message/$idMsg';
                eLog(urlId);
                try {
                  final response = await dio.get(
                    urlId,
                    options: Options(
                      headers: {'Authorization': 'Bearer $token'},
                    ),
                  );
                  if (response.statusCode == 200) {
                    // ignore: use_build_context_synchronously
                    final chatBloc = context.read<ChatgrupBloc>();
                    chatBloc.add(const GetGrupChatMessage());
                  } else {
                    eLog('code bukan 200');
                  }
                } catch (e) {
                  eLog(e);
                }
                // _deleteMessage(message);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  //
  Future<dynamic> showCamera(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextButton(
              onPressed: () {
                _pickImage(1);
              },
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Icon(Icons.camera_alt), Text("Camera")],
              ),
            ),
            TextButton(
              onPressed: () {
                _pickImage(2);
              },
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Icon(Icons.video_camera_back), Text("Video")],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _pickImage(val) async {
    Navigator.pop(context);
    final chatBloc = context.read<ChatgrupBloc>();
    final state = chatBloc.state.selectedChat!.id;
    if (val == 1) {
      final pickedFile = await _picker.pickImage(
        source: ImageSource.camera,
        preferredCameraDevice: CameraDevice.rear,
        imageQuality: 20,
      );
      if (pickedFile != null) {
        File imageFile = File(pickedFile.path);

        // ignore: use_build_context_synchronously
        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) => ImageScreen(
                    image: imageFile,
                    imImage: 'yes',
                    chatId: state,
                    chatInfo: 'grup',
                  )),
        );
      }
    }
    if (val == 2) {
      final XFile? pickedFile = await _picker.pickVideo(
        source: ImageSource.camera,
        maxDuration: const Duration(seconds: 60),
      );

      if (pickedFile != null) {
        File imageFile = File(pickedFile.path);
        // ignore: use_build_context_synchronously
        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) => ImageScreen(
                    image: imageFile,
                    imVideo: 'yes',
                    chatId: state,
                    chatInfo: 'grup',
                  )),
        );
      }
    }
  }
}
