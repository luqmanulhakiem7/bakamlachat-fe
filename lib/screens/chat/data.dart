import 'package:dash_chat_2/dash_chat_2.dart';

String profileImage =
    'https://firebasestorage.googleapis.com/v0/b/molteo-40978.appspot.com/o/1-intro-photo-final.jpeg?alt=media&token=daf78997-d8f0-49d1-9120-a9380bde48b5';

ChatUser user = ChatUser(id: '0');
ChatUser user1 = ChatUser(id: '1', firstName: ' Ganteng');
ChatUser user2 = ChatUser(id: '2', firstName: 'Luqman Ganteng');
ChatUser user3 = ChatUser(id: '3', lastName: 'DinoSaurus');
ChatUser user4 = ChatUser(id: '4', profileImage: profileImage);
ChatUser user5 = ChatUser(id: '5', firstName: 'Kambing', lastName: 'Hutan');
ChatUser user6 =
    ChatUser(id: '6', firstName: 'Kantor', profileImage: profileImage);
ChatUser user7 =
    ChatUser(id: '7', lastName: 'Jingga', profileImage: profileImage);
ChatUser user8 = ChatUser(
    id: '8',
    firstName: 'Dinda',
    lastName: 'Damayanti',
    profileImage: profileImage);

List<ChatMessage> allUsersSample = <ChatMessage>[
  ChatMessage(
    text: 'try',
    user: user1,
    image:
        "https://firebasestorage.googleapis.com/v0/b/molteo-40978.appspot.com/o/1-intro-photo-final.jpeg?alt=media&token=daf78997-d8f0-49d1-9120-a9380bde48b5",
    medias: [
      ChatMedia(
        url:
            "https://firebasestorage.googleapis.com/v0/b/molteo-40978.appspot.com/o/1-intro-photo-final.jpeg?alt=media&token=daf78997-d8f0-49d1-9120-a9380bde48b5",
        fileName: "1-intro-photo-final",
        type: MediaType.image,
      )
    ],
    createdAt: DateTime(2021, 01, 30, 16, 45),
  ),
  ChatMessage(
    text: 'Test user2',
    user: user2,
    createdAt: DateTime(2021, 01, 30, 16, 45),
  ),
  ChatMessage(
    text: 'Test',
    user: user3,
    createdAt: DateTime(2021, 01, 30, 16, 45),
  ),
  ChatMessage(
    text: 'Test',
    user: user4,
    createdAt: DateTime(2021, 01, 30, 16, 45),
  ),
  ChatMessage(
    text: 'Test',
    user: user5,
    createdAt: DateTime(2021, 01, 30, 16, 45),
  ),
  ChatMessage(
    text: 'Test',
    user: user6,
    createdAt: DateTime(2021, 01, 30, 16, 45),
  ),
  ChatMessage(
    text: 'Test',
    user: user7,
    createdAt: DateTime(2021, 01, 30, 16, 45),
  ),
  ChatMessage(
    text: 'Test',
    user: user8,
    createdAt: DateTime(2021, 01, 30, 16, 45),
  ),
];
