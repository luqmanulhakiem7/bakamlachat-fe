import 'dart:ffi';
import 'dart:io';

import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chat/chat_bloc.dart';
import 'package:bakamlachat/blocs/chatgrup/chatgrup_bloc.dart';
import 'package:bakamlachat/screens/chat/contact_screen.dart';
import 'package:bakamlachat/screens/chat/image_screen.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';

class ListMenuItem extends StatefulWidget {
  final String chatInfo;

  const ListMenuItem({
    Key? key,
    required this.chatInfo, // i want to get this value
  }) : super(key: key);

  @override
  State<ListMenuItem> createState() => _ListMenuItem();
}

class _ListMenuItem extends State<ListMenuItem> {
//**
//Function
// */
  String infochat = '';
  @override
  void initState() {
    super.initState();
    infochat = widget.chatInfo;
  }

  Future<dynamic> showCamera(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextButton(
              onPressed: () {
                // Navigator.pop(context);
                _pickImage(1);
              },
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Icon(Icons.camera_alt), Text("Camera")],
              ),
            ),
            TextButton(
              onPressed: () {
                _pickImage(2);
              },
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Icon(Icons.video_camera_back), Text("Video")],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<dynamic> showGallery(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextButton(
              onPressed: () {
                _pickImage(3);
              },
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Icon(Icons.image), Text("Foto")],
              ),
            ),
            TextButton(
              onPressed: () {
                _pickImage(4);
              },
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Icon(Icons.video_camera_back), Text("Video")],
              ),
            ),
          ],
        ),
      ),
    );
  }

//**
//Image Picker
// */
  final _picker = ImagePicker();
  int? state;

  _pickImage(val) async {
    Navigator.pop(context);
    final chatBloc = context.read<ChatBloc>();
    final chatgrupBloc = context.read<ChatgrupBloc>();
    if (widget.chatInfo == "personal") {
      state = chatBloc.state.selectedChat!.id;
    } else {
      state = chatgrupBloc.state.selectedChat!.id;
    }

    if (val == 1) {
      final pickedFile = await _picker.pickImage(
        source: ImageSource.camera,
        preferredCameraDevice: CameraDevice.rear,
        imageQuality: 20,
      );
      if (pickedFile != null) {
        File imageFile = File(pickedFile.path);

        // ignore: use_build_context_synchronously
        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) => ImageScreen(
                    image: imageFile,
                    imImage: 'yes',
                    chatId: state,
                    chatInfo: widget.chatInfo,
                  )),
        );
      }
    }
    if (val == 2) {
      final XFile? pickedFile = await _picker.pickVideo(
        source: ImageSource.camera,
        maxDuration: const Duration(seconds: 60),
      );

      if (pickedFile != null) {
        File imageFile = File(pickedFile.path);
        // ignore: use_build_context_synchronously
        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) => ImageScreen(
                    image: imageFile,
                    imVideo: 'yes',
                    chatId: state,
                    chatInfo: widget.chatInfo,
                  )),
        );
      }
    }
    if (val == 3) {
      final XFile? pickedFile = await _picker.pickImage(
          source: ImageSource.gallery, imageQuality: 20);
      if (pickedFile != null) {
        File imageFile = File(pickedFile.path);

        // ignore: use_build_context_synchronously
        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) => ImageScreen(
                    image: imageFile,
                    imImage: 'yes',
                    chatId: state,
                    chatInfo: widget.chatInfo,
                  )),
        );
      }
    }
    if (val == 4) {
      final XFile? pickedFile = await _picker.pickVideo(
        source: ImageSource.gallery,
      );

      if (pickedFile != null) {
        File imageFile = File(pickedFile.path);
        // ignore: use_build_context_synchronously
        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) => ImageScreen(
                    image: imageFile,
                    imVideo: 'yes',
                    chatId: state,
                    chatInfo: widget.chatInfo,
                  )),
        );
      }
    }
    if (val == 5) {
      final token = AuthBloc().state.token.toString();
      List<XFile> doc = await _picker.pickMultipleMedia();
      List<File> picked = doc.map((XFile xfile) {
        return File(xfile.path);
      }).toList();
      late List<String>? basename;
      basename = picked.map((e) => e.path.split('/').last).toList();

      for (var i = 0; i <= basename.length; i++) {
        var formData = FormData.fromMap({
          'chat_id': state,
          'documents': await MultipartFile.fromFile(picked[i].path,
              filename: basename[i]),
          'message': ' ',
        });
        if (widget.chatInfo == 'personal') {
          chatBloc.add(sendMedia(formData, token));
        } else {
          chatgrupBloc.add(sendGrupMedia(formData, token));
        }
      }
    }
  }

//**
// Location
// */

  void getPosition() async {
    // Panggil fungsi untuk mendapatkan lokasi
    Position? position = await _getCurrentLocation();
    // Gunakan position.latitude dan position.longitude untuk mendapatkan koordinat
    eLog('Latitude: ${position!.latitude}, Longitude: ${position.longitude}');
    postLocation(position.longitude, position.latitude, 1);
  }

  Future<Position?> _getCurrentLocation() async {
    try {
      // Periksa izin lokasi
      LocationPermission permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        // Jika izin belum diberikan, minta izin
        permission = await Geolocator.requestPermission();
        if (permission != LocationPermission.whileInUse &&
            permission != LocationPermission.always) {
          // Izin tidak diberikan, keluar dari fungsi
          return null;
        }
      }

      // Dapatkan lokasi saat ini
      Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
      );
      return position;
    } catch (e) {
      eLog('Error getting location: $e');
      return null;
    }
  }

// Post Location
  Future<void> postLocation(long, lat, duration) async {
    final token = AuthBloc().state.token.toString();
    // ignore: use_build_context_synchronously
    final chatBloc = context.read<ChatBloc>();
    final chatgrupBloc = context.read<ChatgrupBloc>();

    if (widget.chatInfo == "personal") {
      state = chatBloc.state.selectedChat!.id;
    } else {
      state = chatgrupBloc.state.selectedChat!.id;
    }

    var formData = FormData.fromMap({
      'chat_id': state,
      'locations': 1,
      'longitude': long,
      'latitude': lat,
      'delete_time': duration,
      'message': ' ',
    });
    if (widget.chatInfo == 'personal') {
      chatBloc.add(sendMedia(formData, token));
    } else {
      chatgrupBloc.add(sendGrupMedia(formData, token));
    }
  }

  @override
  Widget build(BuildContext context) {
    final String chatInfoa = widget.chatInfo;

    return PopupMenuButton(
      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
      iconSize: 30,
      icon: const Icon(Icons.attach_file),
      itemBuilder: (context) {
        return [
          PopupMenuItem(
            onTap: () {
              //
            },
            enabled: false, // DISABLED THIS ITEM
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                GridView.count(
                  padding: const EdgeInsets.all(20.0),
                  shrinkWrap: true,
                  crossAxisCount: 3,
                  children: [
                    Column(
                      children: [
                        SizedBox(
                          height: 60.0,
                          width: 60.0,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              shape: const CircleBorder(),
                              backgroundColor:
                                  const Color.fromARGB(255, 150, 6, 207),
                            ),
                            onPressed: () {
                              // Navigator.pop(context);
                              _pickImage(5);
                            },
                            child: const Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.attach_file,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                          ),
                        ),
                        const Text(
                          "Dokumen",
                          style: TextStyle(
                            color: Color.fromRGBO(
                              58,
                              58,
                              58,
                              100,
                            ),
                            // fontSize: 25,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 60.0,
                          width: 60.0,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              shape: const CircleBorder(),
                              backgroundColor: Colors.pink,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                              showCamera(context);
                            },
                            child: const Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.camera_alt,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                          ),
                        ),
                        const Text(
                          "Kamera",
                          style: TextStyle(
                            color: Color.fromRGBO(
                              58,
                              58,
                              58,
                              100,
                            ),
                            // fontSize: 25,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 60.0,
                          width: 60.0,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              shape: const CircleBorder(),
                              backgroundColor: Colors.orange,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                              showGallery(context);
                            },
                            child: const Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.image),
                              ],
                            ),
                          ),
                        ),
                        const Text(
                          "Galeri",
                          style: TextStyle(color: Colors.grey),
                          // fontSize: 25,
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 60.0,
                          width: 60.0,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              shape: const CircleBorder(),
                              backgroundColor: Colors.green,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                              getPosition();
                            },
                            child: const Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.location_on),
                              ],
                            ),
                          ),
                        ),
                        const Text(
                          "Lokasi",
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 60.0,
                          width: 60.0,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              shape: const CircleBorder(),
                              backgroundColor: Colors.pinkAccent,
                            ),
                            onPressed: () {
                              if (widget.chatInfo == 'personal') {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => const ContactScreen(
                                      chatInfo:
                                          "personal", //invalid constant value
                                    ),
                                  ),
                                );
                              } else {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => const ContactScreen(
                                      chatInfo: "grup", //invalid constant value
                                    ),
                                  ),
                                );
                              }
                            },
                            child: const Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.person_2),
                              ],
                            ),
                          ),
                        ),
                        const Text(
                          "Kontak",
                          style: TextStyle(
                            color: Color.fromRGBO(
                              58,
                              58,
                              58,
                              100,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        ];
      },
    );
  }
}
