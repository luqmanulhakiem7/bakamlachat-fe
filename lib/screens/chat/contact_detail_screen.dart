import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chat/chat_bloc.dart';
import 'package:bakamlachat/blocs/chatgrup/chatgrup_bloc.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:toast/toast.dart';

class ContactDetailScreen extends StatefulWidget {
  final String fullName;
  final String number;
  final String? send;
  final String chatInfo;
  const ContactDetailScreen({
    Key? key,
    required this.fullName,
    required this.number,
    this.send,
    required this.chatInfo,
  }) : super(key: key);

  @override
  State<ContactDetailScreen> createState() => _ContactDetailScreen();
}

class _ContactDetailScreen extends State<ContactDetailScreen> {
  void _copyText(String text) {
    Clipboard.setData(ClipboardData(text: text));
    ToastContext().init(context);
    Toast.show("Berhasil menyalin telpon",
        duration: Toast.lengthLong, gravity: Toast.bottom);
  }

  void uploadContact(name, number) async {
    const String url = "${Endpoints.baseUrl}${Endpoints.createChatMessages}";
    final authT = AuthBloc().state;
    if (widget.chatInfo == 'personal') {
      final cBloc = context.read<ChatBloc>();
      final state = cBloc.state.selectedChat!.id;
      try {
        var formData = FormData.fromMap({
          'chat_id': state,
          'contact_name': name,
          'contact_number': number,
          'message': ' ',
        });
        final resp = await Dio().post(url,
            data: formData,
            options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer ${authT.token}"
              },
            ));
        if (resp.statusCode == 200) {
          EasyLoading.dismiss();
          cBloc.add(const GetChatMessage());
          // ignore: use_build_context_synchronously
          Navigator.of(context).pop();
          eLog('success');
        } else {
          eLog('fail');
          // return false;
        }
      } catch (e) {
        eLog('error: $e');
      }
    } else {
      final cBloc = context.read<ChatgrupBloc>();
      final state = cBloc.state.selectedChat!.id;
      try {
        var formData = FormData.fromMap({
          'chat_id': state,
          'contact_name': name,
          'contact_number': number,
          'message': ' ',
        });
        final resp = await Dio().post(url,
            data: formData,
            options: Options(
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer ${authT.token}"
              },
            ));
        if (resp.statusCode == 200) {
          EasyLoading.dismiss();
          cBloc.add(const GetGrupChatMessage());
          // ignore: use_build_context_synchronously
          Navigator.of(context).pop();
          eLog('success');
        } else {
          eLog('fail');
          // return false;
        }
      } catch (e) {
        eLog('error: $e');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: titleScreen()),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Row(
                children: [
                  const SizedBox(
                    width: 60.0,
                    height: 60.0,
                    child: CircleAvatar(
                      child: Icon(
                        Icons.person,
                        size: 40.0,
                      ),
                    ),
                  ),
                  const SizedBox(width: 10.0),
                  Expanded(
                      child: Text(
                    widget.fullName,
                    style: const TextStyle(fontSize: 20.0),
                  ))
                ],
              ),
              const Divider(),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: TextButton(
                  onPressed: () {
                    _copyText(widget.number);
                  },
                  child: Row(
                    children: [
                      const Icon(
                        Icons.phone,
                        color: Colors.red,
                      ),
                      const SizedBox(width: 20.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.number,
                            style: const TextStyle(
                                fontSize: 18.0, color: Colors.black),
                          ),
                          const Text(
                            "Ponsel",
                            style:
                                TextStyle(fontSize: 15.0, color: Colors.black),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (widget.send == 'yes') {
            uploadContact(widget.fullName, widget.number);
          } else {
            //
          }
        },
        child: widget.send == 'yes'
            ? const Icon(Icons.send)
            : const Icon(Icons.person_add_alt_1),
      ),
    );
  }

  Text titleScreen() {
    if (widget.send == 'yes') {
      return const Text("Kontak Yang Akan Dikirim");
    } else {
      return const Text("Detail Kontak");
    }
  }
}
