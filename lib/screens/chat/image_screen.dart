import 'dart:io';

import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chat/chat_bloc.dart';
import 'package:bakamlachat/blocs/chatgrup/chatgrup_bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_compress/video_compress.dart';
import 'package:video_player/video_player.dart';
import 'package:encrypt/encrypt.dart' as enkripsi;

class ImageScreen extends StatefulWidget {
  final File? image;
  final String? imVideo;
  final String? imImage;
  final int? chatId;
  final String chatInfo;
  const ImageScreen({
    Key? key,
    this.image,
    this.imImage,
    this.imVideo,
    required this.chatId,
    required this.chatInfo,
  }) : super(key: key);

  @override
  State<ImageScreen> createState() => _ImageScreenState();
}

class _ImageScreenState extends State<ImageScreen> {
  final TextEditingController myInput = TextEditingController();
  bool get isImage => widget.imImage != null;
  bool get isVideo => widget.imVideo != null;

  @override
  void dispose() {
    myInput.dispose();
    super.dispose();
  }

  late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.networkUrl(Uri.parse(
      widget.image!.path,
    ))
      ..initialize().then((_) {
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    return isImage ? imageWidget() : videoWidget();
  }

  Stack videoWidget() {
    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(),
          body: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Center(
                    child: _controller.value.isInitialized
                        ? AspectRatio(
                            aspectRatio: _controller.value.aspectRatio,
                            child: VideoPlayer(_controller),
                          )
                        : Container(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: TextField(
                  controller: myInput,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(10),
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(40.0),
                      ),
                      borderSide: BorderSide(color: Colors.black, width: 1.0),
                    ),
                    hintText: "Masukkan Pesan...",
                    suffixIcon: IconButton(
                      onPressed: () {
                        var status = 'video';
                        uploading(myInput.value.text, status);
                      },
                      icon: const Icon(Icons.send),
                    ),
                  ),
                ),
              ),
            ],
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.miniEndTop,
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.white,
            onPressed: () {
              setState(() {
                _controller.value.isPlaying
                    ? _controller.pause()
                    : _controller.play();
              });
            },
            child: Icon(
              _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
              color: Colors.red,
            ),
          ),
        ),
      ],
    );
  }

  Stack imageWidget() {
    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(),
          body: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Center(
                    child: Image.file(
                      widget.image!,
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: TextField(
                  controller: myInput,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(10),
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(40.0),
                      ),
                      borderSide: BorderSide(color: Colors.black, width: 1.0),
                    ),
                    hintText: "Masukkan Pesan...",
                    suffixIcon: IconButton(
                      onPressed: () {
                        var status = 'image';
                        uploading(myInput.value.text, status);
                      },
                      icon: const Icon(Icons.send),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  uploading(value, status) async {
    String basename = widget.image!.path.split('/').last;

    final chatBloc = context.read<ChatBloc>();
    final chatgrupBloc = context.read<ChatgrupBloc>();
    final key = enkripsi.Key.fromUtf8('my 32 length key.is.bakamla.....');
    final iv = enkripsi.IV.fromLength(16);
    final encrypter = enkripsi.Encrypter(enkripsi.AES(key));
    var keybase = null;

    final token = AuthBloc().state.token.toString();

    if (value != null) {
      if (value.length > 0) {
        final encrypted = encrypter.encrypt(value, iv: iv);
        value = encrypted.base64;
        keybase = iv.base64;
      }
    } else {
      value = basename;
    }
    if (status == 'image') {
      var formData = FormData.fromMap({
        'chat_id': widget.chatId,
        'images': await MultipartFile.fromFile(widget.image!.path,
            filename: basename),
        'message': value,
        'ivkey': keybase
      });
      if (widget.chatInfo == 'personal') {
        chatBloc.add(sendMedia(formData, token));
      } else {
        chatgrupBloc.add(sendGrupMedia(formData, token));
      }
      // ignore: use_build_context_synchronously
      Navigator.of(context).pop();
    }
    if (status == 'video') {
      final info = await VideoCompress.compressVideo(
        widget.image!.path,
        quality: VideoQuality.LowQuality,
        deleteOrigin: false,
        includeAudio: true,
      );
      var formData = FormData.fromMap({
        'chat_id': widget.chatId,
        'videos':
            await MultipartFile.fromFile("${info!.path}", filename: basename),
        'message': value,
        'ivkey': keybase
      });
      if (widget.chatInfo == 'personal') {
        chatBloc.add(sendMedia(formData, token));
      } else {
        chatgrupBloc.add(sendGrupMedia(formData, token));
      }
      // ignore: use_build_context_synchronously
      Navigator.of(context).pop();
    }
  }
}
