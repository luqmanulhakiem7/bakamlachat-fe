import 'package:bakamlachat/screens/call_log_screen/call_log_screen.dart';
import 'package:bakamlachat/screens/chat_group_list/chat_group_list_screen.dart';
import 'package:bakamlachat/screens/chat_list/chat_list_screen.dart';
import 'package:bakamlachat/screens/setelan/setelan_screen.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  static const routeName = "main";

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          // backgroundColor: Colors.red,
          actions: [
            PopupMenuButton<String>(
              onSelected: (result) {
                if (result == '2') {
                  Navigator.of(context).pushNamed(SetelanScreen.routeName);
                }
              },
              itemBuilder: (BuildContext context) => [
                const PopupMenuItem(
                  value: '2',
                  child: Text('Setelan'),
                ),
              ],
            ),
          ],
          bottom: const TabBar(
            tabs: [
              Tab(icon: Icon(Icons.message)),
              Tab(icon: Icon(Icons.group)),
              Tab(icon: Icon(Icons.call)),
            ],
          ),
          title: const Text("Bakamla Chat"),
        ),
        body: const TabBarView(
          children: [
            ChatListScreen(),
            ChatGroupListScreen(),
            CallLogScreen(),
          ],
        ),
      ),
    );
  }
}
