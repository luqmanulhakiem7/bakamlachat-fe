import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:bakamlachat/screens/call_contact_screen/call_contact_screen.dart';
import 'package:bakamlachat/screens/call_page/add_call_screen.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:zego_uikit_prebuilt_call/zego_uikit_prebuilt_call.dart';

class CallLogScreen extends StatefulWidget {
  const CallLogScreen({super.key});

  static const routeName = "CallLog";

  @override
  State<StatefulWidget> createState() => _CallLog();
}

class _CallLog extends State {
  List<dynamic> jsonData = [];
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    loadJsonAsset();
  }

  Future<void> loadJsonAsset() async {
    Directory? appDocumentsDirectory = await getExternalStorageDirectory();

    // Buat path ke file JSON di dalam direktori dokumen aplikasi
    String filePath = '${appDocumentsDirectory!.path}/call_history.json';
    String jsonString = await File(filePath).readAsString();
    var data = await json.decode(jsonString);
    List<dynamic> reversedData = data["data"].reversed.toList();
    setState(() {
      isLoading = false;
      jsonData = reversedData;
      eLog(jsonData.length);
    });
  }

  Future<void> refreshData() async {
    loadJsonAsset();
    setState(() {});
  }

  String formattedDate = DateFormat('MM-dd-yyyy').format(DateTime.now());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {
          refreshData();
        },
        child: isLoading
            ? CircularProgressIndicator()
            : jsonData.isNotEmpty
                ? Container(
                    child: Column(children: [
                      const SizedBox(height: 5),
                      Expanded(
                        child: ListView.builder(
                            itemCount: jsonData.length + 2,
                            itemBuilder: (context, i) {
                              if (i == 0) {
                                // return the header
                                return const Padding(
                                    padding: EdgeInsets.all(15),
                                    child: Text("Terbaru"));
                              }
                              i -= 1;
                              if (i == jsonData.length) {
                                // return the header
                                return const Column(children: [
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.security,
                                        size: 20,
                                        color: Colors.grey,
                                      ),
                                      Text(
                                        "Panggilan pribadi Anda",
                                        style: TextStyle(
                                            fontSize: 10, color: Colors.grey),
                                      ),
                                      SizedBox(width: 2),
                                      Text(
                                        "terenkripsi secara end-to-end",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 10),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ]);
                              }
                              final tanggal =
                                  jsonData[i]["date"] == formattedDate
                                      ? const Text("Hari ini")
                                      : Text(jsonData[i]["date"]);
                              return Card(
                                key: ValueKey(jsonData[i]["id"]),
                                child: InkWell(
                                  onTap: () {
                                    eLog("Detail");
                                  },
                                  onLongPress: () async {
                                    showDialog<String>(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          AlertDialog(
                                        title: const Text('Alert'),
                                        content: const Text(
                                            'Apakah anda ingin menghapus log panggilan ini?'),
                                        actions: <Widget>[
                                          TextButton(
                                            onPressed: () => Navigator.pop(
                                                context, 'Cancel'),
                                            child: const Text('Cancel'),
                                          ),
                                          TextButton(
                                            onPressed: () async {
                                              Directory? appDocumentsDirectory =
                                                  await getExternalStorageDirectory();
                                              List<dynamic> jsonData2 = [];

                                              // Buat path ke file JSON di dalam direktori dokumen aplikasi
                                              String filePath =
                                                  '${appDocumentsDirectory!.path}/call_history.json';
                                              String jsonString2 =
                                                  await File(filePath)
                                                      .readAsString();
                                              var data2 = await json
                                                  .decode(jsonString2);
                                              jsonData2 = data2["data"];
                                              jsonData2.removeWhere((element) =>
                                                  element["id"] ==
                                                  jsonData[i]["id"]);

                                              // // Konversi objek Dart ke JSON
                                              Map<String, dynamic> dataBaru = {
                                                "data": jsonData2
                                              };

                                              String jsonD =
                                                  jsonEncode(dataBaru);
                                              await File(filePath)
                                                  .writeAsString(jsonD);

                                              refreshData();
                                              Navigator.pop(context, 'OK');
                                            },
                                            child: const Text('OK'),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                  child: ListTile(
                                    leading: const CircleAvatar(
                                      backgroundColor:
                                          Color.fromARGB(255, 228, 228, 228),
                                      child: Icon(Icons.person),
                                    ),
                                    title: Text(jsonData[i]["username"]),
                                    subtitle: Row(
                                      children: [
                                        jsonData[i]["desc"] == "incoming"
                                            ? Transform.flip(
                                                flipY: true,
                                                child: Icon(
                                                    Icons.arrow_outward_sharp,
                                                    color: jsonData[i]
                                                                ['status'] ==
                                                            "ditolak"
                                                        ? Colors.red
                                                        : jsonData[i][
                                                                    'status'] ==
                                                                "diterima"
                                                            ? Colors.green
                                                            : Colors.grey))
                                            : Transform.flip(
                                                child: Icon(
                                                    Icons.arrow_outward_sharp,
                                                    color: jsonData[i]
                                                                ['status'] ==
                                                            "ditolak"
                                                        ? Colors.red
                                                        : jsonData[i][
                                                                    'status'] ==
                                                                "diterima"
                                                            ? Colors.green
                                                            : Colors.grey)),
                                        tanggal,
                                        const SizedBox(width: 5),
                                        Text(jsonData[i]["time"]),
                                      ],
                                    ),
                                    trailing: jsonData[i]["type"] == "video"
                                        ? ZegoSendCallInvitationButton(
                                            icon: ButtonIcon(
                                                icon: const Icon(
                                              Icons.video_call,
                                              color: Colors.green,
                                            )),
                                            invitees: [
                                              ZegoUIKitUser(
                                                  id: jsonData[i]["userId"],
                                                  name: jsonData[i]["username"])
                                            ],
                                            buttonSize: const Size(50, 50),
                                            iconSize: const Size(50, 50),
                                            iconTextSpacing: double.nan,
                                            margin: const EdgeInsets.all(0),
                                            padding: const EdgeInsets.all(0),
                                            callID: "${Random().nextInt(100)}",
                                            resourceID: "zego_call",
                                            isVideoCall: true)
                                        : ZegoSendCallInvitationButton(
                                            icon: ButtonIcon(
                                                icon: const Icon(
                                              Icons.call,
                                              color: Colors.green,
                                            )),
                                            invitees: [
                                              ZegoUIKitUser(
                                                  id: jsonData[i]["userId"],
                                                  name: jsonData[i]["username"])
                                            ],
                                            buttonSize: const Size(50, 50),
                                            iconSize: const Size(50, 50),
                                            iconTextSpacing: double.nan,
                                            margin: const EdgeInsets.all(0),
                                            padding: const EdgeInsets.all(0),
                                            callID: "${Random().nextInt(100)}",
                                            resourceID: "zego_call",
                                            isVideoCall: false),
                                  ),
                                ),
                              );
                            }),
                      ),
                    ]),
                  )
                : Center(
                    child: Container(
                      width: 400,
                      height: 170,
                      child: const Column(
                        children: [
                          Icon(
                            Icons.history_edu_rounded,
                            size: 80.0,
                            color: Colors.grey,
                          ),
                          Text(
                            "Belum ada panggilan",
                            style: TextStyle(color: Colors.grey),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Untuk memanggil kontak, ketuk",
                                style: TextStyle(color: Colors.grey),
                              ),
                              SizedBox(width: 2),
                              Icon(
                                Icons.add_ic_call_rounded,
                                color: Colors.grey,
                              ),
                              SizedBox(width: 2),
                            ],
                          ),
                          Text(
                            "di bagian bawah layar.",
                            style: TextStyle(color: Colors.grey),
                          ),
                        ],
                      ),
                    ),
                  ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => const AddCallScreen()),
          );
        },
        child: const Icon(Icons.add_ic_call_rounded),
      ),
    );
  }
}
