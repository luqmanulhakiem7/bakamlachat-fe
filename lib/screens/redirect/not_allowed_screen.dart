import 'package:flutter/material.dart';

class NotAllowedScreen extends StatefulWidget {
  static const routeName = "notallowed";

  const NotAllowedScreen({super.key});

  @override
  State<NotAllowedScreen> createState() => _NotAllowedScreen();
}

class _NotAllowedScreen extends State<NotAllowedScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 400,
        height: 170,
        child: Column(
          children: [
            const Text("Oops Sepertinya anda terdeteksi menggunakan VPN"),
            const Text("Silahkan Matikan VPN Anda Lalu Coba Lagi"),
            ElevatedButton(
                onPressed: () {
                  //
                },
                child: const Text("Coba Lagi"))
          ],
        ),
      ),
    );
  }
}
