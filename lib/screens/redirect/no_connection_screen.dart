import 'package:bakamlachat/screens/splash/splash_screen.dart';
import 'package:flutter/material.dart';

class NoConnectionScreen extends StatefulWidget {
  static const routeName = "noconnection";

  const NoConnectionScreen({super.key});

  @override
  State<NoConnectionScreen> createState() => _NoConnectionScreen();
}

class _NoConnectionScreen extends State<NoConnectionScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 400,
        height: 170,
        child: Column(
          children: [
            const Icon(
              Icons.signal_cellular_connected_no_internet_0_bar,
              size: 80.0,
            ),
            const Text("Oops, Tidak ada internet"),
            ElevatedButton(
                onPressed: () async {
                  await Navigator.of(context)
                      .pushReplacementNamed(SplashScreen.routeName);
                },
                child: const Text("Coba Lagi"))
          ],
        ),
      ),
    );
  }
}
