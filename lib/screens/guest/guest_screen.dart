import 'dart:convert';
import 'dart:io';

import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/cubits/cubits.dart';
import 'package:bakamlachat/screens/home/home_screen.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:bakamlachat/utils/zego/zego_init.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:path_provider/path_provider.dart';

const users = {
  'luqman@gmail.com': '12345678',
  'rofi@gmail.com': '12345678',
};

class GuestScreen extends StatelessWidget {
  const GuestScreen({super.key});

  static const routeName = "guest";

  // @override
  Duration get loginTime => const Duration(milliseconds: 2250);

  Future<void> CreateFile() async {
    Map<String, dynamic> data = {"data": []};

    // Tulis konten JSON ke dalam file di direktori lokal aplikasi
    try {
      // Dapatkan direktori dokumen aplikasi
      Directory? appDocumentsDirectory = await getExternalStorageDirectory();

      // Buat path ke file JSON di dalam direktori dokumen aplikasi
      String filePath = '${appDocumentsDirectory!.path}/call_history.json';

      // Konversi objek Dart ke JSON
      String jsonData2 = jsonEncode(data);

      // Tulis konten JSON ke dalam file
      await File(filePath).writeAsString(jsonData2);
      eLog(jsonData2);
    } catch (e) {
      eLog('Error writing JSON file: $e');
    }
  }

  Future<String?> _authUser(LoginData data) {
    debugPrint('Name: ${data.name}, Password: ${data.password}');
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(data.name)) {
        return 'User not exists';
      }
      if (users[data.name] != data.password) {
        return 'Password does not match';
      }
      return null;
    });
  }

  Future<String?> _signupUser(SignupData data) {
    debugPrint('Signup Name: ${data.name}, Password: ${data.password}');
    return Future.delayed(loginTime).then((_) {
      return null;
    });
  }

  Future<String?> _recoverPassword(String name) async {
    eLog('Name: $name');
    Dio dio = Dio();
    const urlId = 'https://api.bakamla.barengsaya.com/api/reset';
    eLog(urlId);
    try {
      final response = await dio.post(urlId,
          data: {'email': name},
          options: Options(headers: {
            Headers.acceptHeader: 'application/json',
            Headers.contentTypeHeader: 'application/json'
          }));
      if (response.statusCode == 200) {
        // ignore: use_build_context_synchronously
        eLog('berhasil');
      } else {
        eLog('code bukan 200');
      }
    } catch (e) {
      eLog(e);
    }
    return Future.delayed(loginTime).then((_) {
      return null;
    });
  }

  @override
  Widget build(BuildContext context) {
    final cubit = context.read<GuestCubit>();

    return FlutterLogin(
      scrollable: true,
      title: 'Bakamla Chat',
      theme: LoginTheme(
        titleStyle: const TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
        pageColorDark: Colors.red,
        pageColorLight: Colors.red.shade300,
      ),
      // logo: const AssetImage('assets/images/logo.PNG'),
      onLogin: cubit.signIn,
      onSignup: cubit.signUp,
      userValidator: (value) {
        if (value == null || !value.contains('@')) {
          return 'Please enter a valid email address';
        }
        return null;
      },
      passwordValidator: (value) {
        if (value == null || value.length < 5) {
          return 'Please must be at least 5 chars';
        }
        return null;
      },
      onSubmitAnimationCompleted: () {
        final authState = context.read<AuthBloc>().state;

        ZegoInit().init("${authState.user!.id}", authState.user!.username);

        // ZegoUIKitPrebuiltCallInvitationService().init(
        //   appID: ZegoKonfig.appId /*input your AppID*/,
        //   appSign: ZegoKonfig.appSign /*input your AppSign*/,
        //   userID: "${authState.user!.id}",
        //   userName: authState.user!.username,
        //   plugins: [ZegoUIKitSignalingPlugin()],
        // );
        CreateFile();
        Navigator.of(context).pushReplacementNamed(MainScreen.routeName);
      },
      onRecoverPassword: (p0) => _recoverPassword(
          p0), //darimana mendapatkan value text dari recover password ?
      messages: LoginMessages(
        forgotPasswordButton: 'Lupa password?',
        recoverPasswordButton: 'Reset password',
        goBackButton: 'Kembali',
        recoverPasswordDescription:
            'Link reset password akan dikirimkan ke email anda',
        recoverPasswordSuccess: 'link reset berhasil dikirim',
      ),
    );
  }
}
