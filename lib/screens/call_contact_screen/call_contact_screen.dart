import 'dart:math';

import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/user/user_bloc.dart';
import 'package:bakamlachat/models/use_variable.dart';
import 'package:bakamlachat/models/user_models.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zego_uikit_prebuilt_call/zego_uikit_prebuilt_call.dart';

class CallContactScreen extends StatefulWidget {
  const CallContactScreen({super.key});

  @override
  State<CallContactScreen> createState() => _CallContactScreen();
}

class _CallContactScreen extends State<CallContactScreen> {
  List<Contact>? _contacts;
  List<Map<String, dynamic>> UserApi = [];
  List<Map<String, dynamic>> ListKontak = [];
  List<Map<String, dynamic>> BakamlaKontak = [];
  List<Map<String, dynamic>> NonBakamlaKontak = [];
  List<Map<String, dynamic>> SelfKontak = [];
  List<ZegoUIKitUser> invitees = [];
  List<Map<String, dynamic>> index = [];

  // ignore: unused_field
  bool _permissionDenied = false;

  @override
  void initState() {
    super.initState();
    _fetchContacts();
  }

  Future _fetchContacts() async {
    if (!await FlutterContacts.requestPermission(readonly: true)) {
      setState(() => _permissionDenied = true);
    } else {
      final contacts = await FlutterContacts.getContacts();
      setState(() {
        _contacts = contacts;
        _getKontak();
      });
    }
  }

  void _getKontak() async {
    for (var i = 0; i < _contacts!.length; i++) {
      final getKontak = await FlutterContacts.getContact(_contacts![i].id);
      if (getKontak!.phones.isNotEmpty &&
          getKontak.phones.first.number.isNotEmpty) {
        var numberPhone;
        var phoneNumber = getKontak.phones.first.number
            .replaceAll("-", "")
            .replaceAll(" ", "");
        if (phoneNumber.startsWith('0')) {
          numberPhone = "+62${phoneNumber.substring(1)}";
        } else {
          numberPhone = phoneNumber;
        }
        Map<String, dynamic> newObj = {
          'displayName': getKontak.displayName,
          'phones': numberPhone,
        };
        ListKontak.add(newObj);
      }
    }
    setState(() {
      _getUser();
    });
  }

  void _getUser() {
    final userB = context.read<UserBloc>();
    UserState us = userB.state;
    List<UserEntity> a = us.map(
      initial: (_) => [],
      loaded: (state) => state.users,
    );
    setState(() {
      UserApi = a.map((userEntity) {
        return {
          'id': userEntity.id,
          'username': userEntity.username,
          'phones': userEntity.phones,
          'avatar': userEntity.avatar,
        };
      }).toList();
      _selfContact();
      _intersectContact();
      _diffContact();
    });
  }

  void _selfContact() {
    final authBloc = context.read<AuthBloc>();
    for (var a in ListKontak) {
      if (a['phones'].contains(authBloc.state.user!.phones)) {
        // bool isExist = BakamlaKontak.any((e) => e['phones'] == a['phones']);
        // if (!isExist) {
        Map<String, dynamic> newObj = {
          'displayName': a['displayName'],
          'phones': a['phones'],
        };
        SelfKontak.add(newObj);
        break;
        // }
      }
    }
  }

  void _intersectContact() {
    for (var a in ListKontak) {
      for (var b in UserApi) {
        if (a['phones'].contains(b['phones'])) {
          bool isExist = BakamlaKontak.any((e) => e['phones'] == a['phones']);
          if (!isExist) {
            Map<String, dynamic> newObj = {
              'id': b['id'],
              'avatar': b['avatar'],
              'username': b['username'],
              'displayName': a['displayName'],
              'phones': b['phones'],
              'isSelected': false,
            };
            BakamlaKontak.add(newObj);
            break;
          }
        }
      }
    }
  }

  void _diffContact() {
    for (var a in ListKontak) {
      bool existsInNonBakamla =
          NonBakamlaKontak.any((e) => e['phones'] == a['phones']);
      bool existsInUserApi = UserApi.any((b) => b['phones'] == a['phones']);
      bool existsInSelf = SelfKontak.any((b) => b['phones'] == a['phones']);

      if (!existsInNonBakamla && !existsInUserApi && !existsInSelf) {
        Map<String, dynamic> newObj = {
          'displayName': a['displayName'],
          'phones': a['phones']
        };
        eLog(newObj);
        setState(() {
          NonBakamlaKontak.add(newObj);
        });
      }
    }
  }

  void addInvitee(ZegoUIKitUser invitee) {
    bool isAlreadyInvited =
        invitees.any((existingInvitee) => existingInvitee.id == invitee.id);

    if (!isAlreadyInvited) {
      setState(() {
        invitees.add(invitee);
      });
    }
  }

  void addIndexing(Map<String, dynamic> indexing) {
    bool isAlreadyInvited = index.any((e) => e['id'] == indexing['id']);

    if (!isAlreadyInvited) {
      setState(() {
        index.add(indexing);
      });
    }
  }

  void removeInvitee(ZegoUIKitUser invitee) {
    // eLog(invitee.name);
    Iterable<Map<String, dynamic>> searchResult = BakamlaKontak.where(
        (element) => element['id'].toString() == invitee.id);
    if (searchResult.isNotEmpty) {
      for (var abc in searchResult) {
        var check =
            index.where((element) => element['username'] == abc['username']);
        if (check.isNotEmpty) {
          for (var def in check) {
            BakamlaKontak[int.parse(def['id'])]['isSelected'] = false;
          }
        }
      }
    }
    setState(() {
      index.removeWhere((element) => element['username'] == invitee.name);
      invitees
          .removeWhere((existingInvitee) => existingInvitee.id == invitee.id);
    });
  }

  void _redirectToSMSApp(phone) async {
    // Nomor telepon yang dituju
    String phoneNumber = phone;

    // Pesan awal yang akan ditampilkan
    String initialMessage =
        "Ayo chat di Bakamla Messenger, Dapatkan di https://www.google.com";

    // Format URL untuk membuka aplikasi SMS dengan nomor tujuan dan pesan awal
    String uri = "sms:$phoneNumber?body=${Uri.encodeFull(initialMessage)}";

    // Buka aplikasi SMS dengan URI yang dibuat
    if (await canLaunchUrl(Uri.parse(uri))) {
      await launchUrl(Uri.parse(uri));
    } else {
      throw 'Could not launch $uri';
    }
  }

  @override
  Widget build(BuildContext context) {
    var total = BakamlaKontak.length + NonBakamlaKontak.length;

    return Scaffold(
      appBar: AppBar(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Total Kontak",
              style: TextStyle(fontSize: 15),
            ),
            Text('$total Kontak', style: TextStyle(fontSize: 15))
          ],
        ),
        actions: [
          IconButton(
              onPressed: () {
                // eLog(UserApi);
                eLog("self");
                eLog(SelfKontak);
              },
              icon: Icon(Icons.abc))
        ],
      ),
      body: BakamlaKontak.isNotEmpty && NonBakamlaKontak.isNotEmpty
          ? Container(
              child: Column(
                children: [
                  invitees.isNotEmpty
                      ? Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 100,
                                  width:
                                      MediaQuery.of(context).size.width * 0.65,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: invitees.length,
                                    itemBuilder: (context, index) {
                                      var namaInvitees = invitees[index];

                                      return InkWell(
                                        onTap: () {
                                          var lama = ZegoUIKitUser(
                                              id: "${namaInvitees.id}",
                                              name: namaInvitees.name);
                                          removeInvitee(lama);
                                        },
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              const Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 10, right: 10)),
                                              Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    const CircleAvatar(
                                                      backgroundColor:
                                                          Color.fromARGB(255,
                                                              228, 228, 228),
                                                      child: Icon(Icons.person),
                                                    ),
                                                    Text(
                                                        "${namaInvitees.name}"),
                                                  ]),
                                            ]),
                                      );
                                    },
                                  ),
                                ),
                                ZegoSendCallInvitationButton(
                                    onPressed: (code, message, p2) {
                                      ValueType.setString("audio");
                                      List<String> names = invitees
                                          .map((invitee) => invitee.name)
                                          .toList();
                                      List<String> id = invitees
                                          .map((invitee) => invitee.id)
                                          .toList();
                                      var allName = names.join(', ');
                                      var allId = id.join(', ');
                                      Value.setString(
                                        allId,
                                        allName,
                                      );
                                    },
                                    icon: ButtonIcon(
                                        icon: const Icon(
                                          Icons.call,
                                          color: Colors.white,
                                        ),
                                        backgroundColor: const Color.fromARGB(
                                            255, 59, 184, 21)),
                                    buttonSize: const Size(35, 35),
                                    iconSize: const Size(35, 35),
                                    iconTextSpacing: double.nan,
                                    margin: const EdgeInsets.only(
                                        left: 1, right: 5),
                                    padding: const EdgeInsets.all(0),
                                    callID: "${Random().nextInt(100)}",
                                    resourceID: "zego_call",
                                    invitees: invitees,
                                    isVideoCall: false),
                                const SizedBox(
                                  width: 10,
                                ),
                                ZegoSendCallInvitationButton(
                                    onPressed: (code, message, p2) {
                                      ValueType.setString("video");
                                      List<String> names = invitees
                                          .map((invitee) => invitee.name)
                                          .toList();
                                      List<String> id = invitees
                                          .map((invitee) => invitee.id)
                                          .toList();
                                      var allName = names.join(', ');
                                      var allId = id.join(', ');
                                      Value.setString(
                                        allId,
                                        allName,
                                      );
                                    },
                                    icon: ButtonIcon(
                                        icon: const Icon(
                                          Icons.video_call,
                                          color: Colors.white,
                                        ),
                                        backgroundColor: const Color.fromARGB(
                                            255, 59, 184, 21)),
                                    buttonSize: const Size(35, 35),
                                    iconSize: const Size(35, 35),
                                    iconTextSpacing: double.nan,
                                    margin: const EdgeInsets.only(
                                        left: 1, right: 5),
                                    padding: const EdgeInsets.all(0),
                                    callID: "${Random().nextInt(100)}",
                                    resourceID: "zego_call",
                                    invitees: invitees,
                                    isVideoCall: true),
                              ],
                            ),
                            const Divider(),
                          ],
                        )
                      : const SizedBox(
                          height: 2,
                        ),
                  Expanded(
                      child: Container(
                    child: BlocSelector<UserBloc, UserState, List<UserEntity>>(
                      selector: (state) {
                        return state.map(
                          initial: (_) => [],
                          loaded: (state) => state.users,
                        );
                      },
                      builder: (context, state) {
                        return ListView.builder(
                            itemCount: BakamlaKontak.length +
                                SelfKontak.length +
                                NonBakamlaKontak.length +
                                2,
                            itemBuilder: (context, i) {
                              eLog("Bakamla: ${BakamlaKontak.length}");
                              eLog("Non: ${NonBakamlaKontak.length}");
                              eLog("Self: ${SelfKontak.length}");
                              if (i == 0) {
                                // return the header
                                return const Padding(
                                    padding: EdgeInsets.all(15),
                                    child: Text(
                                      "Daftar Kontak Anda di Bakamla Messenger",
                                      style: TextStyle(color: Colors.grey),
                                    ));
                              }
                              i -= 1;

                              if (i == BakamlaKontak.length) {
                                return const Padding(
                                    padding: EdgeInsets.only(
                                        left: 15,
                                        right: 15,
                                        top: 30,
                                        bottom: 15),
                                    child: Text(
                                      "Undang Kontak ke Bakamla Messenger",
                                      style: TextStyle(color: Colors.grey),
                                    ));
                              }
                              if (i >= BakamlaKontak.length &&
                                  i <=
                                      BakamlaKontak.length +
                                          SelfKontak.length) {
                                return ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: SelfKontak.length,
                                    itemBuilder: (context, index) {
                                      return Card(
                                        child: InkWell(
                                          onTap: () {},
                                          child: ListTile(
                                            leading: const CircleAvatar(
                                              backgroundColor: Color.fromARGB(
                                                  255, 228, 228, 228),
                                              child: Icon(Icons.person),
                                            ),
                                            title: Text(
                                                "${SelfKontak[index]["displayName"]} (Anda)"),
                                            subtitle: Text(
                                                SelfKontak[index]["phones"]),
                                          ),
                                        ),
                                      );
                                    });
                              }

                              if (i >=
                                      BakamlaKontak.length +
                                          SelfKontak.length &&
                                  i <=
                                      BakamlaKontak.length +
                                          SelfKontak.length +
                                          NonBakamlaKontak.length) {
                                return ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: NonBakamlaKontak.length,
                                    itemBuilder: (context, ind) {
                                      return Card(
                                        child: InkWell(
                                          onTap: () {
                                            _redirectToSMSApp(
                                                NonBakamlaKontak[ind]
                                                    ['phones']);
                                          },
                                          child: ListTile(
                                            leading: const CircleAvatar(
                                              backgroundColor: Color.fromARGB(
                                                  255, 228, 228, 228),
                                              child: Icon(Icons.person),
                                            ),
                                            title: Text(NonBakamlaKontak[ind]
                                                ["displayName"]),
                                            subtitle: Text(NonBakamlaKontak[ind]
                                                ["phones"]),
                                            trailing: const Text(
                                              "Undang",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.red),
                                            ),
                                          ),
                                        ),
                                      );
                                    });
                              }

                              return Card(
                                child: InkWell(
                                  onTap: () {
                                    var baru = ZegoUIKitUser(
                                        id: BakamlaKontak[i]["id"].toString(),
                                        name: BakamlaKontak[i]["username"]);
                                    if (BakamlaKontak[i]['isSelected'] ==
                                        false) {
                                      BakamlaKontak[i]['isSelected'] = true;
                                      addInvitee(baru);
                                      Map<String, dynamic> indexing = {
                                        'id': "${i}",
                                        'username': BakamlaKontak[i]
                                            ['username'],
                                      };
                                      addIndexing(indexing);
                                      setState(() {
                                        eLog(invitees);
                                      });
                                    } else {
                                      removeInvitee(baru);
                                    }
                                  },
                                  child: ListTile(
                                    leading: const CircleAvatar(
                                      backgroundColor:
                                          Color.fromARGB(255, 228, 228, 228),
                                      child: Icon(Icons.person),
                                    ),
                                    title: Text(
                                        "${BakamlaKontak[i]["username"]} (${BakamlaKontak[i]["phones"]})"),
                                    subtitle:
                                        Text(BakamlaKontak[i]["displayName"]),
                                    trailing: CircleAvatar(
                                      backgroundColor: BakamlaKontak[i]
                                              ['isSelected']
                                          ? Colors.red
                                          : const Color.fromARGB(
                                              255, 228, 228, 228),
                                      child: Icon(
                                        BakamlaKontak[i]["isSelected"]
                                            ? Icons.check
                                            : Icons.add,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            });
                      },
                    ),
                  ))
                ],
              ),
            )
          : BakamlaKontak.isEmpty && NonBakamlaKontak.isNotEmpty
              ? Container(
                  child: Column(
                    children: [
                      Expanded(
                        child: Container(
                          child: ListView.builder(
                              itemCount: NonBakamlaKontak.length,
                              itemBuilder: (context, ind) {
                                return Card(
                                  child: InkWell(
                                    onTap: () {
                                      _redirectToSMSApp(
                                          NonBakamlaKontak[ind]['phones']);
                                    },
                                    child: ListTile(
                                      leading: const CircleAvatar(
                                        backgroundColor:
                                            Color.fromARGB(255, 228, 228, 228),
                                        child: Icon(Icons.person),
                                      ),
                                      title: Text(
                                          NonBakamlaKontak[ind]["displayName"]),
                                      subtitle:
                                          Text(NonBakamlaKontak[ind]["phones"]),
                                      trailing: const Text(
                                        "Undang",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.red),
                                      ),
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ),
                    ],
                  ),
                )
              : Center(
                  child: CircularProgressIndicator(),
                ),
    );
  }
}
