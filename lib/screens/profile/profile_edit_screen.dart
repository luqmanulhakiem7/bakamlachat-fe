import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/profile/profile_bloc.dart';
import 'package:bakamlachat/utils/zego/zego_init.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

String _oldValue = '';

class OldValue {
  String value(String e) {
    _oldValue = e;

    return _oldValue;
  }

  String getValue() {
    return _oldValue;
  }
}

class ProfileEditScreen extends StatefulWidget {
  final String? value;
  final String tipe;
  const ProfileEditScreen({super.key, this.value, required this.tipe});

  @override
  State<ProfileEditScreen> createState() => _ProfileEditScreen();
}

class _ProfileEditScreen extends State<ProfileEditScreen> {
  final profileBloc = ProfileBloc();
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.addListener(_saveOldValue);
    OldValue().value(widget.value as String);
    _controller.text = OldValue().getValue();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _saveOldValue() {
    setState(() {
      OldValue().value(_controller.text);
    });
  }

  void saveUsername() async {
    FormData formData = FormData.fromMap({
      'username': _controller.text,
    });
    profileBloc.add(ProfileEvent.changeProfile(
      data: formData,
      type: 'username',
    ));
    final authState = context.read<AuthBloc>().state;
    ZegoInit().logout();
    ZegoInit().init("${authState.user!.id}", authState.user!.username);
    Navigator.of(context)
      ..pop()
      ..pop()
      ..pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.tipe,
          style: const TextStyle(fontSize: 17),
        ),
        actions: [
          widget.tipe == 'Username'
              ? IconButton(
                  onPressed: () async {
                    saveUsername();
                  },
                  icon: const Icon(Icons.check))
              : const Visibility(visible: false, child: Icon(Icons.check))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10, bottom: 2, top: 10),
        child: widget.tipe == 'Username'
            ? WidgetUsername(controller: _controller, widget: widget)
            : WidgetBio(controller: _controller, widget: widget),
      ),
    );
  }
}

class WidgetUsername extends StatelessWidget {
  const WidgetUsername({
    super.key,
    required TextEditingController controller,
    required this.widget,
  }) : _controller = controller;

  final TextEditingController _controller;
  final ProfileEditScreen widget;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 2),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: _controller,
              decoration: InputDecoration(
                labelText: 'Atur ${widget.tipe}',
              ),
            ),
          ],
        ),
        const SizedBox(height: 20),
        const Expanded(
          child: Column(
            children: [
              Text(
                "Username anda pada Bakamla Messenger akan ditampilkan pada kontak teman dan digunakan untuk mencari profil anda",
                style: TextStyle(color: Colors.grey),
              ),
              SizedBox(height: 5),
              Text(
                "Anda dapat menggunakan a-z, 0-9, dan garis bawah.",
                style: TextStyle(color: Colors.grey),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class WidgetBio extends StatefulWidget {
  const WidgetBio({
    super.key,
    required TextEditingController controller,
    required this.widget,
  }) : _controller = controller;

  // ignore: unused_field
  final TextEditingController _controller;
  final ProfileEditScreen widget;

  @override
  State<WidgetBio> createState() => _WidgetBio();
}

class _WidgetBio extends State<WidgetBio> {
  final profileBloc = ProfileBloc();
  void changeValue(e) {
    setState(() {
      OldValue().value(e);
    });
  }

  List<dynamic> value = [
    {'id': 0, 'value': 'Ada'},
    {'id': 1, 'value': 'Sibuk'},
    {'id': 2, 'value': 'Di kantor'},
    {'id': 3, 'value': 'Baterai hampir habis'},
    {'id': 4, 'value': 'Sedang rapat'},
    {'id': 5, 'value': 'Sedang olahraga'},
  ];

  void saveBio() async {
    FormData formData = FormData.fromMap({
      'bio': OldValue().getValue().toString(),
    });
    profileBloc.add(ProfileEvent.changeProfile(
      data: formData,
      type: 'bio',
    ));
    Navigator.of(context)
      ..pop()
      ..pop()
      ..pop();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 2),
        const Padding(
          padding: EdgeInsets.all(5),
          child: Text("Saat ini disetel ke"),
        ),
        ListTile(
          title: Text("${widget.widget.value}"),
          tileColor: Colors.grey[100],
        ),
        const Divider(),
        const SizedBox(height: 2),
        const Padding(
          padding: EdgeInsets.all(5),
          child: Text("Pilih Info"),
        ),
        Expanded(
          child: ListView.builder(
            itemCount: value.length,
            itemBuilder: (context, i) {
              bool same = value[i]["value"] == widget.widget.value;
              return ListTile(
                onTap: () {
                  changeValue(value[i]["value"]);
                  saveBio();
                },
                title: Text("${value[i]["value"]}"),
                trailing: same
                    ? const Icon(Icons.check)
                    : Visibility(
                        visible: false,
                        child: Icon(
                          Icons.abc,
                          color: Colors.red[0],
                        ),
                      ),
              );
            },
          ),
        ),
      ],
    );
  }
}
