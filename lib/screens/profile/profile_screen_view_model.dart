import 'dart:io';

import 'package:bakamlachat/blocs/profile/profile_bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ProfileScreenViewModel {
  final profileBloc = ProfileBloc();
  final _picker = ImagePicker();

  Future<void> pickImage(context) async {
    final XFile? pickedFile =
        await _picker.pickImage(source: ImageSource.gallery, imageQuality: 20);
    if (pickedFile != null) {
      File imageFile = File(pickedFile.path);
      _cropImage(imageFile, context);
    }
  }

  Future<void> _cropImage(image, context) async {
    if (image != null) {
      final croppedFile = await ImageCropper().cropImage(
        sourcePath: image!.path,
        compressFormat: ImageCompressFormat.jpg,
        compressQuality: 100,
        aspectRatioPresets: [
          CropAspectRatioPreset.ratio4x3,
        ],
        uiSettings: [
          AndroidUiSettings(
              toolbarTitle: 'Potong Gambar',
              toolbarColor: Colors.deepOrange,
              toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.original,
              lockAspectRatio: false),
          IOSUiSettings(
            title: 'Potong Gambar',
          ),
          WebUiSettings(
            context: context,
            presentStyle: CropperPresentStyle.dialog,
            boundary: const CroppieBoundary(
              width: 520,
              height: 520,
            ),
            viewPort:
                const CroppieViewPort(width: 480, height: 480, type: 'circle'),
            enableExif: true,
            enableZoom: true,
            showZoomer: true,
          ),
        ],
      );
      if (croppedFile != null) {
        updateAvatar(croppedFile.path);
      }
    }
  }

  Future<void> updateAvatar(String path) async {
    FormData formData = FormData.fromMap({
      'avatar': await MultipartFile.fromFile(path, filename: 'aba'),
    });
    profileBloc.add(ProfileEvent.changeProfile(
      data: formData,
      type: 'profile',
    ));
  }
}
