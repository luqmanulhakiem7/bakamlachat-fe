import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/contact/contact_bloc.dart';
import 'package:bakamlachat/blocs/profile/profile_bloc.dart';
import 'package:bakamlachat/models/profile.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/screens/profile/profile_edit_screen.dart';
import 'package:bakamlachat/screens/profile/profile_screen_view_model.dart';
import 'package:bakamlachat/utils/formatter_phone.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:bakamlachat/utils/widget/avatar_profile.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfileScreen extends StatefulWidget {
  final ProfileScreenViewModel viewModel;
  const ProfileScreen({super.key, required this.viewModel});
  static const routeName = "profile";

  @override
  State<ProfileScreen> createState() => _ProfileScreen();
}

class _ProfileScreen extends State<ProfileScreen> {
  final profileBloc = ProfileBloc();
  // final contactBloc = ContactBloc().onEvent(s);
  final authToken = AuthBloc().state.token;

  void coba() async {
    final contact = await Dio().get(Endpoints.contact,
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $authToken"
          },
        ));
    // Konversi respons JSON ke dalam bentuk List<Map<String, dynamic>>
    List<Map<String, dynamic>> contactData =
        List<Map<String, dynamic>>.from(contact.data);

// Mendapatkan daftar pengguna dari setiap objek dalam respons
    List<dynamic> users2 = contactData.map((item) => item['user'][0]).toList();

    eLog(users2);
  }

  @override
  void initState() {
    super.initState();
    profileBloc.add(const ProfileEvent.getProfile());
    eLog("AA");
    coba();
    // contactBloc.add(const ContactEvent.getContact());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile"),
      ),
      body: BlocConsumer<ProfileBloc, ProfileState>(
        listener: (BuildContext context, ProfileState state) {
          if (state is ProfileError) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: const Text('Error'),
                content: Text(state.message),
                actions: [
                  TextButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: const Text('OK'),
                  ),
                ],
              ),
            );
          }
        },
        builder: (context, state) {
          if (state is ProfileInitial) {
            return const Center(child: Text('initial'));
          } else if (state is ProfileLoaded) {
            final data = state.profile;
            // return Center(child: Text(data!.username));
            return widgetLoaded(context, data);
          } else if (state is ProfileLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return const Center(child: Text('Unknown state'));
          }
        },
      ),
    );
  }

  ListView widgetLoaded(BuildContext context, ProfileEntity? data) {
    return ListView(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.65,
          child: Column(
            children: [
              const SizedBox(height: 10),
              Center(
                child: GestureDetector(
                  onTap: () {
                    // Handle tap event on image
                    eLog('Image was tapped!');
                  },
                  child: Stack(
                    children: [
                      AvatarProfile(
                        avatar: data!.avatar,
                        height: 100,
                        width: 100,
                      ),
                      Positioned(
                        bottom: 0,
                        right: 1,
                        child: CircleAvatar(
                          child: IconButton(
                            icon: const Icon(Icons.camera_alt),
                            onPressed: () {
                              // Handle tap event on button icon
                              widget.viewModel.pickImage(context);
                              eLog('Button icon was tapped!');
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 20),
        ListTile(
          leading: const Icon(Icons.person_2),
          titleAlignment: ListTileTitleAlignment.threeLine,
          title: const Text(
            "Nama",
            style: TextStyle(color: Colors.grey, fontSize: 13),
          ),
          subtitle: Text(
            data.username,
            style: const TextStyle(fontSize: 15, color: Colors.black),
          ),
          trailing: IconButton(
              onPressed: () {
                // Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProfileEditScreen(
                          value: data.username,
                          tipe: 'Username',
                        )));
              },
              icon: const Icon(Icons.edit)),
        ),
        const Divider(),
        ListTile(
          leading: const Icon(Icons.info),
          title: const Text(
            "Info",
            style: TextStyle(color: Colors.grey, fontSize: 13),
          ),
          subtitle: Text(
            data.bio.toString(),
            style: const TextStyle(fontSize: 15, color: Colors.black),
          ),
          titleAlignment: ListTileTitleAlignment.threeLine,
          trailing: IconButton(
              onPressed: () {
                const getProfile();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProfileEditScreen(
                          value: data.bio,
                          tipe: 'Bio',
                        )));
              },
              icon: const Icon(Icons.edit)),
        ),
        const Divider(),
        ListTile(
          leading: const Icon(Icons.confirmation_num),
          title: const Text(
            "No Anggota",
            style: TextStyle(color: Colors.grey, fontSize: 13),
          ),
          subtitle: Text(
            data.kta != null ? data.kta.toString() : "Belum di Setting",
            style: const TextStyle(fontSize: 15, color: Colors.black),
          ),
          titleAlignment: ListTileTitleAlignment.threeLine,
          enabled: false,
        ),
      ],
    );
  }
}
