import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chat/chat_bloc.dart';
import 'package:bakamlachat/blocs/user/user_bloc.dart';
import 'package:bakamlachat/models/models.dart';
import 'package:bakamlachat/screens/chat/chat_screen.dart';
import 'package:bakamlachat/screens/chat_list/add_chat_screen.dart';
import 'package:bakamlachat/screens/chat_list/chat_list_item.dart';
import 'package:bakamlachat/utils/laravel_echo/laravel_echo.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:bakamlachat/utils/onesignal/onesignal.dart';
import 'package:bakamlachat/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:search_page/search_page.dart';

class ChatListScreen extends StatefulWidget {
  const ChatListScreen({super.key});
  static const routeName = "chat-list";

  @override
  State<ChatListScreen> createState() => _ChatListScreenState();
}

class _ChatListScreenState extends State<ChatListScreen> {
  Future<void> setupOneSignal(int userId) async {
    await initOneSignal();
    registerOneSignalEventListener(
      onOpened: onOpened,
      onReceivedInForeground: onReceivedInForeground,
    );
    promptPolicyPrivacy(userId);
  }

  void onOpened(OSNotificationOpenedResult result) {
    vLog('notification opened handler called with: ${result}');

    vLog(
        "opened notification: \n${result.notification.jsonRepresentation().replaceAll("\\n", "\n")}");

    try {
      final data = result.notification.additionalData;
      if (data != null) {
        final chatId = (data['data']['chatId'] as int);
        final chatBloc = context.read<ChatBloc>();
        final selectedChat = chatBloc.state.selectedChat;

        if (chatId != selectedChat?.id) {
          chatBloc.add(ChatNotificationOpened(chatId));
          Navigator.of(context).pushNamed(ChatScreen.routeName);
        }
      }
    } catch (_) {}
  }

  void onReceivedInForeground(OSNotificationReceivedEvent event) {
    vLog(
        "notification received in foreground notification: \n${event.notification.jsonRepresentation().replaceAll("\\n", "\n")}");

    try {
      final chatBloc = context.read<ChatBloc>();
      final data = event.notification.additionalData;
      final selectedChat = chatBloc.state.selectedChat;

      if (selectedChat != null && data != null) {
        vLog(data);
        final chatId = (data['data']['chatId'] as int);

        if (selectedChat.id == chatId) {
          event.complete(null);
          return;
        }
      }
      chatBloc.add(const ChatStarted());
      event.complete(event.notification);
    } catch (e) {
      event.complete(null);
    }
  }

  Future<void> promptPolicyPrivacy(int userId) async {
    final oneSignalShared = OneSignal.shared;

    bool userProvidedPrivacyConsent =
        await oneSignalShared.userProvidedPrivacyConsent();

    if (userProvidedPrivacyConsent) {
      sendUserTag(userId);
    } else {
      bool requiresConsent = await oneSignalShared.requiresUserPrivacyConsent();
      if (requiresConsent) {
        final accepted =
            await OneSignal.shared.promptUserForPushNotificationPermission();
        if (accepted) {
          await oneSignalShared.consentGranted(true);
          sendUserTag(userId);
          //   Navigator.of(context).pushNamed(ChatScreen.routeName);
        }
      } else {
        sendUserTag(userId);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final autBloc = context.read<AuthBloc>();
    final currentUser = autBloc.state.user!;
    final chatBloc = context.read<ChatBloc>();
    final userBloc = context.read<UserBloc>();

    return StartUpContainer(
      onInit: () async {
        chatBloc.add(const ChatStarted());
        userBloc.add(const UserStarted());

        LaravelEcho.init(token: autBloc.state.token!);
        setupOneSignal(autBloc.state.user!.id);
      },
      onDisposed: () {
        // LaravelEcho.instance.disconnect();
      },
      child: Scaffold(
        body: RefreshIndicator(
          onRefresh: () async {
            chatBloc.add(const ChatStarted());
            userBloc.add(const UserStarted());
          },
          child: BlocConsumer<ChatBloc, ChatState>(
            listener: (_, __) {},
            builder: (context, state) {
              // eLog("Chatsnya : ${state.chats}");
              if (state.chats.isEmpty) {
                return const BlankContent(
                  content: "Belum ada Chat",
                  icon: Icons.chat_rounded,
                );
              }

              return ListView.separated(
                itemBuilder: (context, index) {
                  final item = state.chats[index];

                  return ChatListItem(
                      key: ValueKey(item.id),
                      item: item,
                      currentUser: currentUser,
                      onPressed: (chat) {
                        chatBloc.add(ChatSelected(chat));
                        Navigator.of(context).pushNamed(ChatScreen.routeName);
                      });
                },
                separatorBuilder: (_, __) => const Divider(
                  height: 1.5,
                ),
                itemCount: state.chats.length,
              );
            },
          ),
        ),
        floatingActionButton:
            BlocSelector<UserBloc, UserState, List<UserEntity>>(
          selector: (state) {
            return state.map(
              initial: (_) => [],
              loaded: (state) => state.users,
            );
          },
          builder: (context, state) {
            eLog("Users : $state");

            return FloatingActionButton(
              // onPressed: () => _showSearch(context, state),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (context) => const AddChatScreen()),
                );
              },
              child: const Icon(Icons.message),
            );
          },
        ),
      ),
    );
  }

  void _showSearch(BuildContext context, List<UserEntity> users) {
    showSearch(
      context: context,
      delegate: SearchPage<UserEntity>(
        items: users,
        searchLabel: 'Cari Kontak',
        suggestion: const Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.search,
                size: 25.0,
                color: Colors.grey,
              ),
              Text(
                'Cari Kontak berdasarkan nama',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
        failure: const Center(
          child: Text(
            'Kontak tidak ditemukan :(',
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.w400,
              color: Colors.grey,
            ),
          ),
        ),
        filter: (user) => [
          user.kta,
        ],
        builder: (user) => ListTile(
          leading: const Icon(Icons.account_circle, size: 50.0),
          title: Text(user.username),
          subtitle: Text(user.email),
          onTap: () {
            // Selected User
            vLog("usernya : ${user.id}");
            final chatBloc = context.read<ChatBloc>();
            chatBloc.add(UserSelected(user));

            Navigator.of(context).pushNamed(ChatScreen.routeName);
          },
        ),
      ),
    );
  }
}
