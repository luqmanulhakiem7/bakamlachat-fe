import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chat/chat_bloc.dart';
import 'package:bakamlachat/models/models.dart';
import 'package:bakamlachat/utils/chat.dart';
import 'package:bakamlachat/utils/formatting.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:bakamlachat/utils/my_encription.dart';
import 'package:bakamlachat/utils/widget/avatar_profile.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatListItem extends StatelessWidget {
  const ChatListItem({
    Key? key,
    required this.item,
    required this.currentUser,
    required this.onPressed,
  }) : super(key: key);

  final ChatEntity item;
  final UserEntity currentUser;
  final void Function(ChatEntity) onPressed;

  bool get sameMessage =>
      item.lastmessage?.message == item.lastmessage?.images ||
      item.lastmessage?.message == item.lastmessage?.videos ||
      item.lastmessage?.message == item.lastmessage?.audios ||
      item.lastmessage?.message == item.lastmessage?.locations;
  bool get hasMedia =>
      (item.lastmessage?.images ?? item.lastmessage?.videos) != null;
  bool get isImage => item.lastmessage?.images != null;
  bool get isVideo => item.lastmessage?.videos != null;
  bool get isDocument => item.lastmessage?.documents != null;
  bool get isAudio => item.lastmessage?.audios != null;
  bool get isContact => item.lastmessage?.contact_name != null;
  bool get isLocation => item.lastmessage?.locations != '0';

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: AvatarProfile(
          avatar: getAvatar(item.participants, currentUser),
          height: 50,
          width: 50),
      title: Text(item.name ??
          getChatName(
            item.participants,
            currentUser,
          )),
      subtitle: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          isImage
              ? const Icon(Icons.image)
              : isVideo
                  ? const Icon(Icons.video_file_rounded)
                  : isDocument
                      ? const Icon(Icons.insert_drive_file)
                      : isAudio
                          ? const Icon(Icons.keyboard_voice_rounded)
                          : isLocation
                              ? const Icon(Icons.location_pin)
                              : isContact
                                  ? const Icon(Icons.person_pin_rounded)
                                  : const Text(''),
          sameMessage
              ? pesanSama()
              : Expanded(
                  child: Text(
                    item.lastmessage!.hasKey
                        ? MyEncryptionDecryption.decryptAES(
                            item.lastmessage!.message,
                            item.lastmessage!.ivkey.toString())
                        : item.lastmessage?.message ?? "...",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                ),
          Text(utcToLocal(item.updatedAt)),
        ],
      ),
      onTap: () => onPressed(item),
      onLongPress: () async {
        return _showDeleteConfirmationDialog(context, item.id.toString());
      },
    );
  }

  void _showDeleteConfirmationDialog(BuildContext context, String idMsg) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Hapus Chat'),
          content: const Text('Apakah anda yakin ingin menghapus chat ini?'),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Delete'),
              onPressed: () async {
                final token = AuthBloc().state.token.toString();
                Dio dio = Dio();
                final urlId =
                    'https://api.bakamla.barengsaya.com/api/chat_delete/$idMsg';
                eLog(urlId);
                try {
                  final response = await dio.get(
                    urlId,
                    options: Options(
                      headers: {'Authorization': 'Bearer $token'},
                    ),
                  );
                  if (response.statusCode == 200) {
                    // ignore: use_build_context_synchronously
                    final chatBloc = context.read<ChatBloc>();
                    chatBloc.add(const ChatStarted());
                  } else {
                    eLog('code bukan 200');
                  }
                } catch (e) {
                  eLog(e);
                }
                // _deleteMessage(message);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Expanded pesanSama() {
    return isImage
        ? const Expanded(child: Text("Foto"))
        : isVideo
            ? const Expanded(child: Text("Video"))
            : isDocument
                ? const Expanded(child: Text("Document"))
                : isAudio
                    ? const Expanded(child: Text("Voice"))
                    : isLocation
                        ? const Expanded(child: Text("Lokasi"))
                        : const Expanded(child: Text(" "));
  }
}
