import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/blocs/chat/chat_bloc.dart';
import 'package:bakamlachat/blocs/user/user_bloc.dart';
import 'package:bakamlachat/models/models.dart';
import 'package:bakamlachat/models/user_models.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/screens/chat/chat_screen.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search_page/search_page.dart';

class AddChatScreen extends StatefulWidget {
  const AddChatScreen({super.key});

  @override
  State<AddChatScreen> createState() => _AddChatScreen();
}

class _AddChatScreen extends State<AddChatScreen> {
  List<dynamic> userApi = [];
  final token = AuthBloc().state.token;
  UserEntity? userSelected;

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  void _getUser() async {
    final contact = await Dio().get(Endpoints.contact,
        options: Options(
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          },
        ));

    setState(() {
      userApi = contact.data.map((item) {
        Map<String, dynamic> user = Map<String, dynamic>.from(item['user'][0]);
        user['isSelected'] = false;
        return user;
      }).toList();
    });
  }

  void _addKontak(int userId) async {
    try {
      final contact = await Dio().post(Endpoints.contact,
          data: {
            'id': userId,
          },
          options: Options(
            headers: {
              "Accept": "application/json",
              "Authorization": "Bearer $token"
            },
          ));
      if (contact.statusCode == 200) {
        eLog('success');
      }
    } catch (e) {
      eLog(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pesan Baru"),
      ),
      body: Container(
        child: userApi.length <= 0
            ?
            // const Center(
            //     child: CircularProgressIndicator(),
            //   )
            const Center(
                child: Text(
                  'Belum Memiliki Teman :(',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey,
                  ),
                ),
              )
            : ListView.builder(
                itemCount: userApi.length,
                itemBuilder: (context, i) {
                  return Card(
                    child: ListTile(
                      leading: userApi[i]['avatar'] != null
                          ? CircleAvatar(
                              radius: 20.0,
                              backgroundImage: NetworkImage(
                                  Endpoints.avatar + userApi[i]['avatar']),
                            )
                          : const CircleAvatar(
                              backgroundColor:
                                  Color.fromARGB(255, 228, 228, 228),
                              child: Icon(Icons.person),
                            ),
                      title: Text("${userApi[i]['username']}"),
                      // subtitle: Text("${userApi[i]['phones']}"),
                      subtitle: const Text(""),
                      trailing: IconButton(
                          onPressed: () {
                            final chatBloc = context.read<ChatBloc>();
                            var user = UserEntity(
                                id: userApi[i]["id"],
                                email: userApi[i]["email"],
                                username: userApi[i]["username"]);

                            eLog(userApi[i]["id"]);

                            chatBloc.add(UserSelected(user));
                            Navigator.of(context)
                                .pushNamed(ChatScreen.routeName);
                          },
                          icon: const Icon(Icons.chat)),
                    ),
                  );
                },
              ),
      ),
      floatingActionButton: BlocSelector<UserBloc, UserState, List<UserEntity>>(
        selector: (state) {
          return state.map(
            initial: (_) => [],
            loaded: (state) => state.users,
          );
        },
        builder: (context, state) {
          return FloatingActionButton(
            onPressed: () => _showSearch(context, state),
            child: const Icon(Icons.person_add),
          );
        },
      ),
    );
  }

  void _showSearch(BuildContext context, List<UserEntity> users) {
    String query = '';
    showSearch(
        context: context,
        delegate: SearchPage<UserEntity>(
          items: users,
          searchLabel: 'Cari Kontak',
          suggestion: const Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.search,
                  size: 25.0,
                  color: Colors.grey,
                ),
                Text(
                  'Cari Kontak berdasarkan No Anggota',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
          failure: const Center(
            child: Text(
              'Kontak tidak ditemukan :(',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.w400,
                color: Colors.grey,
              ),
            ),
          ),
          filter: (user) => query.length > 5
              ? [
                  user.kta,
                ]
              : [],
          onQueryUpdate: (value) {
            query = value;
          },
          builder: (user) => ListTile(
            leading: const Icon(Icons.account_circle, size: 50.0),
            title: Text(user.username),
            subtitle: Text(user.kta.toString()),
            trailing: IconButton(
                onPressed: () {
                  eLog(user);
                  _addKontak(user.id);
                  // userSelected = user;
                  setState(() {
                    _getUser();
                  });
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.save)),
            // onTap: () {
            //   // vLog("usernya : ${user.id}");
            //   final chatBloc = context.read<ChatBloc>();
            //   chatBloc.add(UserSelected(user));

            //   Navigator.of(context).pushNamed(ChatScreen.routeName);
            // },
          ),
        ));
  }
}
