import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/screens/guest/guest_screen.dart';
import 'package:bakamlachat/screens/home/home_screen.dart';
import 'package:bakamlachat/screens/redirect/no_connection_screen.dart';
import 'package:bakamlachat/utils/logger.dart';
import 'package:bakamlachat/utils/zego/zego_init.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  static const routeName = 'splash';

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool _isInit = false;

  @override
  void didChangeDependencies() {
    _initialize();
    super.didChangeDependencies();
  }

  // ignore: unused_field
  late DateTime _currentTime;

  MyClass() {
    _currentTime = DateTime.now();
  }

  void _initialize() async {
    MyClass();

    if (!_isInit) {
      await Future.delayed(const Duration(milliseconds: 500));
      if (!mounted) return;
      final authState = context.read<AuthBloc>().state;
      if (authState.user != null) {
        ZegoInit().init("${authState.user!.id}", authState.user!.username);
      }

      final connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        // I am connected to a mobile network.
        final redirectScreen = authState.isAuthenticated
            ? MainScreen.routeName
            : GuestScreen.routeName;
        Navigator.of(context).pushReplacementNamed(redirectScreen);

        eLog("Kamu Konek Menggunakan Data");
      } else if (connectivityResult == ConnectivityResult.vpn) {
        // I am connected to a vpn network.
        eLog("Kamu Konek Menggunakan VPN");
      } else if (connectivityResult == ConnectivityResult.other) {
        eLog("Kamu Konek Menggunakan Sesuatu yang tidak diketahui");
        // I am connected to a network which is not in the above mentioned networks.
      } else if (connectivityResult == ConnectivityResult.none) {
        const redirectScreen = NoConnectionScreen.routeName;
        eLog("Kamu Ga Konek");
        Navigator.of(context).pushReplacementNamed(redirectScreen);
        // I am not connected to any network.
      }
      // final redirectScreen = authState.isAuthenticated
      //     ? MainScreen.routeName
      //     : GuestScreen.routeName;

      _isInit = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 300.0,
        height: 150.0,
        child: const Column(
          children: [
            Icon(
              Icons.chat,
              color: Colors.red,
              size: 100.0,
            ),
            Text("Bakamla Messenger"),
          ],
        ),
      ),
    );
  }
}
