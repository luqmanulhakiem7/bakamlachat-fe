class Endpoints {
  // Api saat ini
  static const baseUrl = "https://api.bakamla.barengsaya.com";
  static const _apiVersion = "/api";

  // Api Fast
  static const storage = "$baseUrl/storage";
  static const sound = "$storage/sound/";
  static const audio = "$storage/audio/";
  static const image = "$storage/image/";
  static const video = "$storage/video/";
  static const document = "$storage/document/";
  static const avatar = "$storage/avatar/";
  static const profileGet = "$baseUrl$_apiVersion/profile";
  static const profileUsername = "$baseUrl$_apiVersion/profile-username";
  static const profileBio = "$baseUrl$_apiVersion/profile-bio";
  static const profileAvatar = "$baseUrl$_apiVersion/profile-avatar";
  static const chatMessage = "$baseUrl$_apiVersion/chat_message";
  static const chatGrupMessage = "$baseUrl$_apiVersion/chat_message_grup";
  static const createGrupChat = "$baseUrl$_apiVersion/chat-group";
  static const contact = "$baseUrl$_apiVersion/contact";
  // Auth
  static const _baseAuth = "$_apiVersion/auth";

  static const register = "$_baseAuth/register";
  static const login = "$_baseAuth/login";
  static const loginWithToken = "$_baseAuth/login_with_token";
  static const logout = "$_baseAuth/logout";

  // Chat
  static const _baseChat = "$_apiVersion/chat";
  static const _baseChatGrup = "$_apiVersion/chat-group";

  static const getChats = _baseChat;
  static const getSingleChat = "$_baseChat/";
  static const createChat = _baseChat;

  static const getGrupChats = _baseChatGrup;

  // Chat Message
  static const _baseChatMessage = "$_apiVersion/chat_message";
  static const _baseGrupChatMessage = "$_apiVersion/chat_message_grup";

  static const getChatMessages = _baseChatMessage;
  static const createChatMessages = _baseChatMessage;

  static const getGrupChatMessages = _baseGrupChatMessage;
  static const createGrupChatMessages = _baseGrupChatMessage;

  // User
  static const getUsers = "$_apiVersion/user";

  // Profile
  static const getProfileUser = "$_apiVersion/profile";
}
