import 'package:bakamlachat/models/app_response.dart';
import 'package:bakamlachat/models/chat_model.dart';
import 'package:bakamlachat/models/requests/create_chat_request.dart';

abstract class BaseChatRepository {
  Future<AppResponse<List<ChatEntity>>> getChats();

  Future<AppResponse<ChatEntity?>> createChat(CreateChatRequest request);

  Future<AppResponse<ChatEntity?>> getSingleChat(int chatId);

  Future<AppResponse<List<ChatEntity>>> getGrupChats();

  Future<AppResponse<List<ChatEntity>>> createGrupChat();

  Future<AppResponse<List<ChatEntity>>> getSingleGrupChat(int chatId);
}
