// import 'package:bakamlachat/blocs/auth/auth_bloc.dart';
import 'package:bakamlachat/models/app_response.dart';
import 'package:bakamlachat/models/user_models.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/repositories/user/base_user_repository.dart';
import 'package:bakamlachat/utils/dio_client/dio_client.dart';
import 'package:dio/dio.dart';

class UserRepository extends BaseUserRepository {
  final Dio _dioClient;

  UserRepository({
    Dio? dioClient,
  }) : _dioClient = dioClient ?? DioClient().instance;
  @override
  Future<AppResponse<List<UserEntity>>> getUsers() async {
    final response = await _dioClient.get(Endpoints.getUsers);

    return AppResponse<List<UserEntity>>.fromJson(
      response.data,
      (dynamic json) {
        if (response.data['success'] && json != null) {
          return (json as List<dynamic>)
              .map((e) => UserEntity.fromJson(e))
              .toList();
        }
        return [];
      },
    );
  }
}
