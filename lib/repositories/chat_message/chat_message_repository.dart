import 'package:bakamlachat/models/app_response.dart';
import 'package:bakamlachat/models/chat_message_model.dart';
import 'package:bakamlachat/models/requests/create_chat_messages_request.dart';
import 'package:bakamlachat/repositories/auth/core/endpoints.dart';
import 'package:bakamlachat/repositories/chat_message/base_chat_message_repository.dart';
import 'package:bakamlachat/utils/dio_client/dio_client.dart';
import 'package:dio/dio.dart';

class ChatMessageRepository extends BaseChatMessageRepository {
  final Dio _dioClient;

  ChatMessageRepository({
    Dio? dioClient,
  }) : _dioClient = dioClient ?? DioClient().instance;

  @override
  Future<AppResponse<ChatMessageEntity?>> createChatMessage(
    CreateChatMessagesRequest request,
    String socketId,
  ) async {
    final response = await _dioClient.post(
      Endpoints.createChatMessages,
      data: request.toJson(),
      options: Options(
        headers: {
          'X-Socket-ID': socketId,
        },
      ),
    );
    return AppResponse<ChatMessageEntity?>.fromJson(
      response.data,
      (dynamic json) => response.data['success'] && json != null
          ? ChatMessageEntity.fromJson(json)
          : null,
    );
  }

  @override
  Future<AppResponse<List<ChatMessageEntity>>> getChatMessages({
    required int chatId,
    required int page,
  }) async {
    final response = await _dioClient.get(
      Endpoints.getChatMessages,
      queryParameters: {
        'page': page,
        'chat_id': chatId,
      },
    );

    return AppResponse<List<ChatMessageEntity>>.fromJson(
      response.data,
      (dynamic json) => response.data['success'] && json != null
          ? (json as List<dynamic>)
              .map((e) => ChatMessageEntity.fromJson(e))
              .toList()
          : [],
    );
  }

  @override
  Future<AppResponse<ChatMessageEntity?>> createChatGrupMessage(
    CreateChatMessagesRequest request,
    String socketId,
  ) async {
    final response = await _dioClient.post(
      Endpoints.createGrupChatMessages,
      data: request.toJson(),
      options: Options(
        headers: {
          'X-Socket-ID': socketId,
        },
      ),
    );
    return AppResponse<ChatMessageEntity?>.fromJson(
      response.data,
      (dynamic json) => response.data['success'] && json != null
          ? ChatMessageEntity.fromJson(json)
          : null,
    );
  }

  @override
  Future<AppResponse<List<ChatMessageEntity>>> getChatGrupMessages({
    required int chatId,
    required int page,
  }) async {
    final response = await _dioClient.get(
      Endpoints.getGrupChatMessages,
      queryParameters: {
        'page': page,
        'chat_id': chatId,
      },
    );

    return AppResponse<List<ChatMessageEntity>>.fromJson(
      response.data,
      (dynamic json) => response.data['success'] && json != null
          ? (json as List<dynamic>)
              .map((e) => ChatMessageEntity.fromJson(e))
              .toList()
          : [],
    );
  }
}
