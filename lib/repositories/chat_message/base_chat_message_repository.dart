import 'package:bakamlachat/models/models.dart';
import 'package:bakamlachat/models/requests/requests.dart';

abstract class BaseChatMessageRepository {
  Future<AppResponse<List<ChatMessageEntity>>> getChatMessages({
    required int chatId,
    required int page,
  });

  Future<AppResponse<ChatMessageEntity?>> createChatMessage(
    CreateChatMessagesRequest request,
    String socketId,
  );

  Future<AppResponse<List<ChatMessageEntity>>> getChatGrupMessages({
    required int chatId,
    required int page,
  });
}
