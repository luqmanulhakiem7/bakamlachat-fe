// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:bakamlachat/models/user_models.dart';

void main() {
  UserEntity user = UserEntity(
      id: 1,
      email: 'luqman@gmail.com',
      username: 'password',
      avatar: 'avatar.jpg');
  print(user);
  UserEntity user2 = user.copyWith(username: 'luqman');
  print(user2);
}
